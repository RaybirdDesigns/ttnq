<?php
/**
 * Plugin Name: ATDW Data Syncomatic
 * Description: Pull and sync data from ATDW
 * Version:     0.0.1
 * Author:      Michael Tayler
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * TTNQ API key 614b9be74e6e4d9c8ed6ea1aa86411a1
 */

// defined( 'ABSPATH' ) or die();

if ( !function_exists( 'add_action' ) && !function_exists( 'get_field' )) {
    return;
}

// Remove cron on plugin deactivation
// Remove action that will remove posts from the current listings array
register_deactivation_hook( __FILE__, 'remove_atdw_sync_cron' );
function remove_atdw_sync_cron() {
    update_option( 'atdwds_auto_sync', '' );
    wp_clear_scheduled_hook('atdw_sync');
    remove_action( 'before_delete_post', 'remove_from_current_array');
}
remove_action( 'before_delete_post', 'remove_from_current_array');
// Setup action to remove product_id from current product_ids array when a post is deleted
function remove_from_current_array($post_id) {
    if ($post_id !== null) {
        $prod_number = get_field('product_id',$post_id);
        if ($prod_number) {
            AtdwDataSyncomatic::remove_product_from_current_product_numbers_array($prod_number);
        }
    }
}
add_action( 'before_delete_post', 'remove_from_current_array');


class AtdwDataSyncomatic {
    
	public static function init() {
		add_action('admin_menu', array('AtdwDataSyncomatic', 'register_menu_page'));
        add_action('admin_init', array('AtdwDataSyncomatic', 'register_settings'));
        add_action('admin_init', array('AtdwDataSyncomatic', 'product_preview'));
        add_action('admin_init', array('AtdwDataSyncomatic', 'activate_atdw_sync'));
        add_action('admin_init', array('AtdwDataSyncomatic', 'create_current_atdw_listings_array'));
		add_action('admin_enqueue_scripts', array('AtdwDataSyncomatic', 'enqueue_scripts_css'));
        add_action('wp_ajax_show_data', array('AtdwDataSyncomatic', 'show_data'));
        add_action('admin_post_remove_product_from_wp', array('AtdwDataSyncomatic', 'remove_product_from_wp'));
        add_action('admin_post_call_updated_products', array('AtdwDataSyncomatic', 'call_updated_products'));
        add_action('admin_post_call_update_product', array('AtdwDataSyncomatic', 'call_update_product'));
        add_action('admin_post_add_single_member', array('AtdwDataSyncomatic', 'add_single_member'));
        add_action('admin_post_cancel_single_member', array('AtdwDataSyncomatic', 'cancel_single_member'));
	}

    /******************
    * Setup functions
    *******************/
	public static function  register_menu_page() {
	    add_menu_page(
	        'ATDW Settings', // Page title
	        'ATDW', // Menu title
	        'manage_options',
	        plugin_dir_path(__FILE__) . 'admin.php',
	        null,
	        plugin_dir_url(__FILE__) . 'images/atdw.png',
	        1000
	    );
	}

	public static function register_settings() {
		// Create fields in database
        add_option('atdwds_key', '');
        add_option('atdwds_email', '');
        add_option('atdwds_last_attempted_update_time', '');
        add_option('atdwds_last_updated_time', '');
        add_option('atdwds_last_updated_products', '');
        add_option('atdwds_was_updated', '');
        add_option('atdwds_current_product_numbers', '');
        add_option('atdwds_current_atdw_post_prod_ids', '');
        add_option('atdwds_created_posts', '');
        add_option('atdwds_error', '');
        add_option('atdwds_execution_time', '');
        add_option('atdwds_add_single_member', '');
        add_option('atdwds_add_single_status', '');
        add_option('atdwds_product_preview', '');
        add_option('atdwds_product_preview_error', '');
        add_option('atdwds_keep_image_sync', '');
        add_option('atdwds_keep_start_end_date_sync', '');
        add_option('atdwds_keep_accomm_sync', '');
        add_option('atdwds_keep_features_sync', '');
        add_option('atdwds_keep_title_sync', '');
        add_option('atdwds_keep_content_sync', '');
        add_option('atdwds_keep_comms_sync', '');
        add_option('atdwds_keep_location_sync', '');
        add_option('atdwds_keep_coords_sync', '');
        add_option('atdwds_keep_platforms_sync', '');
        add_option('atdwds_keep_subcats_sync', '');
        add_option('atdwds_keep_access_sync', '');
        add_option('atdwds_keep_system_sync', '');
        add_option('atdwds_auto_sync', '');
        add_option('atdwds_working', false);
        // add_option('atdwds_test', '');

		/* user-configurable value checking public static functions */
        register_setting( 'atdwds_group', 'atdwds_key', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_email', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_add_single_member', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_image_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_start_end_date_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_accomm_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_features_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_title_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_content_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_comms_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_location_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_coords_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_platforms_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_subcats_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_access_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_keep_system_sync', 'AtdwDataSyncomatic::filter_string' );
        register_setting( 'atdwds_group', 'atdwds_auto_sync', 'AtdwDataSyncomatic::filter_string' );        

        if (AtdwDataSyncomatic::valid_key_secret(get_option('atdwds_key'))) {
            update_option('atdwds_working', true);
        } else {
            update_option('atdwds_working', false);
        }
	}

    public static function activate_atdw_sync() { // Set up cron job for daily syncing
        if (get_option('atdwds_auto_sync') === 'auto_sync' && strpos(get_site_url(), 'www.tropicalnorthqueensland.org') < -1) {

            if (!wp_next_scheduled('atdw_sync')) {
                wp_schedule_event(time(), 'daily', 'atdw_sync');
            }
            add_action('atdw_sync', array('AtdwDataSyncomatic', 'call_updated_products'));
        } else {
            update_option('atdwds_auto_sync', '');
            wp_clear_scheduled_hook('atdw_sync');
        }
    }

    /*****************
    * Helper functions
    ******************/
	public static function filter_string( $string ) {
        return trim(filter_var($string, FILTER_SANITIZE_STRING)); //must consist of valid string characters
    }

    public static function valid_key_secret( $string ) {
        if (strlen($string) === 32) {
            return true;
        } else {
            return false;
        }
    }

    public static function prod_numbers_format( $products ) { // Format product numbers string for use in delta call
        $products_string = preg_replace('/,/' ,'%22,%22', $products);
        return '%22'.$products_string.'%22';
    }

    public static function enqueue_scripts_css( $hook ) { // Add css and js to ATDW plugin settings page
    	if ($hook == 'atdw-data-syncomatic/admin.php' ) {
    		wp_enqueue_style( 'atdwds_styles', plugin_dir_url( __FILE__ ) . 'css/style.css' );
    		wp_enqueue_script( 'atdwds_script', plugin_dir_url( __FILE__ ) . 'js/index.js' );
    	}
    }

    public static function make_api_request( $api_end_point ) { // Make ATDW request and do all the encoding, sanitizing conversion and json decoding
        $raw_json =  @file_get_contents($api_end_point);

        if ($raw_json === false) {
            // update_option('atdwds_error', 'Request empty');
            return false; // Get out early
        }

        $utf8_json = iconv($in_charset = 'UTF-16LE' , $out_charset = 'UTF-8' , $raw_json);

        $sanitized_utf8_json = preg_replace('/[\x00-\x1F\x7F]/', '', $utf8_json);// get rid of any hidden characters
        $data_arr = json_decode($sanitized_utf8_json, true);

        return $data_arr;
    }

    /********************************
    * Functions that get and set data
    **********************************/

    // On page init get all listings that have an ATDW product ID and update the current listings array.
    // Also create an associative array with the post_id => product_id
    public static function create_current_atdw_listings_array() {

        $custom_post_types = array('accommodation', 'product');

        $current_atdw_posts_args = array(
            'post_type' => $custom_post_types,
            'post_status' => array('draft', 'publish', 'pending', 'trash'),
            'paged' => false,
            'posts_per_page' => 1500,
            'update_post_term_cache' => false,
            'no_found_rows' => true,
        );
        $updated_query = new WP_Query($current_atdw_posts_args);
        $updated_posts = $updated_query->posts;

        $current_atdw_ids = array();
        $current_atdw_post_prod_ids = array();
        foreach($updated_posts as $post) {
            $custom_fields = get_post_custom($post->ID);
            if ($custom_fields['product_id'][0]) {
                $current_atdw_ids[] = $custom_fields['product_id'][0];
                $current_atdw_post_prod_ids[$custom_fields['product_id'][0]] = $post->ID;
            }
        }
        update_option('atdwds_current_product_numbers', implode(',',array_filter($current_atdw_ids)));
        update_option('atdwds_current_atdw_post_prod_ids', json_encode($current_atdw_post_prod_ids));
    } 
    
    public static function call_updated_products() { // Function called by "Update Posts" button in the interface and cron job
        $data = AtdwDataSyncomatic::get_each_product_and_create_products_array(false);
    }

    public static function call_update_product() { // Function called by "Update Posts" button in the interface and cron job
        $data = AtdwDataSyncomatic::get_each_product_and_create_products_array(true);
    }

    public static function get_each_product_and_create_products_array($is_single = false) { // then pass on to set set_updated_posts

        $api_key = get_option('atdwds_key');

        if ($api_key === null) {
            return false;
        }

        // Hit delta end point for all products update or just update single chosen product
        if (!$is_single) {
            $updated_product_data = AtdwDataSyncomatic::get_updated_products_from_delta(); // First run delta and get any updated products
        } else {
            $updated_product_data = AtdwDataSyncomatic::single_update(); // Get single product update
        }

        $updated_products = $updated_product_data;

        // update_option( 'atdwds_error', json_encode($updated_products) );

        // Need to create the following arrays and add them to the updated products array. Reason: when a product is made inactive or expires in ATDW
        // only it's status is returned when doing a get product api request. Need to add these into the updated products array so we have these pieces
        // of data to let users know what listing was made inactive
        $prod_numbers = array(); // Make array of just the product numbers
        $prod_statuss = array(); // Make array of just the product statuss
        $prod_category = array(); // Make array of just the product category - use this for inactive products
        $prod_name = array(); // Make array of just the name - use this for inactive products
        foreach($updated_products as $updated_product) {
            $updated_product_id = $updated_product['productId'];
            $updated_product_status = $updated_product['status'];
            $updated_product_category = $updated_product['productCategoryId'];
            $updated_product_name = $updated_product['productName'];

            $prod_numbers[] = $updated_product_id;
            $prod_statuss[] = $updated_product_status;
            $prod_category[] = $updated_product_category;
            $prod_name[] = $updated_product_name;
        }
        
        $base_url = 'https://atlas.atdw-online.com.au/api/atlas/';
        $products = array(); // Combine all products to be added or updated into single array. Also add in the data we got from the delta call.
        $product_index = -1;
        foreach($prod_numbers as $id) {
            $product_index++;
            $api_end_point = $base_url.'/product?key='.$api_key.'&productId='.$id.'&out=json';

            $product_data = AtdwDataSyncomatic::make_api_request($api_end_point); // Make api request to get single product full details
            
            $product_data['status'] = $prod_statuss[$product_index]; // Add status to array so we know what to do when it gets to post creation
            $product_data['product_ID'] = $id; // Add product ID so we know the product if it's status is in active or expired
            $product_data['product_cat'] = $prod_category[$product_index]; // Add product Category so we can query for the product in the correct post type
            $product_data['product_name'] = $prod_name[$product_index]; // Add product name so we can use it for expired posts

            $products[] = $product_data;
        };
        // update_option('atdwds_error', count($products));
        AtdwDataSyncomatic::set_updated_posts($products, $is_single);
    }

    public static function single_update() {
        $api_key = get_option('atdwds_key');

        if ($api_key === null) {
            return false;
        }

        $base_url = 'https://atlas.atdw-online.com.au/api/atlas';

        $prod_id = $_GET['productID'];

        $update_end_point = $base_url.'/products?key='.$api_key.'&additionalQuery=productId:('.$prod_id.')&fl=product_id,status,product_name,product_category&out=json';
        
        $delta_data = AtdwDataSyncomatic::make_api_request($update_end_point);

        $product_array = $delta_data['products'];

        if (count($product_array) > 0) { // handle the result of the api call
            update_option('atdwds_was_updated', true);
            return $product_array;
        } else {
            update_option('atdwds_was_updated', false);

            // Redirect back to settings page
            $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=attempted";
            header("Location: ".$redirect_url);
            exit();
        }
    }

    /*
    * Get updated products from delta end point
    * store time of attempted update
    * https://atlas.atdw-online.com.au/api/atlas/products?key=614b9be74e6e4d9c8ed6ea1aa86411a1&additionalQuery=productId:(56b23f48d270154b45540bf7)&fl=product_id,status,product_name,product_category&delta=2019-06-01&pge=1&size=50&out=json
    * The ATDW Delta call can only handle so many product_Ids at once so we need to "chunk" the array into lots of 20 then loop through them making API calls
    * then we merge the resulting nested array into a single dimensional array and pass it on to make subsequent API calls.
    */
    public static function get_updated_products_from_delta() {
        $api_key = get_option('atdwds_key');

        if ($api_key === null) {
            return false;
        }

        $base_url = 'https://atlas.atdw-online.com.au/api/atlas';
        $atdwds_current_product_numbers = get_option('atdwds_current_product_numbers');
        $last_attempted_check = get_option('atdwds_last_attempted_update_time') !== null ? trim(substr(get_option('atdwds_last_attempted_update_time'), 0, 10)) : date('Y-m-d');
        $atdwds_current_product_numbers_arr = explode(',', $atdwds_current_product_numbers);
        $atdwds_current_product_numbers = array_chunk($atdwds_current_product_numbers_arr, 20); // Chunk current product array into nested array containig 20 listings each

        $products_array = array();
        foreach($atdwds_current_product_numbers as $number_arr) { // make all the API calls

            $prod_ids = AtdwDataSyncomatic::prod_numbers_format(implode(',', $number_arr));

            $delta_end_point = $base_url.'/products?key='.$api_key.'&additionalQuery=productId:('.$prod_ids.')&fl=product_id,status,product_name,product_category&delta='.$last_attempted_check.'&pge=1&size=80&out=json';
            // $delta_end_point = $base_url.'/products?key='.get_option('atdwds_key').'&additionalQuery=productId:('.$prod_ids.')&fl=product_id,status,product_name,product_category&delta=2019-07-28&pge=1&size=80&out=json';
            
            $delta_data = AtdwDataSyncomatic::make_api_request($delta_end_point);

            $products_array[] = $delta_data['products'];
            usleep(50000); // Give server a small brake
        }

        $body_array = call_user_func_array('array_merge', $products_array); // The array passed in is nested and messy, this will flatten it.

        update_option('atdwds_last_attempted_update_time', date('Y-m-d G:i:s')); // Date and time of last update. This is shown in the interface.

        if (count($body_array) > 0) { // handle the result of the api call
            update_option('atdwds_was_updated', true);
            return $body_array;
        } else {
            update_option('atdwds_was_updated', false);

            // Redirect back to settings page
            $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=attempted";
            header("Location: ".$redirect_url);
            exit();
        }
    }

    // Take the data from the Delta call, loop through and update parts of the listings that have been user defined as updateable.
    // Also if a listing has been made inactive or has expired in ATDW set the post to draft.
    public static function set_updated_posts($products, $is_single = false) { 
        $current_products_arr = explode(',',get_option('atdwds_current_product_numbers'));
        $custom_post_types = array('accommodation', 'product');
        $current_prod_post_ids = json_decode(get_option('atdwds_current_atdw_post_prod_ids'), true);

        $title_sync = get_option('atdwds_keep_title_sync');
        $content_sync = get_option('atdwds_keep_content_sync');
        $date_sync = get_option('atdwds_keep_start_end_date_sync');
        $accomm_sync = get_option('atdwds_keep_accomm_sync');
        $features_sync = get_option('atdwds_keep_features_sync');
        $comms_sync = get_option('atdwds_keep_comms_sync');
        $location_sync = get_option('atdwds_keep_location_sync');
        $coords_sync = get_option('atdwds_keep_coords_sync');
        $platforms_sync = get_option('atdwds_keep_platforms_sync');
        $access_sync = get_option('atdwds_keep_access_sync');
        $system_sync = get_option('atdwds_keep_system_sync');
        $image_sync = get_option('atdwds_keep_image_sync');
        $subcats_sync = get_option('atdwds_keep_subcats_sync');

        set_time_limit(300); // allow for extra time to run script

        if (!empty($products)) {

            foreach ($products as $product) {

                $post_id = $current_prod_post_ids[$product['product_ID']];

                if (in_array($product['product_ID'] ,$current_products_arr) && ($product['status'] === 'INACTIVE' || $product['status'] === 'EXPIRED')) {
                    // EXPIRE POST - if it exists and it is expired or inactive
                    $post_arr = array(
                        'ID' => $post_id,
                        'post_status' => 'draft',
                    );
                    wp_update_post($post_arr);

                } elseif (in_array($product['productId'] ,$current_products_arr)) {
                    // UPDATE POST - if already exists and status is ACTIVE
                    $post_arr = array(
                        'ID' => $post_id,
                        'post_status' => 'publish',
                    );

                    if ($title_sync === 'title_sync') {
                        $post_arr['post_title'] = sanitize_text_field($product['productName']);
                    }

                    if ($content_sync === 'content_sync') {
                        $post_arr['post_content'] = sanitize_textarea_field($product['productDescription']);
                    }

                    wp_update_post( $post_arr ); // Update post obj

                    // Following updates the acf fields of the post
                    if ($subcats_sync === 'subcats_sync') {
                        AtdwDataSyncomatic::set_acf_subcats($product, $post_id);
                    }

                    if ($image_sync === 'image_sync') {
                        AtdwDataSyncomatic::set_acf_images($product, $post_id);
                    }

                    if ($date_sync === 'date_sync') {
                        AtdwDataSyncomatic::set_acf_start_end_date($product, $post_id);
                    }

                    if ($accomm_sync === 'accomm_sync') {
                        AtdwDataSyncomatic::set_acf_accomm($product, $post_id);
                        AtdwDataSyncomatic::set_acf_star_rating($product, $post_id);
                    }

                    if ($features_sync === 'features_sync') {
                        AtdwDataSyncomatic::set_acf_features($product, $post_id);
                    }

                    if ($comms_sync === 'comms_sync') {
                        AtdwDataSyncomatic::set_acf_comms($product, $post_id);
                    }

                    if ($location_sync === 'location_sync') {
                        AtdwDataSyncomatic::set_acf_location($product, $post_id);
                    }

                    if ($coords_sync === 'coords_sync') {
                        AtdwDataSyncomatic::set_acf_coords($product, $post_id);
                    }

                    if ($platforms_sync === 'platforms_sync') {
                        AtdwDataSyncomatic::set_acf_platforms($product, $post_id);
                    }

                    if ($access_sync === 'access_sync') {
                        AtdwDataSyncomatic::set_acf_access($product, $post_id);
                    }

                    if ($system_sync === 'system_sync') {
                        AtdwDataSyncomatic::set_acf_system($product, $post_id);
                    }

                }

                usleep(50000); // Give server a small brake
            }

            if (!$is_single) {
                update_option('atdwds_last_updated_time', date('Y-m-d G:i:s')); // date and time of successfull update
                update_option('atdwds_last_updated_products', json_encode($products));

                // Set some stats about what just happened
                $execution_time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]; // Get time the operation took
                update_option( 'atdwds_execution_time', $execution_time );

                // Email notification
                AtdwDataSyncomatic::send_email_notification();
                update_option('atdwds_error', 'products updated');

                $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php";

            } else {
                update_option('atdwds_error', 'single updated');
                $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=updated";
            }

            set_time_limit(60); // Reset time limit after turning it off for the pull                
            // Redirect back to settings page
            header("Location: ".$redirect_url);
            exit();
        }
    }

    public static function send_email_notification() {
        $new_or_updated_posts = json_decode(get_option( 'atdwds_last_updated_products' ), true);
        $email_message;
        foreach($new_or_updated_posts as $new_or_updated_post) {
            $status_color = $new_or_updated_post['status'] === 'INACTIVE' || $new_or_updated_post['status'] === 'EXPIRED' ? 'style="color: red;"' : null;
            $email_message .= '<li '.$status_color.'>'.$new_or_updated_post['product_name'].' - '.$new_or_updated_post['status'].'</li>';
        }

        $to = get_option('atdwds_email');
        $subject = count($new_or_updated_posts).' Listings updated in '.get_bloginfo( 'name' ).' site';
        $message = '<p>The following member listings have been updated:</p><ol>'.$email_message.'</ol><p>More information is available in your <a target="_blank" href="'.site_url().'/wp-admin/admin.php?page=atdw-data-syncomatic%2Fadmin.php">sites ATDW admin area</a>.</p>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $to, $subject, $message, $headers, null );
    }

    // Called after user has entered product ID and hit save.
    // Will make a call to ATDW and get product data to show user before importing
    public static function product_preview() {
        $api_key = get_option('atdwds_key');

        if ($api_key === null) {
            return false;
        }

        $base_url = 'https://atlas.atdw-online.com.au/api/atlas';
        $new_member_id = get_option('atdwds_add_single_member');
        update_option( 'atdwds_product_preview_error', '');
        update_option( 'atdwds_product_preview', '');

        if (!empty(get_option('atdwds_add_single_member')) && empty(get_option('atdwds_product_preview'))) {
            $search_end_point = $base_url.'/products?key='.$api_key.'&additionalQuery=productId:(%22'.$new_member_id.'%22)&out=json';
            $new_member_data = AtdwDataSyncomatic::make_api_request($search_end_point); // Make api request and return data
            $new_member_product = $new_member_data['products'];

            if ($new_member_data['numberOfResults'] > 0) {
                update_option( 'atdwds_product_preview', json_encode($new_member_product));
            } else {
                update_option( 'atdwds_product_preview_error', 'empty');
            }
        }
    }

    // Following two functions remove the product ID from the current product numbers array and perminantly delete the post from WP.
    public static function remove_product_from_current_product_numbers_array($prod_number) {
        if ($prod_number) {
            $current_products_arr = explode(',',get_option('atdwds_current_product_numbers'));
            $prod_index = array_search($prod_number, $current_products_arr);
            unset($current_products_arr[$prod_index]);

            update_option('atdwds_current_product_numbers', implode(',',$current_products_arr));
        }
    }

    public static function remove_product_from_wp() { // Permanently delete post from WP
        $post_id = $_GET['post_id'];

        wp_delete_post($post_id, true);

        // Redirect back to settings page
        $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=deleted";
        header("Location: ".$redirect_url);
        exit();
    }

    public static function cancel_single_member() {
        update_option('atdwds_add_single_member', '');
        update_option('atdwds_product_preview', '');

        // Redirect back to settings page
        $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=cancelled";
        header("Location: ".$redirect_url);
        exit();
    }

    // Make API call to ATDW get the prodct data, create a new post for it in the correct category. Add all the ACF fields.
    public static function add_single_member() {
        $api_key = get_option('atdwds_key');

        if ($api_key === null) {
            return false;
        }

        $base_url = 'https://atlas.atdw-online.com.au/api/atlas';
        $current_members = get_option('atdwds_current_product_numbers');
        $new_member = get_option('atdwds_add_single_member');

        if (!empty($new_member)) {

            $product_end_point = $base_url.'/product?key='.$api_key.'&productId='.$new_member.'&out=json';
            $product = AtdwDataSyncomatic::make_api_request($product_end_point); // Make api request and get data

            if (!in_array($new_member, explode(',',$current_members))) {

                $atdw_category = strtolower($product['productCategoryId']);

                $post_type = $atdw_category === 'accomm' ? 'accommodation' : 'product';

                // CREATE POST - if it doesn't exist && is active
                $post_arr = array(
                    'post_status' => 'draft',
                    'post_type' => $post_type,
                    'post_title' => sanitize_text_field($product['productName']),
                    'post_content' => sanitize_textarea_field($product['productDescription']),
                    'post_author' => 0
                );
                $post_id = wp_insert_post( $post_arr );

                // $taxonomy = strtolower($product['productCategoryId']).'_type';
                // $terms_array = array(); // Sub categorisation
                // foreach($product['verticalClassifications'] as $attribute) {
                //     array_push($terms_array, strtolower($attribute['productTypeId']));
                // }
                // wp_set_object_terms($post_id, $terms_array, $taxonomy, true); // Set the custom taxonomy

                AtdwDataSyncomatic::set_acf_images($product, $post_id);
                AtdwDataSyncomatic::set_acf_start_end_date($product, $post_id);
                AtdwDataSyncomatic::set_acf_accomm($product, $post_id);
                AtdwDataSyncomatic::set_acf_star_rating($product, $post_id);
                AtdwDataSyncomatic::set_acf_features($product, $post_id);
                AtdwDataSyncomatic::set_acf_comms($product, $post_id);
                AtdwDataSyncomatic::set_acf_location($product, $post_id);
                AtdwDataSyncomatic::set_acf_coords($product, $post_id);
                AtdwDataSyncomatic::set_acf_platforms($product, $post_id);
                AtdwDataSyncomatic::set_acf_subcats($product, $post_id);
                AtdwDataSyncomatic::set_acf_access($product, $post_id);
                AtdwDataSyncomatic::set_acf_system($product, $post_id);


                update_option( 'atdwds_add_single_status', 'New member added. Please check in Post Type.' );

                // Redirect back to settings page
                $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=single_success";
                header("Location: ".$redirect_url);

            } else {
                update_option('atdwds_add_single_status', 'Member already exists. Please check Product Number.');

                // Redirect back to settings page
                $redirect_url = get_bloginfo("wpurl") . "/wp-admin/admin.php?page=atdw-data-syncomatic/admin.php&status=single_duplicate";
                header("Location: ".$redirect_url);
            }

            update_option('atdwds_add_single_member', '');

            exit();
        }
    }

    // The following functions sets the various groups of ACF fields.
    
    public static function set_acf_images($product, $post_id) {
        // Multimedia - max 10 images
        $image_index = -1;
        foreach($product['multimedia'] as $image) {
            if ($image['width'] === '2048' && $image['attributeIdSizeOrientation'] === '16X9' && $image_index <= 9) {
                $image_index++;
                update_field('product_image_'.$image_index, esc_url_raw(preg_replace('/\?.*/', '', $image['serverPath'])), $post_id);
                update_field('product_image_alt_'.$image_index, sanitize_text_field($image['altText']), $post_id);
                update_field('product_image_caption_'.$image_index, sanitize_text_field($image['caption']), $post_id);
            } elseif ($image['attributeIdMultimediaContent'] === 'VIDEO') {
                $image_index++;
                update_field('product_video_0', esc_url_raw($image['serverPath']), $post_id);
                update_field('product_video_thumb_0', esc_url_raw($image['videoThumbnailPath']), $post_id);
            }
        }
    }

    public static function set_acf_start_end_date($product, $post_id) {
        // Event - Just a start and end date
        $event_index = -1;
        $date_count = count($product['eventFrequency']);
        foreach($product['eventFrequency'] as $eventDate) {
            $event_index++;
            if (count($product['eventFrequency']) === 1) {
                update_field('start_time', sanitize_text_field($eventDate['frequencyStartDate']), $post_id);
                update_field('start_hour', sanitize_text_field($eventDate['frequencyStartTime']), $post_id);
                update_field('end_hour', sanitize_text_field($eventDate['frequencyEndTime']), $post_id);
                update_field('end_time', sanitize_text_field($eventDate['frequencyEndDate']), $post_id);
            } elseif (count($product['eventFrequency']) > 1) {
                if ($event_index === 0) {
                    update_field('start_time', sanitize_text_field($eventDate['frequencyStartDate']), $post_id);
                    update_field('start_hour', sanitize_text_field($eventDate['frequencyStartTime']), $post_id);
                } elseif($event_index === $date_count - 1) {
                    update_field('end_time', sanitize_text_field($eventDate['frequencyEndDate']), $post_id);
                    update_field('end_hour', sanitize_text_field($eventDate['frequencyEndTime']), $post_id);
                }
            }
        }
    }

    public static function set_acf_accomm($product, $post_id) {
        update_field('product_check_in', sanitize_text_field($product['checkInTime']), $post_id );
        update_field('product_check_out', sanitize_text_field($product['checkOutTime']), $post_id );
        update_field('product_rate_from', sanitize_text_field($product['rateFrom']), $post_id );
        update_field('product_rate_to', sanitize_text_field($product['rateTo']), $post_id );
    }

    // This func contains the $choices array which is specific to TTNQ
    public static function set_acf_features($product, $post_id) {
        $choices = array('carpark','barbeque','swimming-pool','laundry','outdoor-furniture','tour-desk','public-toilet','bar','non-smoking','family-friendly','restaurant','coach-parking','business-facilities','pay-tv','public-telephone','picnic-area','pet-friendly-enquire','cafe','kiosk','conference-function-facilities','communal-lounge','gas','communal-refrigerator','childrens-pool','communal-shower','shop','games-or-recreation-room','communal-kitchen','gym','outdoor-dining-area','camp-kitchen','tennis-court','spa-sauna','foreign-currency-exchange','automatic-teller-machine','baggage-holding-room','day-spa','interpretive-centre','conference-convention-facilities','open-fireplace','radio-stereo','parents-room','interactive-centre','dump-point','al-fresco-dining','24-hour-reception','playground','free');
        $attributes = array(); // Features like gym, ATM, Carpark etc
        foreach($product['attributes'] as $attribute) {
            if ($attribute['attributeTypeIdDescription'] === 'Entity Facility') {
                $facility = str_replace(' ', '-', strtolower($attribute['attributeIdDescription']));
                if (in_array($facility, $choices)) {
                    array_push($attributes, sanitize_text_field($facility));
                }
            }
        }
        update_field('product_facilities', $attributes, $post_id );
    }

    public static function set_acf_star_rating($product, $post_id) {
        foreach($product['attributes'] as $attribute) {
            if ($attribute['attributeTypeId'] === 'STARRATING') {
                update_field('product_star_rating', sanitize_text_field($attribute['attributeId']), $post_id );
            } 
        }
    }

    public static function set_acf_comms($product, $post_id) {
        // Communication
        foreach($product['communication'] as $communication) {
            switch($communication['attributeIdCommunicationDescription']) {
                case 'Email Enquiries':
                    update_field('product_email', sanitize_email($communication['communicationDetail']), $post_id );
                    break;
                case 'Primary Phone':
                    update_field('product_primary_phone', sanitize_text_field($communication['communicationDetail']), $post_id );
                    break;
                case 'Secondary Phone':
                    update_field('product_secondary_phone', sanitize_text_field($communication['communicationDetail']), $post_id );
                    break;
                case 'Booking URL':
                    update_field('product_booking_url', esc_url_raw($communication['communicationDetail']), $post_id );
                    break;
                case 'URL Enquiries':
                    update_field('product_enquiries_url', esc_url_raw($communication['communicationDetail']), $post_id );
                    break;
            }
        }
    }

    public static function set_acf_location($product, $post_id) {
        update_field('product_address_1', sanitize_text_field($product['addresses'][0]['addressLine1']), $post_id );
        update_field('product_address_2', sanitize_text_field($product['addresses'][0]['addressLine2']), $post_id );
        update_field('product_city_name', sanitize_text_field($product['addresses'][0]['cityName']), $post_id );
        update_field('product_area_name', sanitize_text_field($product['addresses'][0]['areaName']), $post_id );
        update_field('product_state_name', sanitize_text_field($product['addresses'][0]['stateName']), $post_id );
        update_field('product_country_name', sanitize_text_field($product['addresses'][0]['countryName']), $post_id );
        update_field('product_post_code', sanitize_text_field($product['addresses'][0]['addressPostalCode']), $post_id );
    }

    public static function set_acf_coords($product, $post_id) {
        $lat = sanitize_text_field($product['addresses'][0]['geocodeGdaLatitude']);
        $lng = sanitize_text_field($product['addresses'][0]['geocodeGdaLongitude']);
        $address = sanitize_text_field($product['addresses'][0]['addressLine1']);

        $coords_arr = array("address" => $address, "lat" => $lat, "lng" => $lng, "zoom" => 12);
        update_field('google_map_location', $coords_arr, $post_id);
    }

    public static function set_acf_platforms($product, $post_id) {
        // External platforms
        foreach($product['externalSystems'] as $external) {
            switch($external['externalSystemCode']) {
                case 'BOOKEASY':
                    update_field('bookeasy_id', sanitize_text_field($external['externalSystemText']), $post_id );
                    break;
                case 'TRIPADVISORID':
                    update_field('trip_advisor_id', sanitize_text_field($external['externalSystemText']), $post_id );
                    break;
                case 'TRIPADVISO':
                    update_field('trip_advisor_url', esc_url_raw($external['externalSystemText']), $post_id );
                    break;
                case 'FACEBOOK':
                    update_field('product_facebook', esc_url_raw($external['externalSystemText']), $post_id );
                    break;
                case 'TWITTER':
                    update_field('product_twitter', esc_url_raw($external['externalSystemText']), $post_id );
                    break;
                case 'INSTAGRAM':
                    update_field('product_instagram', esc_url_raw($external['externalSystemText']), $post_id );
                    break;
                case 'YOUTUBE':
                    update_field('product_youtube', esc_url_raw($external['externalSystemText']), $post_id );
                    break;
            }
        } 
    }

    public static function set_acf_subcats($product, $post_id) {
        $verticalClassifications = array(); // Sub categorisation
        foreach($product['verticalClassifications'] as $attribute) {
            array_push($verticalClassifications, sanitize_text_field($attribute['productTypeId']));
        }
        update_field('vertical_classifications', implode(',', $verticalClassifications), $post_id);
    }

    public static function set_acf_access($product, $post_id) {
        $access_text = !empty($product['accessibilityAttributes']) ? $product['accessibilityAttributes'][0]['attributeSubType1IdDescription'] : '';
        update_field('product_accessibility', sanitize_text_field($access_text), $post_id );
    }

    public static function set_acf_system($product, $post_id) {
        update_field('product_number', sanitize_text_field($product['productNumber']), $post_id );
        update_field('product_id', sanitize_text_field($product['productId']), $post_id );
        update_field('product_category_id', sanitize_text_field($product['productCategoryId']), $post_id );
        update_field('owning_organisation_id', sanitize_text_field($product['owningOrganisationId']), $post_id );
        update_field('owning_organisation_name', sanitize_text_field($product['owningOrganisationName']), $post_id );
        update_field('owning_organisation_number', sanitize_text_field($product['owningOrganisationNumber']), $post_id );
        update_field('product_pixel_url', esc_url_raw($product['productPixelURL']), $post_id );
    }
}
AtdwDataSyncomatic::init();