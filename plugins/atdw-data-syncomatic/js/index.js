// Form page vars
var params = [],
    hash;
var q = document.URL.split('?')[1];
if (q != undefined) {
    q = q.split('&');
    for (var i = 0; i < q.length; i++) {
        hash = q[i].split('=');
        params.push(hash[1]);
        params[hash[0]] = hash[1];
    }
}

var currentURL = document.URL;
var statusQueryStr = document.URL.split('&')[1];
var currentURLSansStatus = currentURL.replace('&'+statusQueryStr, '');

jQuery(document).on('ready', function(){

	// Get intial data /////////////////////
	var $getDataBtn = jQuery('[data-get-data]');
	var $getUpdateBtn = jQuery('[data-update]');

	$getDataBtn.each(function(i, btn) {
		var $btn = jQuery(btn);

		$btn.on('click', function() {
		var $imageLoader = $btn.next('[data-loader-image]');
			$imageLoader.fadeIn('fast');
		});
	});

	$getUpdateBtn.each(function(i, btn) {
		var $btn = jQuery(btn);

		$btn.on('click', function(){
			var $this = jQuery(this);
			$this.find('span').remove();
			$this.find('img').removeAttr('style');
		});
	});

	if (params['status'] === 'single_duplicate') {
		alert('Member already exists. Listing was not imported. Please check Product Number.');
		window.history.replaceState({}, document.title, currentURLSansStatus);
		location.reload();
	}

	if (params['status'] === 'attempted') {
		alert('You\'re up to date, nothing to update!');
		window.history.replaceState({}, document.title, currentURLSansStatus);
		location.reload();
	}

	if (params['status'] === 'updated') {
		alert('Listing Updated');
		window.history.replaceState({}, document.title, currentURLSansStatus);
		location.reload();
	}

	jQuery('[data-show-settings]').on('click', function() {
		var $this = jQuery(this);
		$this.toggleClass('active');
		jQuery('[data-settings-container]').slideToggle();

		if ($this.hasClass('active')) {
			$this.text('Hide Settings');
		} else {
			$this.text('Show Settings');
		}
	});

	jQuery('[data-check-btn]').on('click', function() {
		jQuery(this).text('Checking ATDW for you now...');
	});
});