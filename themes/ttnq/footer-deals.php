<?php

// Footer

?>

		</div>

		<footer id="footer" class="footer">
			<div class="row">
				<div class="small-12 columns">
					<div class="back-to-top-wrapper">
						<p>Back to top</p> <button class="back-to-top-btn small-icon-btn"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
					</div>
					<div class="footer-social">
						<h5>Follow us</h5>
						<?php if( have_rows('social_links', 'option') ): ?>

							<ul class="list-reset list-inline footer-social-list">

							<?php while( have_rows('social_links', 'option') ): the_row(); 

								$social_channel = get_sub_field('social_channel');
								$social_channel_url = get_sub_field('social_url');

								?>

								<li><a href="<?php echo $social_channel_url; ?>" target="_blank" class="social-link"><i class="fa fa-<?php echo $social_channel; ?>" aria-hidden="true"></i></a>
								</li>

							<?php endwhile; ?>

							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row footer-menus">
				<div class="small-12 medium-12 large-2 columns">
					<div class="footer-subscribe">
						<h5>Subscribe</h5>
						<p>Get the latest information on offers &amp; upcoming attractions via our weekly newsletter.</p>
						<?php echo do_shortcode( '[ninja_form id=1]' ); ?>
					</div>
				</div>
				<div class="small-12 medium-4 large-2 columns">
					<h5 class="menu-item-header-mobile">Where to go</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-where-to-go-menu' ) ); ?>
				</div>
				<div class="small-12 medium-8 large-4 columns">
					<h5 class="menu-item-header-mobile">Things to do</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-things-to-do-menu' ) ); ?>
				</div>
				<div class="small-12 medium-4 large-2 columns">
					<div class="small-12 medium-12 large-12 float-left">
						<h5 class="menu-item-header-mobile">What's on</h5>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-whats-on-menu' ) ); ?>
					</div>
					<div class="small-12 medium-12 large-12 float-left">
						<h5 class="useful-links-heading menu-item-header-mobile">Useful links</h5>
						<?php wp_nav_menu( array( 'theme_location' => 'footer-useful-links-menu' ) ); ?>
					</div>
				</div>
				<div class="small-12 medium-8 large-2 columns">
					<h5 class="menu-item-header-mobile">Plan your trip</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-plan-your-trip-menu' ) ); ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<div class="footer-geo-switcher">
						<aside class="geo-switcher-footer">
							<div class="geo-switcher-footer-wrapper">
								<div class="geo-switcher-footer-mask">Change region</div>
								<select name="geo-switcher" class="geo-switcher-footer-select" id="geo-switcher-footer-select">
									<option selected="true" disabled="disabled" value="">Select a region</option>
									<option value="AU">Australia</option>
									<option value="JP">Japan</option>
									<option value="DE">Germany</option>
								</select>
							</div>
							<button disabled class="btn geo-switcher-footer-btn">Continue</button>
						</aside>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 large-2 columns">
					<?php $main_logo = get_field('region', 'option') ? get_field('region', 'option') : 'au' ?>
					<svg class="footer-logo" role="presentation">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/logos/TNQ_logo_no-tagline.svg#Layer_1"></use>
                    </svg>
				</div>
				<div class="small-12 large-10 columns">
					<div class="footer-legal">
						<div class="footer-legal-menu">
							<?php wp_nav_menu( array( 'theme_location' => 'footer-legal-menu' ) ); ?>
						</div>
						<div>
							<p class="footer-copyright">© <span><?php echo date("Y"); ?></span> <?php the_field('legal_text','options'); ?> </p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<script>

			function ResponsiveBackgroundImage(element) {

				var element = element;
		        var img = element.querySelector('img');
		        var src = '';

		        img.addEventListener('load', function() {
		            update(element);
		        });

		        if (img.complete) {
		            update(element);
		        }

				function update(element) {
					var src = typeof img.currentSrc !== 'undefined' ? img.currentSrc : img.src;
				        if (src !== '') {
				            // src = src;
				            element.style.backgroundImage = 'url("' + src + '")';
				        }
				}
			}

			// run responsive bg on all elements with data attribute
			var bgImageElements = document.querySelectorAll('[data-responsive-background-image]');

			if (bgImageElements.length >= 1 ) {
				for (var i=0; i<bgImageElements.length; i++) {
					ResponsiveBackgroundImage(bgImageElements[i]);
				}
			}

		</script>
		<?php wp_footer()?>
		<script type="text/javascript">
		let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
		let page = 1;
		let tab_id =  "<?php echo ( isset($_GET['tab']) ? $_GET['tab'] : "" ) ?>";
		jQuery(function($) {
			
			$('body').on('click', '.see-more', function() {
				let data = {
					'action': 'load_posts_by_ajax',
					'page': page
				};
		
				$.post(ajaxurl, data, function(response) {
					if(response != '') {
						$('.deals-first-load').html(response);
						page++;
						$('.see-more').hide();
					} else {
						$('.see-more').hide();
					}
				});
			});
			let data_second = {
				'action': 'load_posts_by_ajax',
				'page': page
			};
	
			$.post(ajaxurl, data_second, function(response) {
				if(response != '') {
					$('.deals-first-load').html(response);
					page++;
				} else {
					$('.see-more').hide();
				}
			});


			$('body').on('click', '.tab-link', function() {
				let deal_category_id = $(this).data('id');
				let deal_category_tab = $(this).data('tab');
				let data_third = {
					'action': 'get_deal_item',
					'deal_category_id': deal_category_id,
					'deal_category_tab': deal_category_tab
				};
		
				$.post(ajaxurl, data_third, function(response) {
					if(response != '') {
						$('#'+deal_category_tab).html(response);
					}
				});
			});
			let deal_category_id = tab_id;
			let deal_category_tab = "tab-"+tab_id;
			let data_fourth = {
				'action': 'get_deal_item',
				'deal_category_id': deal_category_id,
				'deal_category_tab': deal_category_tab
			};
	
			$.post(ajaxurl, data_fourth, function(response) {
				if(response != '') {
					$('#'+deal_category_tab).html(response);
				}
			});

		});
		</script>
    </body>
</html>