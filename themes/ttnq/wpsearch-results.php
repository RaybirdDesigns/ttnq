<?php
/**
 * Template Name: Custom results
 */
global $post;

// retrieve our search query if applicable
$query = isset( $_REQUEST['swpquery'] ) ? sanitize_text_field( $_REQUEST['swpquery'] ) : '';
// retrieve our pagination if applicable
$swppg = isset( $_REQUEST['swppg'] ) ? absint( $_REQUEST['swppg'] ) : 1;

$event_term = isset( $_GET['term'] ) ? sanitize_text_field( $_GET['term'] ) : '';

if ( class_exists( 'SWP_Query' ) ) {

    $engine = 'events'; // taken from the SearchWP settings screen

    $swp_query = new SWP_Query(
        // see all args at https://searchwp.com/docs/swp_query/
        array(
            's'      => $query,
            'engine' => $engine,
            'page'   => $swppg,
            'tax_query' => array(
            array(
                'taxonomy' => 'post_tag',
                'field'    => 'slug',
                'terms'    => $event_term,
                )
            )
        )
    );
    // set up pagination
    $pagination = paginate_links( array(
        'format'  => '?swppg=%#%',
        'current' => $swppg,
        'total'   => $swp_query->max_num_pages,
        'next_text' => false,
        'prev_text' => false,
    ) );
}

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
    $thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
    $thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

?>

<?php get_header()?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<?php

if ( ! empty( $query ) && isset( $swp_query ) && ! empty( $swp_query->posts ) ) : ?>

<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <div class="search-hero-content text-center">
        <h1 class="pagetitle">Search results for "<?php echo ucwords($query) ?>" in <?php echo ucwords($event_term) ?> Events</h1>
    </div>
    <div class="hero-bottom-gradient"></div>
</div>

<section id="post-list">
    <div class="row">
        <div class="small-12 columns">

        <ul class="list-reset">

            <?php foreach( $swp_query->posts as $post ) : setup_postdata( $post ); ?>
            
            <?php $post_image = has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' )[0] : null; ?>

                <li class="post" id="post-<?php the_ID(); ?>">
                    <div class="post-image" style="background-image: url('<?php echo $post_image ?>');"></div>
                    <div class="post-text">
                        <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?><?php echo $event_time = get_field('time_of_event') ? ' - ' . get_field('time_of_event') : null; ?></a></h4>
                    <?php

                    if ( function_exists('the_excerpt') ) {
                        the_excerpt();
                    } ?>
                    </div>
                    
                    
                </li>
            <?php endforeach; ?>
            
            </ul>
            <?php // pagination
                if ( $swp_query->max_num_pages > 1 ) { ?>
                <div class="wp-pagination">
                    <div class="nav-links">
                        <?php echo wp_kses_post( $pagination ); ?>
                    </div>
                </div>
                <?php } ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</section>

<?php else : ?>

<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <div class="search-hero-content text-center">
        <h1 class="pagetitle">Sorry we couldn't find any results for "<?php echo ucwords($query) ?>" in <?php echo ucwords($event_term) ?> Events</h1>
    </div>
</div>

<section id="no-search-results" class="component">
        <div class="row">
            <div class="small-12 columns">
                <h2 class="text-center">Search for it?</h2>
                <div class="search-form-wrapper">
                    <svg class="search-ico" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
                    </svg>
                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                        <label>
                            <span class="show-for-sr">Search for:</span>
                            <input type="search" class="search-field" placeholder="Search Tropical North Queensland" value="" name="s" title="Search for:" />
                        </label>
                    </form>

                </div>
            </div>
        </div>
    </section>

<?php endif; ?>


<?php get_footer()?>