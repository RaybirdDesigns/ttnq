<?php
/**
 * Template Name: Events Category
 */

$theme_colour = get_field('theme_colour');

$category = get_the_category();
$first_category = $category[0]->slug;
$first_category_name = $category[0]->name;

// $allposttags = get_the_tags();
// $first_tag = $allposttags[0]->slug;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-home/hero', 'home') ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn,
	#feature-single a {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover,
	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	#feature-single a:hover,
	#feature-single a:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}
	
</style>

<?php
/*
	Set dynamic events tiles with custom override
*/

// set vars
$tile_spaces = 5;
$row_name = 'featured_events';
$subfield_name = 'featured_event';

$selected_posts_obj = [];
$selected_post_count  = null;

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);
}

// clean the acf array, remove the 'featured_events' key.
if (is_array($selected_posts_obj)) {
	
	$clean_selected_posts_obj = array();
	foreach($selected_posts_obj as $item) {
		$clean_selected_posts_obj[] = $item[$subfield_name];
	}
}

// only run priority sort and merge if less than $tile_spaces
if ($selected_post_count < $tile_spaces) {

	// calculate number of pages needed from event priority sort query
	$sort_posts_per_page = $tile_spaces - $selected_post_count;

	// event priority sort query
	$priority_wp_query_posts = priority_event_sort($sort_posts_per_page, false, null, $first_category);
	$priority_wp_query_posts = $priority_wp_query_posts->posts;

	// merge the two arrays together
	if ($selected_post_count > 0) {

		$posts = array_merge($clean_selected_posts_obj, $priority_wp_query_posts);
	} else {

		$posts = $priority_wp_query_posts;
	}
	
} else {

	$posts = $clean_selected_posts_obj;
}

?>

<?php if (is_user_logged_in() && count($posts) < $tile_spaces): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Events section doesn't have enough posts to be visible, <?php echo $tile_spaces;  ?> posts are required. Posts dynamically found: <?php echo count($priority_wp_query_posts); ?>. Posts authored: <?php echo count($clean_selected_posts_obj); ?>.</p>
</div>
<?php endif; ?>

<?php
    //change !== to == to reinitialise on site
    if ($posts && count($posts) == $tile_spaces): ?>
<section id="events">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<?php the_field('events_text'); ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php include(locate_template('/components/tiles-5-up/tiles-5-up.php')); ?>
		</div>
	</div>
	<?php
	$events_URL = add_query_arg(
		array(
			'post-type' => 'event',
			'tag' => $first_category,
			), get_site_url() . '/post-list/' 
		);

	$cta_text = $first_category && $first_category != 'uncategorised' ? 'Browse '.$first_category_name.' events ' : 'Browse all events ';
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost green btn-margin" href="<?php echo $events_URL; ?>"><?php echo $cta_text; ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>
<?php endif; ?>

<?php if ('event_feature_single_text'): ?>
	<section id="feature-single" class="feature-single">
		<div class="clearfix">
			<div class="feature-single-image small-12 large-7 columns" data-responsive-background-image>
				<img <?php responsive_image(get_field('event_feature_single_image'), 'full'); ?> />
			</div>
			<div class="feature-single-text small-12 large-5 columns">
				<div class="feature-single-text-wrapper">
					<?php the_field('event_feature_single_text'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if(!empty(get_field('event_social_feed_snippet'))): ?>
<section id="event-social" class="event-social">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns component text-center">
			<?php the_field('event_social_text'); ?>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<?php the_field('event_social_feed_snippet'); ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (!get_field('hide_event_search')) : ?>
<div class="event-search">
    <div class="row columns">
        <svg class="search-ico" role="presentation">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#search"></use>
        </svg>
        <div class="event-search-form">
			<form role="search" method="get" class="search-form clearfix" action="<?php echo get_site_url() .'/events-results/'; ?>">
				<label>
					<span class="show-for-sr">Search for:</span>
				</label>
					<input name="swpquery" type="search" class="search-field" placeholder="Search <?php echo strtolower($first_category_name); ?> events" value="" title="Search for:" />
					<input type="hidden" name="term" value="<?php echo $first_category; ?>">
					<button class="btn" type="submit">Search</button>
			</form>
        </div>
    </div>
</div>
<?php endif; ?>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<?php
// Show ad unit based on category
// randomised if more than one
get_template_part('components/ad-units/dyn-banner-wrapper');
?>

<?php if(!empty(get_field('related_full_width_bg'))): ?>
<section id="full-width-related" class="collapse">
	<div class="full-width-related clearfix text-center">
		<img <?php responsive_image(get_field('related_full_width_bg'), 'full') ?> >
		<div class="full-width-related-content">
			<?php the_field('related_pages_full_width_text'); ?>
			<?php if(!empty(get_field('related_pages_button_text'))): ?>
			<a class="btn btn-medium btn-ghost lite-blue btn-margin" href="<?php the_field('related_pages_full_width_url'); ?>"><?php the_field('related_pages_button_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>


<?php get_footer()?>