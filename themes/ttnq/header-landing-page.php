<?php
// require get_template_directory() . '/includes/geo-redirect.php';

// Region array. Add new regions as required
// This is used for the hreflang elements.
// @key attr hreflang
// @value sub folder
$region_arr = array("en-AU"=>"","ja-JP"=>"jp","de"=>"de");
?>

<!doctype html>
<html class="no-js test" lang="<?php echo get_locale() ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php bloginfo('title')?> | <?php the_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(is_admin()): ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
        <?php endif; ?>
        <link rel="icon" type="image/png" href="<?php the_field('favicon', 'option') ?>" sizes="64x64" />
        <?php foreach($region_arr as $key => $item):
        // Construct hreflang urls
        $slash = $item != '' ? '/' : null;
        $folders = get_current_blog_id() != '1' ? substr($_SERVER['REQUEST_URI'], 4) : substr($_SERVER['REQUEST_URI'], 1);
        $lang_url_main = 'http://www.tropicalnorthqueensland.org.au/'.$item.$slash.$folders;
        $lang_url_regions = 'https://www.tropicalnorthqueensland.org/'.$item.$slash.$folders;
        $lang_url = $item != '' ? $lang_url_regions : $lang_url_main;
        ?>
          <link rel="alternate" href="<?php echo $lang_url; ?>" hreflang="<?php echo $key; ?>">
        <?php endforeach; ?>

        <?php wp_head() ?>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T9N2BC6');</script>
        <!-- End Google Tag Manager -->
        <script type='text/javascript'>
        //<![CDATA[ 
        function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }loadCSS("https://use.fontawesome.com/1ff8671c70.css");

        <?php if(is_page_template('calendar.php')): ?>
        var numOfCalendarCalls = "<?php echo getNumCalls(); ?>";
        <?php endif; ?>
        //]]>
        </script>
        <?php the_field('header_scripts','option'); ?>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var cachebuster = Date.now();
                var script = document.createElement('script');
                script.src =
                    '//pixel.roymorgan.com/stats_v2/Tress.php?u=5tqi9smhs3&ca=20004801&a=daw1nwh3&cb=' + cachebuster;
                script.async = true;
                document.body.appendChild(script);
            });
        </script>
    </head>

    <?php $locale = isset($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : 'AU'; ?>

    <body class="landing-page-template <?php global $post; $post_slug=$post->post_name; echo $post_slug; ?>" data-locale="<?php echo $locale; ?>">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9N2BC6"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <!--[if lt IE 10]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php $main_logo = get_field('region', 'option') ? get_field('region', 'option') : 'au' ?>
        
        <header class="header-main">
            <div class="row">
                <div class="large-6 columns">
                       
                    <div class="header-container">        
                        <a href="javascript:history.back();" class="back-arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/landing-page/back-arrow.jpg" /></a>
                        <a class="logo-link" href="<?php echo (get_post_type() == 'deal')?site_url()."/deals":site_url(); ?>">
                            <!-- <svg class="header-logo" role="presentation">
                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#tnq-logo-<?php echo $main_logo; ?>"></use>
                            </svg> -->
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/CGBR-TNQ_logo-lockup.svg">
                        </a>                  
                    </div>
                </div>
                <div class="large-6 columns top-caption">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/landing-page/rainforest-text.png" />
                </div>
            </div>
        </header>
        

        <?php 
        // add schema type to main wrapper if page is a post
        switch(get_post_type()) {
            case 'post' :
                $schema_type = 'itemscope itemtype="http://schema.org/Article"';
                break;
            case 'product' : 
                $schema_type = 'itemscope itemtype="http://schema.org/TouristAttraction"';
                break;
            case 'accommodation' :
                $schema_type = 'itemscope itemtype="http://schema.org/Accommodation"';
                break;
            case 'event' :
                $schema_type = 'itemscope itemtype="http://schema.org/Event"';
                break;
            default:
                $schema_type = null;
        }

        // no schema on archive pages
        $schema_type_render = is_archive() || is_search() ? null : $schema_type;

        $notranslate = get_field('prevent_translation_of_this_page') ? 'notranslate' : '';

        ?>
        
        <div <?php echo $notranslate; ?> <?php echo $schema_type_render; ?> id="main">

        <?php
        // if it's a post add google schema meta tags
        if(is_single()) :
        $post_image_id = get_post_thumbnail_id($post->ID);
        $meta_image = wp_get_attachment_image_url($post_image_id, 'full') ? wp_get_attachment_image_url($post_image_id, 'full') : null ?>
        <meta itemprop="url" content="<?php the_permalink(); ?>">
        <meta itemprop="image" content="<?php echo $meta_image; ?>">
        <?php endif; ?>

        <?php
        // if blog article add the publish date google schema meta tags
        if(is_single() && get_post_type() == 'post') : ?>
        <meta itemprop="datePublished" content="<?php echo get_the_date(c); ?>">
        <span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="Tropical North Queensland"></span>
        <?php endif; ?>

