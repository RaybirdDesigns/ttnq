<?php
/*
Single Post Template: Event Article
Description: Template for an event article
*/

$theme_colour = get_field('theme_colour');

$category = get_the_category();

?>

<?php get_header()?>

<div id="facilities-sprite" class="hide">
    <?php echo file_get_contents( get_template_directory_uri() . '/images/features-sprite.svg', false, null ); ?>
</div>

<?php get_template_part('components/hero-banners/hero-event/hero', 'event') ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.event-details li i.fa,
	#while-here a.while-here-link,
	#feature a.feature-link {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px transparent;
	}

	#while-here a.while-here-link:hover,
	#feature a.feature-link:hover,
	#while-here a.while-here-link:focus,
	#feature a.feature-link:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.event-details li a {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<section id="description">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center component">
			<?php
			if (have_posts()) :
				while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile;
			endif;
			?>
		</div>
	</div>
	<?php if (get_field('prod_email') || 
			  get_field('prod_phone') ||
			  get_field('prod_website') ||
			  get_field('prod_address_line_1') ||
			  get_field('prod_address_line_1') ||
			  get_field('prod_address_line_2') ||
			  get_field('prod_area') ||
			  get_field('url_facebook') ||
			  get_field('url_twitter') ||
			  get_field('url_instagram') ||
			  get_field('url_youtube') ||
			  get_field('is_accessible')) : ?>
	<div class="row text-center">
		<div class="small-12 large-10 large-offset-1">
			<ul class="list-inline event-details event-details-row-1">
				<?php if(get_field('prod_email')) : ?>
				<li>
					<i class="fa fa-envelope" aria-hidden="true"></i>
					<p class="details-title"  >Email</p>
					<a href="mailto:<?php the_field('prod_email'); ?>"><?php the_field('prod_email'); ?></a>
				</li>
				<?php endif; ?>
				<?php if(get_field('prod_phone')) : ?>
				<li>
					<i class="fa fa-phone" aria-hidden="true"></i>
					<p class="details-title">Phone</p>
					<p class="details-detail"><?php the_field('prod_phone'); ?></p>
				</li>
				<?php endif; ?>
				<?php if(get_field('prod_website')) : ?>
				<?php 
					$website_url = get_field('prod_website');
					$website_url = preg_replace('#^https?://#', '', $website_url);
				?>
				<li>
					<i class="fa fa-mouse-pointer" aria-hidden="true"></i>
					<p class="details-title">Website</p>
					<a class="details-detail-website-link" target="_blank" href="<?php echo addhttp($website_url); ?>"><?php the_field('prod_website'); ?></a>
				</li>
				<?php endif; ?>
			</ul>
			<ul class="event-details event-details-row-2 list-inline">
				<?php if(get_field('prod_address_line_1') || get_field('prod_address_line_2') || get_field('prod_area')) : ?>
				<li itemprop="location" itemscope itemtype="http://schema.org/Place">
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					<span class="show-for-sr" itemprop="name"><?php the_title(); ?></span>
					<p class="details-title">Address</p>
					<p class="details-detail" itemprop="location" itemscope itemtype="http://schema.org/Place">
					<span itemprop="streetAddress"><?php the_field('prod_address_line_1'); ?>
					<?php echo $address2 = get_field('prod_address_line_2') ? get_field('prod_address_line_2') . "<br/>" : null; ?></span>
					<?php echo $area = get_field('prod_area') ? get_field('prod_area') . "<br/>" : null; ?>
					<span itemprop="addressLocality"><?php the_field('prod_city'); ?>
					<?php the_field('prod_suburb'); ?></span><br/>
					<span itemprop="addressRegion"><?php the_field('prod_state'); ?>
					<span itemprop="postalCode"><?php echo $post_code = get_field('prod_postcode') ? get_field('prod_postcode') . "," : null; ?></span>
					<?php the_field('prod_country'); ?>
					</p>
				</li>
				<?php endif; ?>
				<?php if(get_field('url_facebook') || get_field('url_twitter') || get_field('url_instagram') || get_field('url_youtube')) : ?>
				<li>
					<i class="fa fa-share" aria-hidden="true"></i>
					<p class="details-title">Social</p>
					<div class="details-social">
						<?php if(get_field('url_facebook')) : ?>
						<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_facebook')); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
						<?php endif; ?>
						<?php if(get_field('url_twitter')) : ?>
						<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_twitter')); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<?php endif; ?>
						<?php if(get_field('url_instagram')) : ?>
						<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_instagram')); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<?php endif; ?>
						<?php if(get_field('url_youtube')) : ?>
						<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_youtube')); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
						<?php endif; ?>
					</div>
				</li>
				<?php endif; ?>
				<?php if(get_field('is_accessible')) : ?>
				<li>
					<i class="fa fa-wheelchair-alt" aria-hidden="true"></i>
					<p class="details-title">Accessibility</p>
					<p>Accessible facilities available. Please contact operator for specific details.</p>
				</li>
				<?php endif; ?>
			</ul>	
		</div>
	</div>
	<?php endif; ?>
</section>

<?php 

$location = get_field('product_map');
if( !empty($location) ): ?>
<section id="map" class="collapse">
	<div class="row component">
		<div class="columns text-center">
			<h2>Event Location</h2>
		</div>
	</div>
	<div class="clearfix">
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
		<div class="address-overlay">
			<h4><?php the_title(); ?></h4>
			<p>
			<?php the_field('prod_address_line_1'); ?>
			<?php echo $address2 = get_field('prod_address_line_2') ? get_field('prod_address_line_2') . "<br/>" : null; ?>
			<?php echo $area = get_field('prod_area') ? get_field('prod_area') . "<br/>" : null; ?>
			<?php the_field('prod_city'); ?>
			<?php the_field('prod_suburb'); ?><br/>
			<?php the_field('prod_state'); ?>
			<?php echo $post_code = get_field('prod_postcode') ? get_field('prod_postcode') . "," : null; ?>
			<?php the_field('prod_country'); ?>
			</p>
			<div>
				<a href="http://maps.google.com/?q=<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>" target="_blank" class="btn btn-medium btn-ghost lite-blue" href="">Open in google maps <i class="fa fa-window-restore" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (have_rows('event_information_text')) :

	$count = count(get_field('event_information_text'));
	$col_layout = $count == 1 ? 'large-12': ($count == 2 ? 'large-6' : 'large-4');

?>
<section id="information">
	<div class="row text-center component">
		<div class="small-12 medium-8 medium-offset-2 component">
			<?php the_field('event_information_heading'); ?>
		</div>
	</div>
	<div class="row">
		<?php while ( have_rows('event_information_text') ) : the_row();?>
			
			<div class="small-12 <?php echo $col_layout; ?> columns">
				<?php the_sub_field('column_text'); ?>
			</div>

		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>

<?php if (have_rows('while_here')) : ?>
<section id="while-here" class="collapse">
	<div class="full-width-while-here clearfix" data-responsive-background-image >
		<img <?php responsive_image(get_field('while_here_bg_image'), 'full') ?> >
		<div class="row full-width-while-here-wrapper">
			<div class="clearfix component">
				<div class="small-12 columns text-center">
					<h2>While you're here</h2>
				</div>
			</div>
			<div class="clearfix">
				<ul class="list-reset while-here-list">

				   <?php while ( have_rows('while_here') ) : the_row();?>

					<li class="small-12 medium-6 columns">
						<div class="while-here-image" data-responsive-background-image >
							<img <?php responsive_image(get_sub_field('image'), 'large') ?> >
						</div>
						<?php the_sub_field('text'); ?>

						<?php if (get_sub_field('link_select') == 'file') : ?>
						<a target="_blank" class="while-here-link file" href="<?php the_sub_field('file_link'); ?>"><?php the_sub_field('link_text'); ?></a>
						<?php endif; ?>

						<?php if (get_sub_field('link_select') == 'external') : ?>
						<a target="_blank" class="while-here-link external" href="<?php the_sub_field('external_link'); ?>"><?php the_sub_field('link_text'); ?></a>
						<?php endif; ?>

						<?php if (get_sub_field('link_select') == 'internal') : ?>
						<a class="while-here-link internal" href="<?php the_sub_field('tnq_internal_link'); ?>"><?php the_sub_field('link_text'); ?></a>
						<?php endif; ?>
					</li>

				   <?php endwhile; ?>

				</ul>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if(get_field('video')) : ?>
<section id="video">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 medium-6 columns">
			<?php the_field('video_text'); ?>

			<?php if(get_field('video_btn_url')) : ?>
			<a target="_blank"  class="btn btn-medium btn-ghost btn-margin" href="<?php the_field('video_btn_url'); ?>"><?php the_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php if (get_field('feature_text')) : ?>
<section id="feature" class="collapse" data-responsive-background-image>
	<img <?php responsive_image(get_field('feature_background_image'), 'full') ?> >
		<div class="row">
			<div class="feature-content small-12 large-6 large-offset-6">
			<?php the_field('feature_text'); ?>

			<?php if (get_field('feature_is_internal_link') == 'internal') : ?>
			<a target="_blank" class="feature-link internal" href="<?php the_field('feature_internal_link'); ?>"><?php the_field('feature_link_text'); ?></a>
			<?php endif; ?>

			<?php if (get_field('feature_is_internal_link') == 'external') : ?>
			<a target="_blank" class="feature-link external" href="<?php the_field('feature_external_link'); ?>"><?php the_field('feature_link_text'); ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (get_field('stackla_html_snippet')) : ?>
<?php include(locate_template('/components/stackla/stackla.php')); ?>
<?php endif; ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-oulh7EDnr1Za7X2z-tsyhX6c8C5Jlsg"></script>

<?php get_footer()?>