<?php
/**
 * Template Name: Deals
 */

?>

<?php get_header('deals')?>

<?php get_template_part('components/hero-banners/hero-category/hero', 'category') ?>

<!-- Deal Main Page -->
<!-- <video id="videoBG" poster="" autoplay muted loop> -->
	<!-- <source src="<?php echo get_template_directory_uri(); ?>/video/ttnq-compressed.mp4" type="video/mp4"> -->
	<!-- <source src="https://tnqmedia.s3.amazonaws.com/tnq-deals-header.mp4" type="video/mp4"> -->
<!-- </video> -->
<!-- <div class="banner-wrapper">
	<img src="<?php echo get_template_directory_uri(); ?>/images/landing-page/banner-1.jpg" />
</div> -->
<?php /*<div class="banner-wrapper">
	<div class='embed-container'>
		<iframe src='https://player.vimeo.com/video/359437568?api=1&background=1&autoplay=1&loop=1&muted=1' height="300px" frameborder='0'></iframe>
	</div>
</div>
*/?>

<section class="collapse-bottom">

	<div class="row">
		<div class="small-12 columns text-center component">
			<?php while (have_posts()) : the_post(); ?>
				<?php echo get_the_content();?>
			<?php endwhile; ?>
		</div>		
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="small-12 show-more-container-inner">
			<ul class="list-reset tile-4-list clearfix">
				<?php 
					$post_ids = implode(',', get_field('exclusivedeals'));
					echo do_shortcode('[exclusive_deals post_ids='.$post_ids.']'); ?>
			</ul>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="small-12 show-more-container-inner">
			<ul class="list-reset tile-2-list clearfix">
				<?php 
				$normal_deals_ids = implode(',', get_field('normal_deals'));
				echo do_shortcode('[exclusive_deals_normal post_ids='.$normal_deals_ids.']'); ?>
			</ul>
		</div>
	</div>
	<div class="row">&nbsp;</div>
</section>
<section class="grid-container-list">
	<div class="row">
		<div class="small-12 columns text-center component">
			<h2>More options</h2>
		</div>
	</div>
	<!-- Tab Filters -->
	<div class="row">
		<!-- <div class="filter-selection mobile"><span>Show filters</span><i class="fa fa-angle-down close-icon" aria-hidden="true"></i></div> -->
		<ul class="tabs">
			<li class="tab-link <?php echo (!isset($_GET['tab']) || $_GET['tab'] == "all") ? "current":""; ?>" data-id="all" data-tab="tab-all">all</li>
			<?php
            if(!isset($_GET['tab'])) {
                $_GET['tab'] = 'all';
            }
			$ctr = 0;
			foreach(get_deals_categories() as $category) : $ctr++;
			if(isset($_GET['tab'])) {
                $activeTab = ($_GET['tab'] == $category->cat_ID) ? "current" : "";
            }
			?>
				<li class="tab-link <?php echo $activeTab; ?>" data-id="<?php echo $category->cat_ID; ?>" data-tab="tab-<?php echo $category->cat_ID; ?>"><?php echo $category->name; ?></li>
			<?php endforeach; ?>
		</ul>
			<!-- Tab content -->
			<div id="tab-all" class="tab-content <?php echo (!isset($_GET['tab']) || $_GET['tab'] == "all") ? "current":""; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/loader.gif" class="loading-image"/>
			</div> <!-- End Tab content -->
		<?php 
			$inc=0;
			foreach(get_deals_categories() as $category) : $inc++;
                if(isset($_GET['tab'])) {
                    $activeContent = ($_GET['tab'] == $category->cat_ID) ? "current" : "";
                }
			?>

			<!-- Tab content -->
			<div id="tab-<?php echo $category->cat_ID; ?>" class="tab-content <?php echo $activeContent; ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/loader.gif" class="loading-image"/>
			</div> <!-- End Tab content -->
		<?php
			endforeach;
		?>
		<!-- First Load and See more button -->
		<?php if(!isset($_GET['tab'])) : ?>
			<div class="row">
				<div class="small-12 show-more-container-inner">
					<ul class="list-reset tile-3-list clearfix deals-first-load tab-load">
						<div style="text-align:center"><img src="<?php echo get_template_directory_uri(); ?>/images/loader.gif" class="loading-image"/></div>
					</ul>
				</div>
			</div>
			<div class="deals-second-load tab-load"></div>
			<div class="row text-center">
				<a class="btn btn-medium btn-ghost green btn-margin see-more" href="javascript:void(0);">See more <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
			</div>		
		<?php endif; ?>
	</div>
</section>
<?php get_template_part('components/article-tile/featured-articles-deals'); ?>
<?php /*<section class="signup-wrapper">
	<div class="row">
		<div class="large-5 columns">
			<h3>Discover more</h3>
			<p>Be the first to hear about exclusive offers and deals in Tropical North Queensland. Sign up here for inspiration to plan your northern adventure.</p>
			<!-- <input class="text-input" type="text" placeholder="Email address..."/>
			<input id="signup-btn" class="ninja-forms-field btn btn-ghost btn-small dark-blue nf-element" type="button" value="Sign up" disabled=""> -->
			<?php echo do_shortcode('[ninja_form id=5]'); ?>
		</div>
		<div class="large-7 columns">
			<img src="<?php echo get_template_directory_uri(); ?>/images/landing-page/bottom-image.png" />
		</div>
	</div>
</section>
<!-- End Deal Main Page -->


<?php if (have_posts()) : ?>
	<!-- <section>
		<div class="row">
			<div class="columns">
				<?php
				while (have_posts()) : the_post();
					the_content();
				endwhile;
				?>
			</div>
		</div>
	</section> -->
<?php endif; */?>

<?php get_footer('deals'); ?>