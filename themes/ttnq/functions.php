<?php

// create custom hero image size
add_theme_support( 'post-thumbnails' );
add_image_size( 'hero-thumb', 300, 300 );
add_image_size( 'mobile-banner', 640, 1280, true );

 /*
 	=======================================
	Remove ATDW cron job in production
 	=======================================
 */
if (strpos(get_site_url(), 'www.tropicalnorthqueensland.org') < -1) {
	wp_clear_scheduled_hook('atdw_sync');
}

 /*
 	=======================================
	Check for Basic Auth
	Allow cross origin get request
 	=======================================
 */
function basic_auth_cred() {
	$url = $_SERVER['SERVER_NAME'];
	$auth = $url === 'ttnqdev.wpengine.com' ? base64_encode("ttnqdev:273d0f1d") : base64_encode("ttnqstage:97d20a53");

	if ($url === 'ttnqdev.wpengine.com' || $url === 'ttnqstage.wpengine.com') {
		$context = stream_context_create([
		    "http" => [
		        "header" => "Authorization: Basic $auth"
		    ]
		]);
	} else {
		$context = null;
	}

	return $context;
}

/*
	=======================================
	Get primary category
	=======================================
*/
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list) && !is_wp_error($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

/*
	=======================================
	Allow cross origin get request
	=======================================
*/
function my_customize_rest_cors() {
	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function( $value ) {
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Access-Control-Allow-Methods: GET' );
		header( 'Access-Control-Allow-Credentials: true' );
		header( 'Access-Control-Expose-Headers: Link', false );
		header( 'Access-Control-Allow-Headers: X-Requested-With' );
		return $value;
	} );
}
add_action( 'rest_api_init', 'my_customize_rest_cors', 15 );

/*
	=======================================
	Allow custom post types on tags template
	=======================================
*/
function wpse28145_add_custom_types( $query ) {
    if( is_tag() && $query->is_main_query() ) {

        // this gets all post types:
        $post_types = get_post_types();

        $query->set( 'post_type', $post_types );
    }
}
add_filter( 'pre_get_posts', 'wpse28145_add_custom_types' );

/*
	=======================================
	Insert Custom Login Logo
	=======================================
*/
function custom_login_logo() {
	echo '
		<style>
			.login h1 a { background-image: url(' . get_template_directory_uri() . '/images/logos/ttnq-logo.jpg) !important; background-size: 224px 165px; width:224px; height:165px; display:block; }
		</style>
	';
}
add_action( 'login_head', 'custom_login_logo' );


/*
	=======================================
	Remove admin sidebar stuff
	=======================================
*/
function remove_admin_menu_items() {
	$remove_menu_items = array(__('Comments'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
		unset($menu[key($menu)]);}
	}
}

add_action('admin_menu', 'remove_admin_menu_items');

/*
	=======================================
	Set base url
	=======================================
*/
function base_url(){
	$url = $_SERVER['SERVER_NAME'];

	$base_url = strpos($url, 'local') ? 'http://localhost:8080' : get_template_directory_uri();

	return $base_url;
}

/*
	=======================================
	Get base url + http/https
	=======================================
*/
function full_Base_url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    null
  );
}


/*
	=======================================
	Add [more..] with link to content excerpt
	=======================================
*/

function new_excerpt_more( $more ) {
    return ' <a class="post-text-more" href="'.get_permalink().'">[more...]</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/*
	=======================================
	Add scripts and css
	=======================================
*/
function add_theme_scripts() {

	wp_enqueue_script( 'picturefill.min', 'https://cdnjs.cloudflare.com/ajax/libs/picturefill/3.0.3/picturefill.min.js', false, 1.1, true);
	
	function map_script() {
		wp_enqueue_script( 'map.bundle', base_url() . '/build/map.bundle.js', false, 1.1, true);
	}


	if (!is_single() && get_post_type() !== 'deal' && !is_page_template('deals.php')) {
		wp_enqueue_script( 'main.bundle', base_url() . '/build/main.bundle.js', false, 1.1, true);
	}

	if (is_single() && get_post_type() !== 'deal' && !is_page_template('deals.php')) {
		wp_enqueue_script( 'product.bundle', base_url() . '/build/product.bundle.js', false, 1.1, true);
	}

	if (is_page_template('itineraries.php')) {
		wp_enqueue_script( 'itineraries.bundle', base_url() . '/build/itineraries.bundle.js', false, 1.1, true);
	}

	if (is_page_template('calendar.php')) {
		wp_enqueue_script( 'calendarApp.bundle', base_url() . '/build/calendarApp.bundle.js', false, 1.1, true);
	}

	if (get_post_type() === 'deal' || is_page_template('deals.php')) {
		wp_enqueue_script( 'deals.bundle', base_url() . '/build/deals.bundle.js', false, 1.1, true);
	}

	if (base_url() != 'http://localhost:8080') {
		wp_enqueue_style( 'site', get_template_directory_uri() . '/build/site.css' );
	}

	if (is_search()) {
		wp_enqueue_script( 'searchListingApp.bundle', base_url() . '/build/searchListingApp.bundle.js', false, 1.1, true);
	}
	
	// if (is_page_template('post-list.php')) {
		wp_localize_script( 'main.bundle', 'ajax_list_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	// }
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );



/*
	=======================================
	Support Featured Images
	=======================================
*/
add_theme_support( 'post-thumbnails' );



/*
	=======================================
	Assign product template to custom post types
	=======================================
*/
add_filter( 'single_template', function( $template ) {

    $my_types = array( 'accommodation', 'attraction' );
    $post_type = get_post_type();

    if ( ! in_array( $post_type, $my_types ) )
        return $template;

    return get_stylesheet_directory() . '/single-product.php'; 
});



/*
	=======================================
	Limit excerpt length
	=======================================
*/
function custom_excerpt_length( $length ) {
        return 20;
	}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_trim_excerpt($text) { // Fakes an excerpt if needed
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$excerpt_length = 1;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, '[...]');
			$text = implode(' ', $words);
		}
	}
	return $text;
}
add_filter('get_the_excerpt', 'custom_trim_excerpt');



/*
	=======================================
	Remove welcome widget
	=======================================
*/
remove_action('welcome_panel', 'wp_welcome_panel');



/*
	=======================================
	Get rid of unneccessary scripts
	=======================================
*/
function my_deregister_scripts() {
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );



/*
	=======================================
	Remove query string from static files
	=======================================
*/
function remove_cssjs_ver( $src ) {
	if( strpos( $src, '?ver=' ) )
	$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );



/*
	=======================================
	Add tag support to pages
	=======================================
*/
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}



/*
	=======================================
	Ensure all tags are included in queries
	=======================================
*/
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}



/*
	=======================================
	Add categories to pages
	=======================================
*/
function categories_for_pages(){
    register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'categories_for_pages' );


/*
	=======================================
	Add custom post types to main query
	=======================================
*/
function custom_post_type_cat_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_category()) {
      $query->set( 'post_type', array( 'post', 'accommodation', 'product', 'event' ) );
    }
  }
}

add_action('pre_get_posts','custom_post_type_cat_filter');

/*
	=======================================
	Includes
	=======================================
*/

require get_template_directory() . '/includes/menus.php';
require get_template_directory() . '/includes/image-helpers.php';
require get_template_directory() . '/includes/acf-setup.php';
require get_template_directory() . '/includes/remove-stuff.php';
require get_template_directory() . '/includes/post-types.php';
require get_template_directory() . '/includes/taxonomies.php';
require get_template_directory() . '/includes/pagination.php';
require get_template_directory() . '/includes/searchwp-live-ajax-search.php';
require get_template_directory() . '/includes/priority-sort.php';
require get_template_directory() . '/includes/helpers.php';
require get_template_directory() . '/includes/post-list-ajax.php';
// require get_template_directory() . '/includes/events-expire.php';
// require get_template_directory() . '/includes/calendar-helper.php';
require get_template_directory() . '/includes/remote-events-end-point.php';
require get_template_directory() . '/includes/search-end-point.php';
require get_template_directory() . '/includes/calendar-end-point.php';
require get_template_directory() . '/includes/itineraries-end-point.php';
require get_template_directory() . '/includes/deals.php';
require get_template_directory() . '/includes/map-end-points.php';
