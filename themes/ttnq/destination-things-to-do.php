<?php
/**
 * Template Name: Destination things to do
 */

global $post;

$page_slug = $post->post_name;

$category = get_the_category();

$first_category = $category[0]->slug;

$theme_colour = get_field('theme_colour');

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-things/hero', 'things') ?>

<style>

	.tip-icon {
		fill: #<?php echo $theme_colour ?>;
	}

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<?php if (have_posts()) :?>
<section id="description-1">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php
			have_posts();
			    while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
			    <?php
			    endwhile;
			// wp_reset_query();
			?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if( have_rows('tips') ): ?>
<section id="tips" class="collapse-bottom">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<h2>Tips</h2>
		</div>
	</div>
	<div class="row">
		<ul class="list-reset tips-list">

			<?php

			    while ( have_rows('tips') ) : the_row();?>

				<li class="small-12 medium-6 columns">
					<?php get_template_part('components/tip-tile/tip', 'tile') ?>
				</li>
			   <?php endwhile;

			?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php 
// dynamic product tile listing. 8 tiles.
$args = array(
	'post_type' 	=> 'product',
	'category_name' => $first_category,
	'post_status'	=> 'publish',
	'order'			=> 'DESC',
	'no_found_rows'	=> true,
	'update_post_term_cache' => false,
	'posts_per_page' => 8,
);

$query = new WP_Query($args);

$posts = $query->posts;

if ($posts && count($posts) >= 4) : ?>
<section id="explore-attractions">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('explore_text'); ?>
		</div>
	</div>
	<div class="row show-more-container">
		<div class="small-12 show-more-container-inner">

			<ul class="list-reset attractions-list clearfix">

			<?php

				foreach( $posts as $post ):
					
					setup_postdata( $post ); ?>
						
					<li class="small-6 medium-6 large-3 columns">
						<?php get_template_part('components/custom-post-tile/custom-post', 'tile') ?>
					</li>

				<?php endforeach; ?>

			<?php wp_reset_postdata(); ?>

			</ul>
			
		</div>
	</div>
	<?php
	$things_URL = add_query_arg(
		array(
			'post-type' => 'product',
			'category_name' => $first_category,
			), get_site_url() . '/post-list/' 
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost btn-margin" href="<?php echo $things_URL; ?>">view all things to do in <?php echo $first_category; ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>

</section>
<?php endif; ?>

<?php if (get_field('related_full_width_bg')): ?>
<section id="full-width-related" class="collapse">
	<div class="full-width-related clearfix text-center" data-responsive-background-image >
		<img <?php responsive_image(get_field('related_full_width_bg'), 'full') ?> >
		<div class="full-width-related-content">
			<?php the_field('related_pages_full_width_text'); ?>
			<a class="btn btn-medium btn-ghost lite-blue btn-margin" href="<?php the_field('related_pages_full_width_url'); ?>"><?php the_field('related_pages_button_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_footer()?>