<?php
/**
 * Single Post Template: Itinerary
 */

$theme_colour = get_field('theme_colour');

// Overview Information
$itinerary_type = get_field('itinerary_type');
$is_inspirational = $itinerary_type == 'inspirational' ? true : false;
$distance = get_field('distance');
$transport = get_field('mode_of_transport');
$location = get_field('location');
$date = get_field('date');
$map = get_field('map');

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-itinerary/hero-itinerary'); ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	#itinerary .btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
		color: #ffffff;
	}

	#itinerary a {
		border: 0;
		color: #<?php echo $theme_colour ?>;
	}
	#itinerary a:hover,
	#itinerary .itinerary-section a {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.day-top a {
		border-bottom: solid 2px #<?php echo $theme_colour ?>!important;
	}

	.itinerary-icon {
		fill: #<?php echo trim($theme_colour) ?>;
	}

	#itinerary .row {
		max-width: 1080px;
	}

	.itinerary-start .itinerary-path {
		border-bottom: 3px solid #<?php echo trim($theme_colour) ?>;
	}

	#itinerary .itinerary-scroll-nav-list li a {
		border: 3px solid #<?php echo trim($theme_colour) ?>!important;
	}

	#itinerary .itinerary-scroll-nav-list li a:hover {
		border: 3px solid #<?php echo trim($theme_colour) ?>!important;
		background-color: #<?php echo trim($theme_colour) ?>;
	}

	.sites-wrapper:before,
	.sites-wrapper:after,
	.sites-content-icon {
		background-color: #<?php echo trim($theme_colour) ?>;
	}

	.itinerary-line:before,
	.itinerary-scroll-nav-list li.active a {
		background-color: #<?php echo trim($theme_colour) ?>;
	}

	.itinerary-distance-left,
	.itinerary-distance-right,
	h3,
	.itinerary-scroll-nav-key,
	.itinerary-scroll-nav-more {
		color: #<?php echo trim($theme_colour) ?>;
	}

	#dashed-line {
		stroke: #<?php echo trim($theme_colour) ?>; stroke-width:100%;
		width: 4px;
		height: 100%;
	}

    .map-nav__cat {
        background: transparent;
    }
	
</style>

<div id="itinerary">
	
	<?php include(locate_template('/components/itineraries/itinerary/overview.php')); ?>

    <section class="map-section theme-coral">
        <?php get_template_part('components/map/map'); ?>
        <?php map_script(); ?>
    </section>

	<?php include(locate_template('/components/itineraries/itinerary/scroll-nav.php')); ?>

	<div class="itinerary-start">
		<div class="row">
			<div class="itinerary-path">
				<?php if (!$is_inspirational): ?>
				<svg class="itinerary-icon"  role="presentation">
					<use xlink:href="<?php echo get_template_directory_uri()?>/images/blog-sprite.svg#<?php echo $transport['value']; ?>"></use>
	            </svg>
	        	<?php endif; ?>
	            <div class="itinerary-line">
	            	<svg id="dashed-line">
						<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
					</svg>
	            </div>
	            <div class="itinerary-distance">
	            	<span class="itinerary-distance-left"></span>
					<span class="itinerary-distance-right"></span>
	            </div>
			</div>
		</div>
	</div>

	<?php if( have_rows('day') ): ?>
		<?php $day_index = 0; ?>
		<?php while ( have_rows('day') ) : the_row(); ?>
			<?php if( get_row_layout() == 'day_section' ): ?>
			<?php $day_index++ ?>

        		<?php include(locate_template('/components/itineraries/itinerary/day-section.php')); ?>

        	<?php elseif( get_row_layout() == 'detour_section' ): ?>
	
				<?php include(locate_template('/components/itineraries/itinerary/detour.php')); ?>

			<?php endif; ?>

		<?php endwhile; ?>

	<?php endif; ?>


<?php get_template_part('components/article-tile/featured-articles'); ?>

<?php get_template_part('components/full-width-banner/full-width-banner'); ?>

</div>

<script>
	var itinerarySettings = {
		color: '#<?php echo $theme_colour ?>',
		id: <?php echo $post->ID; ?>,
		type: '<?php echo $itinerary_type; ?>',
		highlights: [
			<?php if( have_rows('highlights') ): ?>
				<?php while ( have_rows('highlights') ) : the_row(); ?>
				    "<?php echo get_sub_field('highlight') ?>",
				<?php endwhile; ?>
			<?php endif; ?>
		],
		<?php if(!$is_inspirational): ?>
			iconNames: ['distance', '<?php echo $transport['value']; ?>', 'where', 'when'],
			icons: <?php echo file_get_contents(get_template_directory().'/images/itinerary/icons.js'); ?>,
		<?php endif; ?>
		<?php if($map): ?>
			map: '<?php echo base_64_convert(get_field('map')); ?>'
		<?php endif; ?>
	};
</script>

<?php get_footer()?>