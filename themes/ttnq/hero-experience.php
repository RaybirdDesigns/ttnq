<?php
/**
 * Template Name: Hero experience
 *
 * Used by: Hero experience pages and Landing pages
 */
$category = get_the_category();

$first_category = !empty($category) ? $category[0]->slug : null;

$theme_colour = get_field('theme_colour');

$is_landing_page = get_field('is_landing_page') == true ? true : false;

// influencer background colour
switch($theme_colour) {
	case '639083' :
		$influencer_bg_colour = '274f2c';
		break;
	case 'CE975C' : 
		$influencer_bg_colour = '2f2920';
		break;
	case '6fa0ae' :
		$influencer_bg_colour = '1f5668';
		break;
	default:
		$influencer_bg_colour = '2f2920';
}

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-experience/hero', 'experience'); ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.feature-narrative-tile a {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px transparent;
	}

	.feature-narrative-tile a:hover {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.feature-narrative-tile a:focus {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	#influencers h3,
	#influencers p {
		color: #<?php echo $theme_colour ?>;
	}

	button.scroll-btn {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	#influencers {
		background-color: #<?php echo $influencer_bg_colour; ?>
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<?php
// Non landing page Ad banner position
if ($is_landing_page):
	// Show ad unit based on category
	// randomised if more than one
	get_template_part('components/ad-units/dyn-banner-wrapper');
endif; ?>

<?php if(get_field('hero-experience-text')): ?>
<section id="description-1">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('hero-experience-text'); ?>
		</div>
	</div>
	<div class="row component">
		<div class="small-12 columns text-center">

			<?php if(!$is_landing_page): ?>
			<button class="btn btn-medium btn-ghost btn-margin scroll-btn scroll-down-btn-js" data-scroll-to="related-pages">Show activities <i class="fa fa-chevron-down" aria-hidden="true"></i></button>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php if( have_rows('feature') ): ?>
<section id="feature-narrative" class="collapse">
	<div class="clearfix">
		<?php

		$feature_index = 0;

	    while ( have_rows('feature') ) : the_row();

	    	$feature_index++;

	        include(locate_template('/components/feature-narrative-tile/feature-narrative-tile.php'));

	    endwhile;

		?>
	</div>
</section>
<?php endif; ?>

<?php
// Non landing page Ad banner position
if (!$is_landing_page):

	// Show ad unit based on category
	// randomised if more than one
	$args = array(
	    'post_type'     => 'ad_unit',
	    'category_name' => $first_category,
	    'post_status'   => 'publish',
	    'orderby' 		=> 'rand',
	    'order'         => 'DESC',
	    'posts_per_page' => '1',
	    'no_found_rows' => true,
	    'update_post_term_cache' => false,
	    'posts_per_page' => 1,
	    'paged'          => false,
	);

	$wp_query = new WP_Query($args);

	$posts = $wp_query->posts;

	if ( have_posts() ) : ?>
	<section id="banner-1" class="collapse">
		<div class="small-12">

			<?php foreach( $posts as $post ): 
			
			setup_postdata( $post );
			
			?>
			
			<?php get_template_part('components/ad-units/banner', 'ad') ?>

			<?php endforeach; ?>
						
		</div>
	</section>
<?php
	endif;
	wp_reset_query();
endif;
?>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<?php if ( have_rows('related_pages') ): ?>
<section id="related-pages" class="collapse-bottom">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns text-center component">
			<?php the_field('related_page_text'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="list-reset">
			<?php

				while ( have_rows('related_pages') ) {

				the_row();

				$post_object = get_sub_field('featured_page');

				if ( $post_object ) {

					$post = $post_object;
					setup_postdata( $post_object );

					?>
					
					<li class="small-12 medium-6 large-3 float-left">
						<?php get_template_part('components/image-title-tile/image-title', 'tile') ?>
					</li>
					
		<?php		wp_reset_postdata();
				}
			}
		?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php if(get_field('video_btn_url')) : ?>
<section id="video">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 medium-6 columns">
			<?php the_field('video_text'); ?>

			<?php if(get_field('video_btn_url')) : ?>
			<a class="btn btn-medium btn-ghost blue btn-margin" href="<?php the_field('video_btn_url'); ?>"><?php the_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php if(!empty(get_field('influencers_post'))): ?>
<section id="influencers">
	<div class="row">
		<div class="small-12 columns text-center">
			<h2>#exploreTNQ</h2>
		</div>
	</div>
	<div class="clearfix">
		<?php

			$post_object = get_field('influencers_post');

			if ( $post_object ) {

				$post = $post_object;
				setup_postdata( $post );
			?>

			<?php get_template_part('components/influencer-post/influencer', 'post') ?>
						
		<?php	wp_reset_postdata();
			}
		?>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('components/full-width-banner/full-width-banner'); ?>

<?php get_footer()?>