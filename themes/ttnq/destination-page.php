<?php
/**
 * Template Name: Destination
 */

$theme_colour = get_field('theme_colour');

$category = get_the_category();

$first_category = $category[0]->slug;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-destination/hero', 'destination') ?>

<style>

	.tip-icon {
		fill: #<?php echo $theme_colour ?>;
	}

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	button.scroll-btn {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff;
	}

	.feature-narrative-tile a,
	#getting-here a {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px transparent;
	}

	.feature-narrative-tile a:hover,
	#getting-here a:hover,
	.feature-narrative-tile a:focus,
	#getting-here a:focus {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<section id="description-1">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('destination_text'); ?>
		</div>
	</div>
	<div class="row component">
		<div class="small-12 columns text-center">
			<button class="btn btn-medium btn-ghost scroll-btn scroll-down-btn-js btn-margin" data-scroll-to="related-pages">Show activities <i class="fa fa-chevron-down" aria-hidden="true"></i></button>
		</div>
	</div>
</section>

<section id="feature-narrative" class="collapse">
	<div class="clearfix">
		<?php
		if( have_rows('feature') ):

			$feature_index = 0;

		    while ( have_rows('feature') ) : the_row();

		    	$feature_index++;

		        include(locate_template('/components/feature-narrative-tile/feature-narrative-tile.php'));

		    endwhile;

		endif;
		?>
	</div>
</section>

<?php
// Show ad unit based on category
// randomised if more than one
get_template_part('components/ad-units/dyn-banner-wrapper');
?>

<?php if( have_rows('tips') ): ?>
<section id="tips" class="collapse-bottom">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<h2>Tips</h2>
		</div>
	</div>
	<div class="row">
		<ul class="list-reset tips-list">

			<?php

			    while ( have_rows('tips') ) : the_row();?>

				<li class="small-12 medium-6 columns">
					<?php get_template_part('components/tip-tile/tip', 'tile') ?>
				</li>
			   <?php endwhile;

			?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<section id="related-pages" class="collapse-bottom">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns text-center component">
			<?php the_field('related_page_text'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="list-reset">
			<?php
			if ( have_rows('related_pages') ) {

				while ( have_rows('related_pages') ) {

					the_row();

					$post_object = get_sub_field('featured_page');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object );

						?>
						
						<li class="small-12 medium-6 large-3 float-left">
							<?php get_template_part('components/image-title-tile/image-title', 'tile') ?>
						</li>
					
		<?php			wp_reset_postdata();
					}
				}
			}
		?>
		</ul>
	</div>
</section>

<?php include(locate_template('/components/itineraries/itineraries-carousel/itineraries-carousel.php')); ?>

<?php
/*
	Set dynamic events tiles with custom override
*/

// set vars
$tile_spaces = 5;
$selected_post_count = 0;
$row_name = 'featured_events';
$subfield_name = 'featured_event';

// count number of selected posts
if ( have_rows($row_name) ) {

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {
		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// only run priority sort and merge if less than $tile_spaces
if ($selected_post_count > 0) {

	// calculate number of pages needed from event priority sort query
	$sort_posts_per_page = $tile_spaces - $selected_post_count;

	if ($sort_posts_per_page > 0) {
		// event priority sort query
		$priority_wp_query = priority_event_sort($sort_posts_per_page, false, $first_category, null);
		$priority_wp_query_posts = $priority_wp_query->posts;

		// merge the two arrays together
		$posts = array_unique(array_merge($clean_selected_posts_obj, $priority_wp_query_posts), SORT_REGULAR);
		
	} else {
		$posts = $clean_selected_posts_obj;
	}

} else {

	$priority_wp_query = priority_event_sort($tile_spaces, false, $first_category, null);
	$priority_wp_query_posts = $priority_wp_query->posts;
	$posts = $priority_wp_query_posts;
}

$posts = array_slice($posts, 0, 5);

?>

<?php if (is_user_logged_in() && count($posts) < $tile_spaces): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Events section doesn't have enough posts to be visible, <?php echo $tile_spaces;  ?> posts are required. Posts dynamically found: <?php echo count($priority_wp_query_posts); ?>. Posts authored: <?php echo $selected_post_count; ?>.</p>
</div>
<?php endif; ?>

<?php //change !== to == to reinitialise on site
if ($posts && count($posts) !== $tile_spaces): ?>
<section id="events">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<?php the_field('events_text'); ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php include(locate_template('/components/tiles-5-up/tiles-5-up.php')); ?>
		</div>
	</div>
	<?php
	$events_URL = add_query_arg(
		array(
			'post-type' => 'event',
			'category_name' => $first_category,
			), get_site_url() . '/post-list/' 
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost green btn-margin" href="<?php echo $events_URL; ?>">Browse all events <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>
<?php endif; ?>

<?php if (have_rows('getting_here')) : ?>
<section id="getting-here" class="collapse">
	<div class="full-width-getting-here clearfix">
		<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('getting_here_bg_image'), 'full', true); ?> >
		<div class="row full-width-getting-here-wrapper">
			<div class="clearfix component">
				<div class="small-12 columns text-center">
					<h2>Getting here</h2>
				</div>
			</div>
			<div class="clearfix">
				<ul class="list-reset getting-here-list">

				<?php while ( have_rows('getting_here') ) : the_row();?>

					<li class="small-12 medium-6 columns">
						<div class="getting-here-image">
							<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_sub_field('image'), 'large', true); ?> >
						</div>
						<?php the_sub_field('text'); ?>
					</li>

				<?php endwhile; ?>
				
				</ul>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php

/*
	Set dynamic accommodation tiles with custom override
*/

// set vars
$row_name = 'accomodation';
$subfield_name = 'featured_accomodation';
$selected_post_count = 0;

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);

	// clean the acf array, remove the 'accomodation' key.
	if (is_array($selected_posts_obj)) {
		
		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// accommodation priority sort query
// priority_sort($posts_per_page, $paged, $post_type, $category, $tag)
$priority_wp_query = priority_sort(-1, false, 'accommodation', $first_category, null);
$priority_wp_query_posts = $priority_wp_query->posts;

// merge the two arrays together
if ($selected_post_count > 0) {

	$posts = array_unique(array_merge($clean_selected_posts_obj, $priority_wp_query_posts), SORT_REGULAR);
} else {

	$posts = $priority_wp_query_posts;
}

?>
<?php if (is_user_logged_in() && !$posts): ?>
<div class="tnq-notification">
	<p>The Accommodation section doesn't have enough posts to be visible.</p>
</div>
<?php endif; ?>

<?php if ( have_posts()): ?>
<section id="stay" class="show-more-section">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('accomodation_text'); ?>
		</div>
	</div>
	<div class="row show-more-container">
		<div class="small-12 show-more-container-inner">
			<?php include(locate_template('/components/tiles-4-up/tiles-4-up.php')); ?>
		</div>
	</div>

	<?php if (count($posts) > 8) : ?>
	<div class="row text-center">
		<button class="btn btn-medium btn-ghost btn-margin show-more-btn">See more</button>
	</div>
	<?php endif; ?>

</section>
<?php endif; ?>

<?php if (get_field('stackla_html_snippet')) : ?>
<?php include(locate_template('/components/stackla/stackla.php')); ?>
<?php endif; ?>

<?php get_footer()?>