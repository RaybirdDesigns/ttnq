<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
?>

<?php get_header(); ?>

<?php
// get thumbnamil
// gets base64 encoded and used in the before pseudo element
$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];

?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>



<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

</div>

<section id="missing" class="missing">
    <div class="row">
        <div class="small-12 columns text-center">
            <h1 class="pagetitle">404</h1>
            <h4 class="text-center">Try searching?</h4>
            <div class="search-form-wrapper">
                <svg class="search-ico" role="presentation">
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#search"></use>
                </svg>
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <label>
                        <span class="show-for-sr">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search Tropical North Queensland" value="" name="s" title="Search for:" />
                    </label>
                </form>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>