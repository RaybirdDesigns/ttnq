<?php
/**
 * Template Name: Category
 */

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
    $thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
    $thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

?>

<?php get_header(); ?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<?php if (have_posts()) : ?>

    <div id="hero-banner" class="search-hero" data-responsive-background-image >

        <?php if (has_post_thumbnail( $post->ID )) : ?>

            <img class="hero-img" <?php responsive_image_post('full') ?> >

        <?php else : ?>

           <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

        <?php endif; ?>

        <div class="search-hero-content">
            <h1 class="pagetitle"><?php single_tag_title(); ?></h1>
        </div>
        <div class="hero-bottom-gradient"></div>
    </div>

    <section id="post-list">
        <div class="row">
            <div class="small-12 columns">

                <ul class="list-reset">
                <?php while (have_posts()) : the_post(); ?>

                    <?php $post_image = has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' )[0] : null; ?>

                    <li class="post" id="post-<?php the_ID(); ?>">
                        <div class="post-image" style="background-image: url('<?php echo $post_image ?>');"></div>
                        <div class="post-text">
                            <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h4>
                        <?php

                        if ( function_exists('the_excerpt') ) {
                            the_excerpt();
                        } ?>
                        </div>  
                    </li>
                    
                <?php endwhile; ?>
                </ul>
                <?php echo numeric_posts_nav(); ?>
            </div>
        </div>    
    </section>

             
<?php endif; ?>


<?php get_footer(); ?>