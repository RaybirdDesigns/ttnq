<?php
/**
 * Single Post Template: Deal
 */

?>

<?php get_header('landing-page')?>
<?php 
	/* grab the url for the full size featured image */
	$featured_img_url = get_the_post_thumbnail_url($post->ID,'large'); 
				
	$post_title = $post->post_title;
	$provider = get_field('provider', $post->ID);
	$location = get_field('location', $post->ID);
	$cta = get_field('cta', $post->ID);
	$promo_code = get_field('promo_code', $post->ID);
	$terms = get_field('terms', $post->ID);
	$slug = get_post_field( 'post_name', $post->ID );
?>
<!-- Individual Deal Page -->
	<section id="individual-deal">
		<div class="row desktop">
			<div class="small-12 columns">
				<a href="javascript:history.back();" class="back-to"><i class="fa fa-angle-left" aria-hidden="true"></i> <span>Back to more holiday deals</span></a>
			</div>
		</div>
		
		<div class="row details-container">		
			<div class="large-7 columns">
				<img alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
			</div>
			<div class="large-5 columns">
				<div class="grid-details">
					<h4 class="prod-name"><?php echo $post_title; ?></h4>
					<h5 class="org-name"><strong><?php echo $provider; ?></strong></h5>
					<?php if($location): ?><span class="location-icon"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?></span><?php endif; ?>						
					<p class="product-desc">
						<?php echo $post->post_content; ?>
					</p>				
				</div>
				<div class="book-container">
					<div class="small-6 columns book-btn">
						<a href="<?php echo $cta; ?>" id="<?php echo $slug; ?>" target="_blank" class="book-now">Book Now</a>
					</div>
					<div class="small-6 columns">
						<?php if($promo_code) : ?><div class="promo-code-container">
							<span>Use Promo Code:</span>
							<strong><?php echo $promo_code; ?></strong>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>	
		</div>
		<div class="row mobile">
			<div class="small-12 columns">
				<a href="javascript:history.back();" class="back-to"><i class="fa fa-angle-left" aria-hidden="true"></i> <span>Back to more holiday deals</span></a>
			</div>
		</div>
	</section>
	<section class="bottom-content">
		<div class="row terms-container">
			<div class="small-12 columns">
				<a href="javascript:void(0);" class="terms"><span>Terms and conditions</span> <i class="fa fa-angle-up" aria-hidden="true"></i></a>
			</div>
		</div>
	</section>
	<section class="bottom-content">
		<div class="row terms-container">
			<div class="small-12 columns">
				
			<p><strong>Please check operator's website for full terms and conditions.</strong></p>
			<?php echo $terms; ?>
			</div>
		</div>
	</section>
<!-- End Individual Deal Page -->


<?php get_footer('landing-page')?>