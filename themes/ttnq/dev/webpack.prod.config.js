const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const wpTemplateName = 'ttnq';

module.exports = {
	entry: {
		main: './main.js',
		product: './product.js',
		itineraries: './itineraries.js',
		searchListingApp: './searchListingApp.js',
		calendarApp: './calendarApp.js',
		itineraryPdf: './itineraryPdf.js',
		deals: './deals.js',
		map: './map.js',
		googlemap: './googlemap.js',
	},
	output: {
		path: __dirname,
		filename: '../build/[name].bundle.js',
	},
	devtool: 'eval-cheap-module-source-map',
	module: {
		rules: [
			{
				test: /\.(scss|css)$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader?minimize=true', 'sass-loader', 'postcss-loader'],
				}),
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['babel-preset-env'],
				},
			},
			{
				test: require.resolve('jquery'),
				use: [
					{
						loader: 'expose-loader',
						options: '$',
					},
				],
			},
			{
				test: /\.(png|jpeg|jpg|gif|svg)$/,
				loader: 'url-loader',
				options: {
					limit: 1,
					name: 'images/[name].[ext]',
					publicPath: `/wp-content/themes/${wpTemplateName}`,
				},
			},
			{
				test: /\.(woff|woff2|eot|ttf)$/,
				loader: 'url-loader',
				options: {
					limit: 1,
					name: 'fonts/[name].[ext]',
					publicPath: `/wp-content/themes/${wpTemplateName}`,
				},
			},
		],
	},
	performance: {
		hints: false,
	},
	plugins: [
		new ExtractTextPlugin('../build/site.css'),
		new webpack.DefinePlugin({
			__DEVTOOLS__: false,
		}),
	],
};
