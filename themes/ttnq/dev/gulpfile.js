const gulp = require('gulp');
const gulpBase64 = require("gulp-to-base64");
const raster = require('gulp-raster');
const rename = require('gulp-rename');
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const path = require('path');

/* ---------------------------------------------------------------
    SVG spriting
------------------------------------------------------------------*/

gulp.task('svg-global', () => {
    return gulp
        .src('./images/svg-icons/global-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: false,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images'));
});

gulp.task('svg-features', () => {
    return gulp
        .src('./images/svg-icons/features-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: false,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images'));
});

gulp.task('svg-blog', () => {
    return gulp
        .src('./images/svg-icons/blog-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: false,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images'));
});

gulp.task('svg-tips', () => {
    return gulp
        .src('./images/svg-icons/tips-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: false,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images'));
});

gulp.task('svg-site', () => {
    return gulp
        .src('./images/svg-icons/site-sprite/*.svg')
        .pipe(
            svgmin(file => {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [
                        {
                            cleanupIDs: {
                                prefix: prefix + '-',
                                minify: false,
                            },
                        },
                    ],
                };
            }),
        )
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('../images'));
});

// Generates base64s for itineraries icons
gulp.task('svg-png', () => {
    return gulp
        .src('./images/svg-icons/blog-sprite/*.svg')
        .pipe(raster({format: 'png', scale: 1}))
	    .pipe(rename({extname: '.png'}))
	    .pipe(gulp.dest('../images/itinerary'))
        .pipe(gulpBase64({
            size: false,
            outPath:"../images/itinerary/icons.js"
        }));
});

gulp.task('svg', () => {
    gulp.start('svg-global');
    gulp.start('svg-blog');
    gulp.start('svg-features');
    gulp.start('svg-tips');
    gulp.start('svg-site');
    gulp.start('svg-png');
});
