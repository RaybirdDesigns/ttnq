// https://github.com/henrygd/bigpicture
import BigPicture from 'bigpicture';

var videoLinks = document.querySelectorAll('.lightbox-video .cinemagraph-wrapper');
for (var i = 0; i < videoLinks.length; i++) {
	videoLinks[i].addEventListener('click', function(e) {
		e.preventDefault();

		console.log($(this).data('bp'));

		BigPicture({
			el: this,
			vimeoSrc: $(this).data('bp'),
		});
	});
}