// https://github.com/henrygd/bigpicture
import BigPicture from 'bigpicture';

var imageLinks = document.querySelectorAll('.lightbox-image');
for (var i = 0; i < imageLinks.length; i++) {
	imageLinks[i].addEventListener('click', function(e) {
		e.preventDefault();

		BigPicture({
			el: this,
			imgSrc: e.target.dataset.bp,
		});
	});
}