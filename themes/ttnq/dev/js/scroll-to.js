import $ from 'jquery';
import 'gsap/ScrollToPlugin';

// Scroll to element
// compensated for fixed header
const $scrollBtn = $('.scroll-down-btn-js');
const $stickyNav = $('#sticky-nav');

$scrollBtn.on('click', function(e) {
	let $this = $(this);
	let sectionId = `#${$this.data('scroll-to')}`;

	let theOffSet = $stickyNav.length > 0 ? 65 : 0;

	TweenLite.to(window, 1.2, { scrollTo: { y: sectionId, offsetY: theOffSet, ease: Power1.easeInOut } });

	e.preventDefault();
});
