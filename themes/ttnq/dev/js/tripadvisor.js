import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';

// This is used to grab the href out of the
// trip advisor widget and put it in the template

function relocateHref(srcTarget, destTarget) {
	const srcHref = $(srcTarget)
		.find('a')
		.attr('href');

	const destHref = document.querySelector(destTarget);

	destHref.setAttribute('href', srcHref);
}

// clone tripadvisor rating element and copy to hero
// remove id so no duplicates
function relocateRating(srcContainer, targetContainer) {
	const $srcEl = $(srcContainer);
	const $destEl = $(targetContainer);

	const clonedSrcEl = $srcEl.clone().removeAttr('id');
	const iconTemplate = '<i class="fa fa-tripadvisor brand" aria-hidden="true"></i>';

	$destEl.html(clonedSrcEl);

	// prepend owl head to comply with tripadvisor brand guidelines
	$destEl.find('.cdsComponent').prepend(iconTemplate);
}

// poll for the trip advisor widget to load
// then run relocateHref
let pollForTripAdvisor = () => {
	if (document.getElementById('CDSRATINGWIDGET474')) {
		relocateRating('#CDSROWRATING474', '#trip-advisor-hero-rating');
		relocateHref('#CDSROWLOC474', '#tripadvisor-reviews-link');
		document.getElementById('TA_cdsratingsonlynarrow474').classList.remove('hide');

		return;
	}
	setTimeout(pollForTripAdvisor, 200);
};
pollForTripAdvisor();
