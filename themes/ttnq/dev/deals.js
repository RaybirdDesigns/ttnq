import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';

import 'slick-carousel/slick/slick.scss';
import './main.scss';
// import deals scripts
import './js/lazysizes.js';
import './components/deals/deals.js';
import './components/carousel/article-carousel.js';
import './components/article-tile/article-tile.js';
import './components/hero-banners/video-bg.js';
import './components/hero-banners/hero-banner.js';
import './components/header/header-search.js';
import './components/footer/footer.js';
import './components/main-menu/main-menu.js';
import './components/sticky-nav/sticky-nav.js';
import './components/show-more/show-more.js';