import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import enquire from 'enquire.js';

// import css
import 'slick-carousel/slick/slick.scss';
// import 'slick-carousel/slick/slick-theme.scss';
import './main.scss';

// import js
import 'intersection-observer';
import 'object-fit-polyfill';
import 'nodelist-foreach-polyfill';
import './js/scroll-to.js';
import './js/big-picture.js';
import './js/lazysizes.js';
import './components/carousel/article-carousel.js';
import './components/article-tile/article-tile.js';
import './components/carousel/influencer-carousel.js';
import './components/carousel/prod-carousel.js';
import './components/header/header-search.js';
import './components/footer/footer.js';
import './components/hero-banners/video-bg.js';
import './components/hero-banners/hero-banner.js';
import './components/main-menu/main-menu.js';
import './components/region-switcher/region-switcher.js';
import './components/itineraries/itinerary/day-section.js';
import './components/itineraries/itinerary/scroll-nav.js';
import './js/tripadvisor.js';

// Async load itinerary PDF script
const $pdfButton = $('.itineraries-pdf');
$pdfButton.on('click', () => {

	const url = location.host === location.host.indexOf('local') > 0 ? 'http://localhost:8080/build/itineraryPdf.bundle.js' : `${location.origin}/wp-content/themes/ttnq/build/itineraryPdf.bundle.js`;

	if ($('#itineraryPdfScript').length === 0) {

		const script = document.createElement("script");
		
		$pdfButton.append(`<img src="${location.origin}/wp-content/themes/ttnq/images/loader.gif" />`);
	    
	    script.type = "text/javascript";
	    script.src = url;
	    script.id = 'itineraryPdfScript';
	    document.getElementsByTagName("head")[0].appendChild(script);

	    script.onload = function() {
	      window.itineraryPDFGen();
	    };
	} else {
		window.itineraryPDFGen();
	}

    return false;
});

// google maps setup
(function($) {
	/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

	function new_map($el) {
		// var
		var $markers = $el.find('.marker');

		// vars
		var args = {
			zoom: 16,
			center: new google.maps.LatLng(0, 0),
			mapTypeId: google.maps.MapTypeId.HYBRID,
			draggable: true,
			scrollwheel: false,
			mapTypeControl: false,
			streetViewControl: false,
		};

		// create map
		var map = new google.maps.Map($el[0], args);

		// add a markers reference
		map.markers = [];

		// add markers
		$markers.each(function() {
			add_marker($(this), map);
		});

		// center map
		center_map(map);

		enquire
			.register('(max-width: 1199px)', () => {
				map.panBy(0, 0);
			})
			.register('(min-width: 1200px)', () => {
				map.panBy(-300, -30);
			});

		// return
		return map;
	}

	/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

	function add_marker($marker, map) {
		// var
		var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

		var image = {
			url: `${location.origin}/wp-content/themes/ttnq/images/tnq-map-pin.png`,
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(69, 99),
			// The origin for this image is (0, 0).
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at (0, 32).
			//anchor: new google.maps.Point(0, 32)
		};

		// create marker
		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			icon: image,
		});

		// add to array
		map.markers.push(marker);

		// if marker contains HTML, add it to an infoWindow
		if ($marker.html()) {
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content: $marker.html(),
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, marker);
			});
		}
	}

	/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

	function center_map(map) {
		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each(map.markers, function(i, marker) {
			var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

			bounds.extend(latlng);
		});

		// only 1 marker?
		if (map.markers.length == 1) {
			// set center of map
			map.setCenter(bounds.getCenter());
			map.setZoom(10);
		} else {
			// fit to bounds
			map.fitBounds(bounds);
		}
	}

	/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
	// global var
	var map = null;

	$(document).ready(function() {
		$('.acf-map').each(function() {
			// create map
			map = new_map($(this));
		});
	});
})(jQuery);
