const webpack = require('webpack');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: {
        main: './main',
        product: './product',
        itineraries: './itineraries',
        searchListingApp: './searchListingApp.js',
        calendarApp: './calendarApp.js',
        itineraryPdf: './itineraryPdf.js',
        deals: './deals.js',
        map: './map.js'
    },
    output: {
        path: __dirname,
        publicPath: 'http://localhost:8080/',
        filename: '../build/[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: {
                                safe: true,
                            },
                        },
                    },
                    {
                        loader: 'sass-loader?outputStyle=expanded',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            autoprefixer: {
                                browsers: ['last 2 versions'],
                            },
                        },
                    },
                ],
            },
            {
                test: /\.js$/,
                loaders: ['babel-loader?presets[]=babel-preset-env'],
                exclude: /node_modules/,
                include: __dirname,
            },
            {
                test: require.resolve('jquery'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: '$',
                    },
                ],
            },
            {
                test: /\.(png|jpeg|jpg|ttf|...)$/,
                loader: 'url-loader',
            },
            {
                test: /\.woff$/,
                loader: 'url-loader',
                options: {
                    limit: 1,
                },
            },
        ],
    },
    devServer: {
        hot: true,
        quiet: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        },
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new FriendlyErrorsWebpackPlugin(),
    ],
};
