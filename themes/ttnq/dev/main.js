// import css
import 'slick-carousel/slick/slick.scss';
import './main.scss';

// import js
import  svg4everybody from 'svg4everybody';
import 'object-fit-polyfill';
import 'nodelist-foreach-polyfill';
import 'intersection-observer';
import './js/scroll-to.js';
import './js/big-picture.js';
import './js/lazysizes.js';
import './js/big-picture-video.js';
import './components/carousel/article-carousel.js';
import './components/article-tile/article-tile.js';
import './components/carousel/influencer-carousel.js';
import './components/carousel/map-carousel.js';
import './components/carousel/prod-carousel.js';
import './components/header/header-search.js';
import './components/footer/footer.js';
import './components/hero-banners/video-bg.js';
import './components/hero-banners/hero-banner.js';
import './components/main-menu/main-menu.js';
import './components/sticky-nav/sticky-nav.js';
import './components/event-search/event-search.js';
import './components/post-list/post-list.js';
import './components/custom-post-tile/custom-post-tile.js';
import './components/show-more/show-more.js';
import './components/region-switcher/region-switcher.js';
import './components/itineraries/itineraries-carousel/itineraries-carousel.js';

// fix for svg sprites in ie11
svg4everybody();
