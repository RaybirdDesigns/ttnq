import React, { Component } from 'react';

const Fragment = React.Fragment;

class SearchForm extends Component {
	constructor() {
		super();
		this.state = { inputValue: '' };
		this.clearForm = this.clearForm.bind(this);
		this.handleTextChange = this.handleTextChange.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.updateSearchTerm = this.updateSearchTerm.bind(this);
	}

	handleTextChange(event) {
		this.setState({ inputValue: event.target.value });
	}

	handleKeyUp() {
		this.updateSearchTerm();
	}

	clearForm() {
		this.setState({ inputValue: '' });
		this.input.focus();
	}

	updateSearchTerm() {
		const inputValue = this.state.inputValue;
		this.props.getNewSearchTerm(inputValue);
	}

	render() {
		return (
			<Fragment>
				<div role="search form" className="search-form">
					<label>
						<svg className="search-form-ico" role="presentation">
							<use xlinkHref="/wp-content/themes/ttnq/images/global-sprite.svg#search" />
						</svg>
						<span className="show-for-sr">Search for:</span>
						<input
							ref={input => (this.input = input)}
							onChange={this.handleTextChange}
							onKeyUp={this.handleKeyUp}
							type="search"
							className="search-form-field"
							placeholder="search..."
							value={this.state.inputValue}
							title="Search for:"
						/>
					</label>
				</div>
				<button
					onClick={() => {
						this.clearForm();
					}}
					className="search-clear-btn">
					<svg role="presentation">
						<use xlinkHref="/wp-content/themes/ttnq/images/global-sprite.svg#times" />
					</svg>
				</button>
			</Fragment>
		);
	}
}

export default SearchForm;
