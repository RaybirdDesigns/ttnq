import React from 'react';
import ReactDOM from 'react-dom';
import SearchListingApp from './SearchListingApp.js';

// render app to dom
ReactDOM.render(<SearchListingApp />, document.getElementById('search-listing-root'));
