import React, { Component } from 'react';

import SearchForm from './SearchForm';
import Filter from './Filter';

class SearchHeader extends Component {
	constructor() {
		super();
		this.renderFilter = this.renderFilter.bind(this);
	}

	renderFilter() {
		return <Filter getFilter={this.props.getFilter} filterTerm={this.props.filterTerm} />;
	}

	render() {
		const props = this.props;
		const searchTerm =
			props.filterTerm !== '' && props.filterTerm !== 'all'
				? `"${props.searchTerm}" in ${props.filterName}`
				: `"${props.searchTerm}"`;
		const renderTerm = searchTerm.length >= 3 ? searchTerm.replace('+', ' ') : '...';
		return (
			<section id="search-header" className="search-header">
				<div className="row">
					<div className="small-12 columns text-center">
						<h1>Search Results</h1>
						<p>
							Your search for <strong>{renderTerm}</strong> returned {props.foundPosts} results
						</p>
						<div className="search">
							<SearchForm getNewSearchTerm={props.getNewSearchTerm} />
						</div>
					</div>
				</div>
				{this.renderFilter()}
			</section>
		);
	}
}

export default SearchHeader;
