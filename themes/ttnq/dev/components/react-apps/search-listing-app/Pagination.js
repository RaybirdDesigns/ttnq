import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import 'gsap/ScrollToPlugin';

class Pagination extends Component {
	constructor() {
		super();
		this.handlePageClick = this.handlePageClick.bind(this);
	}

	handlePageClick(data) {
		let selected = data.selected;
		let offset = Math.ceil(selected + 1);

		TweenLite.to(window, 1.5, {
			scrollTo: { y: '#search-header', offsetY: 0, ease: Power1.easeInOut },
			onComplete: this.props.paginationOffset(offset),
		});
	}

	render() {
		const props = this.props;
		return (
			<div className="pagination-wrapper">
				<ReactPaginate
					previousLabel={''}
					nextLabel={''}
					breakLabel={<a href="">...</a>}
					breakClassName={'break-me'}
					pageCount={this.props.totalPages}
					marginPagesDisplayed={2}
					pageRangeDisplayed={2}
					onPageChange={this.handlePageClick}
					containerClassName={'pagination'}
					subContainerClassName={'pages pagination'}
					activeClassName={'active'}
				/>
			</div>
		);
	}
}

export default Pagination;
