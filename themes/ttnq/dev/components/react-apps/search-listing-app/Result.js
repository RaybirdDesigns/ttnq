import React, { Component } from 'react';
import defaultImage from '../../../images/Beaches_Hero-150x150.jpg';

class SearchResult extends Component {
	constructor() {
		super();
		this.content = this.content.bind(this);
		this.heading = this.heading.bind(this);
		this.renderDate = this.renderDate.bind(this);
	}

	heading() {
		return { __html: this.props.result.title };
	}

	content() {
		return { __html: this.props.result.excerpt };
	}

	renderDate() {
		if (this.props.result.time_of_event) {
			return <span className="post-result-text-date">{this.props.result.time_of_event}</span>;
		}
	}

	render() {
		const props = this.props.result;
		const image = props.image;
		const postTypes = {
			product: 'Product',
			post: 'Article',
			event: 'Event',
			page: 'List Page',
			accommodation: 'Accommodation',
			itinerary:"Itineraries"
		};
		return (
			<a href={props.link} className="post-result-item">
				<div
					className="post-result-image"
					style={{ backgroundImage: `url(${image})` }}
				/>
				<div className="post-result-text">
					{this.renderDate()}
					<h4 dangerouslySetInnerHTML={this.heading()} />
					<div dangerouslySetInnerHTML={this.content()} />
					<div className="type">
						<p>{postTypes[props.category]}</p>
					</div>
				</div>
			</a>
		);
	}
}

export default SearchResult;
