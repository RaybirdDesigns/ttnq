import React, { Component } from 'react';
import { FadeLoader } from 'react-spinners';
import $ from 'jquery';

import Header from './Header';
import Result from './Result';
import Pagination from './Pagination';
import Showing from './Showing';

const Fragment = React.Fragment;

// Get Params
let params = [],
	hash;
let q = document.URL.split('?')[1];
if (q != undefined) {
	q = q.split('&');
	for (let i = 0; i < q.length; i++) {
		hash = q[i].split('=');
		params.push(hash[1]);
		params[hash[0]] = hash[1];
	}
}

class SearchResultsApp extends Component {
	constructor() {
		super();
		this.baseApiUrl = document.location.origin + '/wp-json/custom/v1/search';
		this.getData = this.getData.bind(this);
		this.getNewSearchTerm = this.getNewSearchTerm.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.renderSpinner = this.renderSpinner.bind(this);
		this.paginationOffset = this.paginationOffset.bind(this);
		this.renderNoResults = this.renderNoResults.bind(this);
		this.getFilter = this.getFilter.bind(this);
		this.state = {
			siteUrl: this.siteUrl,
			searchTerm: decodeURI(params['s']),
			filterName: '',
			filterTerm: '',
			wpSearchApiUrl: `${this.baseApiUrl}?s=${params['s']}&posts_per_page=7&page=1`,
			results: [],
			resultCount: 0,
			loading: true,
			foundPosts: 0,
			totalPages: 0,
			page: 1,
			app: 'search',
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData() {
		this.setState(
			{
				loading: true,
			},
			() => {
				$.ajax({
					url: this.state.wpSearchApiUrl,
					dataType: 'json',
					success: (rawData, textStatus, request) => {
						// count number of results
						const resultCount = rawData.length;

						// set state
						this.setState({
							results: rawData,
							resultCount,
							loading: false,
							foundPosts: Number(request.getResponseHeader('x-wp-total')),
							totalPages: Number(request.getResponseHeader('x-wp-totalpages')),
						});
					},
					error: error => {
						console.log('error:', error);
					},
				});
			},
		);
	}

	getNewSearchTerm(term) {
		let apiUrl =
			this.state.filterTerm === '' || this.state.filterTerm === 'all'
				? `${this.baseApiUrl}?s=${term}&posts_per_page=7&page=1`
				: `${this.baseApiUrl}?s=${term}&engine=${this.state.filterTerm}&posts_per_page=7&page=1`;
		if (term.length >= 3) {
			this.setState(
				{
					wpSearchApiUrl: apiUrl,
					searchTerm: decodeURI(term),
				},
				() => {
					this.getData();
				},
			);
		} else {
			this.setState({
				searchTerm: decodeURI(term),
			});
		}
	}

	renderPagination() {
		if (this.state.foundPosts > 7) {
			return (
				<Pagination
					searchTerm={this.state.searchTerm}
					foundPosts={this.state.foundPosts}
					totalPages={this.state.totalPages}
					paginationOffset={this.paginationOffset}
				/>
			);
		}
	}

	paginationOffset(offSet) {
		this.setState(
			{
				page: offSet,
				wpSearchApiUrl: `${this.baseApiUrl}?s=${this.state.searchTerm}&posts_per_page=7&page=${offSet}`,
			},
			() => {
				this.getData();
			},
		);
	}

	getFilter(term, name) {
		let filterApiUrl =
			term === 'all'
				? `${this.baseApiUrl}?s=${this.state.searchTerm}&posts_per_page=7&page=1`
				: `${this.baseApiUrl}?s=${this.state.searchTerm}&engine=${term}&posts_per_page=7&page=1`;
		this.setState(
			{
				filterTerm: term,
				filterName: name,
				wpSearchApiUrl: filterApiUrl,
			},
			() => {
				this.getData();
			},
		);
	}

	renderSpinner() {
		if (this.state.loading) {
			return (
				<div className="loading-overlay">
					<div className="loading-overlay-spinner">
						<FadeLoader color={'#b5b5b5'} loading={this.state.loading} height={10} width={10} radius={10} />
					</div>
				</div>
			);
		}
	}

	renderNoResults() {
		if (this.state.foundPosts === 0 && this.state.loading === false) {
			let message =
				this.state.filterName !== '' && this.state.filterName !== 'all'
					? `"${this.state.searchTerm}" in ${this.state.filterName}`
					: `"${this.state.searchTerm}"`;
			return (
				<div className="no-results">
					<p>
						Sorry we couldn't find any results for <strong>{message}</strong>.
					</p>
				</div>
			);
		}
	}

	render() {
		const searchResults = this.state.results;

		return (
			<Fragment>
				<Header
					searchTerm={this.state.searchTerm}
					foundPosts={this.state.foundPosts}
					filterName={this.state.filterName}
					filterTerm={this.state.filterTerm}
					getNewSearchTerm={this.getNewSearchTerm}
					getFilter={this.getFilter}
					filterTerm={this.state.filterTerm}
				/>
				<Showing foundPosts={this.state.foundPosts} resultCount={this.state.resultCount} app={this.state.app} />
				<section id="search-results">
					<div className="row">
						<div className="small-12 columns">
							<ul className="list-reset">
								{searchResults.map(result => {
									return (
										<li key={result.id} className="post-result">
											<Result result={result} />
										</li>
									);
								})}
							</ul>
							{this.renderNoResults()}
							{this.renderPagination()}
						</div>
					</div>
					{this.renderSpinner()}
				</section>
			</Fragment>
		);
	}
}

export default SearchResultsApp;
