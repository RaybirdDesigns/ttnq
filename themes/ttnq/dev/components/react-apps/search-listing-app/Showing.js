import React, { Component } from 'react';

class Showing extends Component {
	render() {
		const props = this.props;
		return (
			<div id="showing-wrapper">
				<p>
					Showing {this.props.resultCount} out of {this.props.foundPosts} results
				</p>
			</div>
		);
	}
}

export default Showing;
