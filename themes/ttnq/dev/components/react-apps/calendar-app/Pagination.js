import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
// import 'gsap/ScrollToPlugin';

class Pagination extends Component {
	constructor() {
		super();
		this.handlePageClick = this.handlePageClick.bind(this);

		this.state = {
			page: 0,
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			page: nextProps.page - 1,
		});
	}

	handlePageClick(data) {
		let selected = data.selected;
		let offset = Math.ceil(selected);

		// TweenLite.to(window, 1.5, {
		// 	scrollTo: { y: '.results-filter-wrapper', offsetY: 0, ease: Power1.easeInOut },
		// 	onComplete: this.props.paginationOffset(offset),
		// });
		this.props.paginationOffset(offset);
	}

	render() {
		const props = this.props;

		return (
			<div className="pagination-wrapper">
				<ReactPaginate
					previousLabel={''}
					nextLabel={''}
					breakLabel={<a href="">...</a>}
					breakClassName={'break-me'}
					pageCount={this.props.totalPages}
					marginPagesDisplayed={3}
					pageRangeDisplayed={2}
					forcePage={this.state.page}
					onPageChange={this.handlePageClick}
					containerClassName={'pagination'}
					subContainerClassName={'pages pagination'}
					activeClassName={'active'}
				/>
			</div>
		);
	}
}

export default Pagination;
