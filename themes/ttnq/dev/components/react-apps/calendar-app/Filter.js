import React, { Component } from 'react';

class Filter extends Component {
	constructor() {
		super();
		this.handleClick = this.handleClick.bind(this);
		this.tagsObjFromWP = window.tag_filter_calendar;
		this.state = {
			filterTerm: 'all',
			terms: this.tagsObjFromWP,
		};
	}

	handleClick(e) {
		e.preventDefault();
		let term = e.target.getAttribute('data-term');
		this.setState({ filterTerm: term }, () => {
			this.props.getCatFilter(term);
		});
	}

	render() {
		// reverse filters so all is first
		const postTypeKeys = Object.keys(this.state.terms).reverse();

		return (
			<div className="results-filter-wrapper">
				<div className="results-filter">
					<ul className="list-reset list-inline">
						{postTypeKeys.map(term => {
							return (
								<li key={term} className={term === this.state.filterTerm ? 'active' : null}>
									<a
										data-term={term}
										onClick={e => {
											this.handleClick(e);
										}}
										ref={filter => {
											this.filter = filter;
										}}
										href="#">
										{this.state.terms[term]}
									</a>
								</li>
							);
						})}
					</ul>
				</div>
			</div>
		);
	}
}

export default Filter;
