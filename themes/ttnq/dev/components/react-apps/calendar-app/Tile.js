import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Dotdotdot from 'react-dotdotdot';

class Tile extends Component {
	constructor() {
		super();
		// this.categoriesObjFromWP = window.categories_calendar;
		this.renderDate = this.renderDate.bind(this);
		this.mouseIn = this.mouseIn.bind(this);
		this.mouseLeave = this.mouseLeave.bind(this);
		this.handleInfoClick = this.handleInfoClick.bind(this);
		this.renderInfoBox = this.renderInfoBox.bind(this);
		this.renderPhoneButton = this.renderPhoneButton.bind(this);
		this.renderMainTile = this.renderMainTile.bind(this);
		this.renderMonthTile = this.renderMonthTile.bind(this);
		this.heading = this.heading.bind(this);
		this.location = this.location.bind(this);
		this.mainColor = '#6FA0AE';

		this.state = {
			buttonActive: false,
			infoBoxActive: false,
			buttonStyles: {
				backgroundColor: 'transparent',
				borderColor: this.mainColor,
			},
			tileType: 'main',
			// categoriesObjFromWP: this.categoriesObjFromWP,
		};
	}

	mouseIn() {
		this.setState({
			buttonActive: true,
			buttonStyles: {
				backgroundColor: this.mainColor,
			},
		});
	}

	mouseLeave() {
		if (!this.state.infoBoxActive) {
			this.setState({
				buttonActive: false,
				buttonStyles: {
					backgroundColor: 'transparent',
				},
			});
		}
	}

	handleInfoClick() {

		this.setState({
			infoBoxActive: !this.state.infoBoxActive,
			buttonStyles: {
				backgroundColor: this.mainColor,
			},
		});
	}

	renderPhoneButton() {
		const { prod_email, prod_phone } = this.props.result;
		if (prod_email || prod_phone) {
			const icon = prod_phone ? (
				<svg
					className="calendar-tile-info-btn-phone"
					viewBox="0 0 1792 1792"
					xmlns="http://www.w3.org/2000/svg">
					<path d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-53-3.5t-57.5-12.5-47-14.5-55.5-20.5-49-18q-98-35-175-83-127-79-264-216t-216-264q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47-12.5-57.5-3.5-53q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z" />
				</svg>
			) : (
				<svg
					className="calendar-tile-info-btn-mail"
					width="1792"
					height="1792"
					viewBox="0 0 1792 1792"
					xmlns="http://www.w3.org/2000/svg">
					<path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z" />
				</svg>
			);

			return (
				<button
					ref={button => (this.button = button)}
					className={`calendar-tile-info-btn ${this.state.infoBoxActive ? 'active' : null}`}
					style={this.state.buttonStyles}
					onMouseEnter={() => {
						this.mouseIn();
					}}
					onMouseLeave={() => {
						this.mouseLeave();
					}}
					onClick={() => {
						this.handleInfoClick();
					}}>
					{icon}
				</button>
			);
		}
	}

	renderDate() {
		const { end_time_of_event, time_of_event, date_type } = this.props.result;
		const startDay = time_of_event ? time_of_event.substring(0, 2).trim() : null;
		const endDay = end_time_of_event ? end_time_of_event.substring(0, 2).trim() : null;
		const months = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December',
		];
		const month = months.filter(month => {
			if (time_of_event.indexOf(month) >= 0) {
				return month;
			}
		});
		if (date_type === 'single-date') {
			return (
				<div className="calendar-tile-left" style={{ backgroundColor: this.mainColor }}>
					<div className="calendar-tile-left-day single-date">
						<span>{startDay}</span>
					</div>
					<div className="calendar-tile-left-month">{String(month).substring(0, 3)}</div>
				</div>
			);
		} else if (date_type === 'date-range') {
			return (
				<div className="calendar-tile-left" style={{ backgroundColor: this.mainColor }}>
					<div className="calendar-tile-left-day multi-date">
						<span>
							{startDay} - {endDay}
						</span>
					</div>
					<div className="calendar-tile-left-month">{String(month).substring(0, 3)}</div>
				</div>
			);
		} else if (date_type === 'month-year') {
			return (
				<div className="calendar-tile-left" style={{ backgroundColor: this.mainColor }}>
					<div className="calendar-tile-left-month single-month">
						<span>{String(month).substring(0, 3)}</span>
					</div>
				</div>
			);
		} else if (date_type === 'month-tba') {
			return (
				<div className="calendar-tile-left" style={{ backgroundColor: this.mainColor }}>
					<div className="calendar-tile-left-day date-tba">
						<span>TBA</span>
					</div>
					<div className="calendar-tile-left-month">{String(month).substring(0, 3)}</div>
				</div>
			);
		}
	}

	renderInfoBox() {
		const { prod_phone, prod_email, title } = this.props.result;

		const email = prod_email ? (
			<a className="calendar-tile-info-box-email" href={`mailto:${prod_email}?subject=Enquirey about ${title}`}>
				Email us
			</a>
		) : null;
		const phone = prod_phone ? <p className="calendar-tile-info-box-phone">{prod_phone}</p> : null;
		if (this.state.infoBoxActive) {
			return (
				<div className="calendar-tile-info-box">
					{phone}
					{email}
				</div>
			);
		}
	}

	heading() {
		return { __html: this.props.result.title };
	}

	location() {
		// const categoryId = this.props.result.category;
		return { __html: this.props.result.category };
	}

	renderMainTile() {
		const props = this.props.result;
		return (
			<div className="calendar-tile">
				{this.renderDate()}
				<div className="calendar-tile-right">
					<h4>
						<Dotdotdot clamp={2}>
							<a href={props.link} dangerouslySetInnerHTML={this.heading()} />
						</Dotdotdot>
					</h4>
					<Dotdotdot clamp={2}>
						<p dangerouslySetInnerHTML={this.location()} />
					</Dotdotdot>
				</div>
				{this.renderPhoneButton()}
				<ReactCSSTransitionGroup
					transitionName="info-show"
					transitionEnterTimeout={200}
					transitionLeaveTimeout={200}>
					{this.renderInfoBox()}
				</ReactCSSTransitionGroup>
			</div>
		);
	}

	renderMonthTile() {
		const props = this.props.result;
		return (
			<div className="calendar-tile calendar-tile-month">
				<div className="calendar-tile-month-wrapper">
					<h4>{props.title}</h4>
					<div className="calendar-tile-month-year">{props.year}</div>
				</div>
			</div>
		);
	}

	render() {
		const tile = this.props.result.tileType === 'month' ? this.renderMonthTile() : this.renderMainTile();
		// return <div>test</div>;
		return tile;
	}
}

export default Tile;
