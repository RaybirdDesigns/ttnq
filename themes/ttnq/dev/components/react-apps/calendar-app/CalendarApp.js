import React, { Component } from 'react';
import { FadeLoader } from 'react-spinners';
import $ from 'jquery';

import { sortByDate, addMonths, filterByTag, fromMonth, paginateFilter } from './Data.js';
import Pagination from './Pagination';
import Filter from './Filter';
import Tile from './Tile';
import Dates from './Dates';
import Showing from './Showing';

const Fragment = React.Fragment;

class CalendarApp extends Component {
	constructor() {
		super();
		this.baseApiUrl = document.location.origin + '/wp-json/custom/v1/calendar';
		this.appElement = document.getElementById('calendar-root');
		this.monthNow = this.appElement.dataset.month;
		this.yearNow = this.appElement.dataset.year;
		this.postsPerPage = this.appElement.dataset.postsPerPage;
		this.getData = this.getData.bind(this);
		this.getCatFilter = this.getCatFilter.bind(this);
		this.getDateFilter = this.getDateFilter.bind(this);
		this.filter = this.filter.bind(this);
		this.renderNoResults = this.renderNoResults.bind(this);
		this.renderSpinner = this.renderSpinner.bind(this);
		this.renderPagination = this.renderPagination.bind(this);
		this.paginationOffset = this.paginationOffset.bind(this);
		this.state = {
			// siteUrl: this.siteUrl,
			apiUrl: this.baseApiUrl,
			results: [],
			tagFilter: null,
			dateFilter: null,
			renderResults: [],
			resultCount: 0,
			loading: true,
			foundPosts: 0,
			totalPages: 0,
			postsPerPage: Number(this.postsPerPage),
			page: 1,
			monthNow: this.monthNow.toLowerCase(),
			yearNow: Number(this.yearNow),
		};
	}

	componentWillMount() {
		this.getData();
	}

	getData() {

		this.setState(
			{
				loading: true,
			},
			() => {
				$.ajax({
					url: this.state.apiUrl,
					dataType: 'json',
					success: (rawData, textStatus, request) => {
						// count number of results
						const resultCount = rawData.length;
						// console.log(rawData);
						// set state
						this.setState({
							results: rawData,
							renderResults: addMonths(sortByDate(fromMonth({ month: this.monthNow, year: this.yearNow }, rawData))),
							resultCount,
							loading: false,
							foundPosts: resultCount,
							totalPages: Math.ceil(resultCount / this.state.postsPerPage),
						});
					},
					error: error => {
						console.log('error:', error);
					},
				});
			},
		);
	}

	getCatFilter(term) {
		this.setState(
			{
				tagFilter: term,
			},
			() => {
				this.filter();
			},
		);
	}

	getDateFilter(dateObj) {
		this.setState(
			{
				dateFilter: dateObj,
			},
			() => {
				this.filter();
			},
		);
	}

	filter() {
		const state = this.state;
		let filteredResult = state.tagFilter
			? addMonths(sortByDate(filterByTag(state.tagFilter, fromMonth(state.dateFilter, this.state.results))))
			: addMonths(sortByDate(fromMonth(state.dateFilter, this.state.results)));

		this.setState({
			resultCount: filteredResult.length,
			renderResults: filteredResult,
			totalPages: Math.ceil(filteredResult.length / this.state.postsPerPage),
			page: 1,
		});
	}

	renderSpinner() {
		if (this.state.loading) {
			return (
				<div className="loading-overlay">
					<div className="loading-overlay-spinner">
						<FadeLoader color={'#b5b5b5'} loading={this.state.loading} height={10} width={10} radius={10} />
					</div>
				</div>
			);
		}
	}

	renderNoResults() {
		if (this.state.renderResults.length === 0 && !this.state.loading) {
			return (
				<div className="no-results">
					<p>Sorry we couldn't find any events.</p>
				</div>
			);
		}
	}

	paginationOffset(offSet) {
		this.setState({
			page: offSet + 1,
		});
	}

	renderPagination() {
		const renderedResultsCount = this.state.renderResults.length;

		// show pagination if result after filtering is more than posts per page
		if (renderedResultsCount > this.state.postsPerPage) {
			return (
				<Pagination
					foundPosts={renderedResultsCount}
					totalPages={this.state.totalPages}
					paginationOffset={this.paginationOffset}
					page={this.state.page}
				/>
			);
		}
	}

	render() {
		const queryResults = paginateFilter(this.state.page, this.state.postsPerPage, this.state.renderResults);
		const resultsShowing = queryResults.resultEventsCount;

		return (
			<Fragment>
				<Filter getCatFilter={this.getCatFilter} />
				<Showing
					foundPosts={this.state.foundPosts}
					resultsShowing={resultsShowing}
					app={this.state.app}
					dateFilter={this.state.dateFilter}
					tagFilter={this.state.tagFilter}
					dateFilter={this.state.dateFilter}
				/>
				<Dates monthNow={this.state.monthNow} yearNow={this.state.yearNow} getDateFilter={this.getDateFilter} />
				<section id="event-results">
					<div className="row">
						<div className="small-12 columns">
							<ul className="list-reset clearfix">
								{queryResults.result.map(result => {
									return (
										<li key={result.id} className="small-12 medium-6 large-4 columns event-item">
											<Tile result={result} />
										</li>
									);
								})}
							</ul>
							{this.renderNoResults()}
							{this.renderPagination()}
						</div>
					</div>
					{this.renderSpinner()}
				</section>
			</Fragment>
		);
	}
}

export default CalendarApp;
