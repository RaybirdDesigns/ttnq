import React, { Component } from 'react';

class Dates extends Component {
	constructor() {
		super();
		this.handleClick = this.handleClick.bind(this);
		this.handleListClick = this.handleListClick.bind(this);
		this.state = {
			yearOne: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
			yearTwo: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
			activeDate: {},
			activeDateIndex: null,
			activeList: null,
		};
	}

	componentWillMount() {
		// setup array for two years with years indexed
		// set current date in state and pass back to filter
		const { yearNow, monthNow } = this.props;
		const nextYear = yearNow + 1;
		const yearOne = [String(yearNow), ...this.state.yearOne];
		const yearTwo = [String(nextYear), ...this.state.yearOne];

		// get index of current date in array to use in indicating current month in ui
		const activeDateIndex = this.state.yearOne.indexOf(monthNow);

		this.setState(
			{
				yearOne: yearOne,
				yearTwo: yearTwo,
				activeDate: {
					month: monthNow.substring(0, 3),
					year: yearNow,
				},
				activeDateIndex: activeDateIndex,
			},
			() => {
				this.props.getDateFilter(this.state.activeDate);
			},
		);
	}

	handleClick(e, i) {
		e.preventDefault();
		const { yearNow, monthNow } = this.props;
		const nextYear = yearNow + 1;
		let date = e.target.getAttribute('data-date');

		let month = date.slice(0, 3);
		let year = Number(date.slice(3)) > 12 ? nextYear : yearNow;

		this.setState(
			{
				activeDate: {
					month: month,
					year: year,
				},
				activeDateIndex: i - 1,
				activeList: null,
			},
			() => {
				this.props.getDateFilter(this.state.activeDate);
			},
		);
	}

	handleListClick(list) {
		this.setState({
			activeList: list === this.state.activeList ? null : list,
		});
	}

	render() {
		const state = this.state;
		const props = this.props;

		return (
			<div className="dates row">
				<p>Jump to a month</p>
				<ul className={`dates-list dates-list-year-one ${this.state.activeList === 'yearOne' ? 'active' : ''}`}>
					{this.state.yearOne.map((date, i) => {
						return (
							<li key={i} className={i === state.activeDateIndex + 1 ? 'active' : null}>
								<a
									data-date={date + i}
									href="#"
									onClick={e => {
										this.handleClick(e, i);
									}}
									ref={filter => {
										this.filter = filter;
									}}>
									{date}
								</a>
							</li>
						);
					})}
					<button
						className="yearOneBtn btn-reset"
						onClick={e => {
							this.handleListClick('yearOne');
						}}
					/>
				</ul>
				<ul className={`dates-list dates-list-year-two ${this.state.activeList === 'yearTwo' ? 'active' : ''}`}>
					{this.state.yearTwo.map((date, i) => {
						return (
							<li key={i + 13} className={i + 13 === state.activeDateIndex + 1 ? 'active' : null}>
								<a
									data-date={date + (i + 13)}
									href="#"
									onClick={e => {
										this.handleClick(e, i + 13);
									}}
									ref={filter => {
										this.filter = filter;
									}}>
									{date}
								</a>
							</li>
						);
					})}
					<button
						className="yearTwoBtn btn-reset"
						onClick={e => {
							this.handleListClick('yearTwo');
						}}
					/>
				</ul>
			</div>
		);
	}
}

export default Dates;
