import React, { Component } from 'react';

class Showing extends Component {
	render() {
		const props = this.props;

		const month = props.dateFilter ? `${props.dateFilter.month} ${props.dateFilter.year}` : null;
		const plural = props.resultsShowing > 1 ? 's' : '';
		const tag =
			props.categoryFilter && props.categoryFilter !== 'all'
				? `${tag_filter_calendar[Number(props.tagFilter)]} event${plural}`
				: '';

		return (
			<div id="showing-wrapper">
				<p>
					Showing {props.resultsShowing} {tag} of {props.foundPosts} events from <span>{month}</span>
				</p>
			</div>
		);
	}
}

export default Showing;
