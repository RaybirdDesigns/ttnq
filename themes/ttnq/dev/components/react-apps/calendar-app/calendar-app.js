import React from 'react';
import ReactDOM from 'react-dom';
import CalendarApp from './CalendarApp.js';

// render app to dom
ReactDOM.render(<CalendarApp />, document.getElementById('calendar-root'));
