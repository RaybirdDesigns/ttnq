import uniqBy from 'lodash/uniqBy';

// Paginate filter
// returns an object
export function paginateFilter(pageNumber, postsPerPage, data) {
	--pageNumber;
	let result = data.slice(pageNumber * postsPerPage, (pageNumber + 1) * postsPerPage);
	let count = 0;

	// get number of actual events
	result.forEach(post => {
		count += post.tileType === 'main' ? 1 : 0;
	});

	let resultObj = {
		result: result,
		resultEventsCount: count,
	};

	return resultObj;
}

// Sort by date. Used in ajax call
export function sortByDate(data) {
	let result = data.sort((a, b) => new Date(a.time_of_event) - new Date(b.time_of_event));
	return result;
}

// Filter all posts by first tag
export function filterByTag(term, data) {
	if (term === 'all') {
		let result = data;
		return result;
	} else {
		let result = data.filter(post => {
			if (post.tags[0] === Number(term)) {
				return post;
			}
		});
		return result;
	}
}

// Filter data to get events that start from a certain date
export function fromMonth(date, data) {
	let fromDate = new Date(date.month + ' 01 ' + String(date.year));

	let result = data.filter(post => {
		let timeOfEvent = new Date(post.time_of_event);
		if (timeOfEvent >= fromDate) {
			return post;
		}
	});

	return result;
}

// Adds a month object to the posts array to render a tile for
// the beginning of every month.
// Initially adds obj before every post then we remove duplicates
// leaving the only the first unique object. We "test" on id
export function addMonths(data) {
	const dataCopy = [...data];

	let withMonthsArr = [];
	let monthNames = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];

	dataCopy.forEach(post => {
		let timeOfEvent = new Date(post.time_of_event);
		let monthName = monthNames[timeOfEvent.getMonth()];
		let year = timeOfEvent.getFullYear();

		// acf fields required data
		let month = {
			id: monthName + String(year),
			tileType: 'month',
			year: String(year),
			title: monthName,
			colour: '#178b85',
			time_of_event: '',
		};
		post.tileType = 'main';
		withMonthsArr.push(month);
		withMonthsArr.push(post);
	});

	return uniqBy(withMonthsArr, 'id');
}
