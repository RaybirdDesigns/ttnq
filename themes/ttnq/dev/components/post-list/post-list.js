import { Spinner } from 'spin.js';

///////////////////////// Ajax Post List //////////////////////////////

const $eventReorder = $('#event-reorder');
const $dateOrderInput = $eventReorder.find('.order-by-date');
const $postListContainer = $('#post-list-container');

// get query params and store in array for use in ajax call
let params = [],
	hash;
let q = document.URL.split('?')[1];
if (q != undefined) {
	q = q.split('&');
	for (let i = 0; i < q.length; i++) {
		hash = q[i].split('=');
		params.push(hash[1]);
		params[hash[0]] = hash[1];
	}
}

// make initial ajax call if post-list template
if ($('body').hasClass('post-list')) {
	list_get_posts();
}

// handle reorder of events by start date
$dateOrderInput.on('change', function() {
	if ($dateOrderInput.is(':checked')) {
		list_get_posts(null, 'time_of_event');
	} else {
		list_get_posts();
	}
});

// if pagination is clicked, load correct posts
$(document).on('click', '.post-list-pagination a', function(e) {
	e.preventDefault();

	const url = $(this).attr('href');
	const paged = url.split('&paged=');

	const sectionId = params['post-type'] === 'event' ? '#event-reorder' : '#post-list';

	const ajaxCall = function() {
		if ($dateOrderInput.is(':checked')) {
			list_get_posts(paged[1], 'time_of_event');
		} else {
			list_get_posts(paged[1]);
		}
	};

	TweenLite.to(window, 1.2, { scrollTo: { y: sectionId, offsetY: 0, ease: Power1.easeInOut }, onComplete: ajaxCall });
});

// main ajax function
function list_get_posts(paged, metaKey) {
	const paged_value = paged;
	const ajax_url = ajax_list_params.ajax_url;
	// snipper setup
	const opts = {
		lines: 9, // The number of lines to draw
		length: 0, // The length of each line
		width: 22, // The line thickness
		radius: 28, // The radius of the inner circle
		scale: 1, // Scales overall size of the spinner
		corners: 1, // Corner roundness (0..1)
		color: '#b5b5b5', // CSS color or array of colors
		fadeColor: 'transparent', // CSS color or array of colors
		opacity: 0.5, // Opacity of the lines
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		speed: 1.3, // Rounds per second
		trail: 100, // Afterglow percentage
		fps: 20, // Frames per second when using setTimeout() as a fallback in IE 9
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		className: 'list-spinner', // The CSS class to assign to the spinner
		top: '100px', // Top position relative to parent
		left: '50%', // Left position relative to parent
		shadow: 'none', // Box-shadow for the lines
		position: 'absolute', // Element positioning
	};

	$.ajax({
		type: 'GET',
		url: ajax_url,
		data: {
			action: 'post_list',
			postType: params['post-type'],
			category: params['category_name'],
			metaKey: metaKey,
			tag: params['tag'],
			paged: paged_value,
		},
		beforeSend: function() {
			const target = document.querySelector('#post-list-container');
			const spinner = new Spinner(opts).spin(target);
			$(target).append('<div class="list-spinner-overlay"></div>');
		},
		success: function(data) {
			// const target = document.querySelector('#post-list-container');
			// const spinner = new Spinner(opts).spin(target);
			$postListContainer.html(data);
		},
		error: function() {
			$postListContainer.html(
				'<div class="text-center"><p>Oops! There was an error. Please try again.</p></div>',
			);
		},
	});
}
