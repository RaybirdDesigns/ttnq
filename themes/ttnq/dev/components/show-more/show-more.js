import 'gsap/ScrollToPlugin';

const $showMoreBtn = $('.show-more-btn');

function showMore($thisShowMoreSection) {
	const showMoreContainerHeight = $thisShowMoreSection.find('.show-more-container').outerHeight();
	const showMoreContainerInnerHeight = $thisShowMoreSection.find('.show-more-container-inner').outerHeight();

	$thisShowMoreSection.find('.show-more-container').css('max-height', showMoreContainerInnerHeight + 30);

	$thisShowMoreSection
		.find('.show-more-btn')
		.text('Show less')
		.addClass('active');
}

function showLess($thisShowMoreSection) {
	const sectionId = '#' + $thisShowMoreSection.attr('id');
	// scroll to top of section on close
	TweenLite.to(window, 1.2, { scrollTo: { y: sectionId, offsetY: 0, ease: Power1.easeInOut } });

	$thisShowMoreSection.find('.show-more-container').removeAttr('style');

	$thisShowMoreSection
		.find('.show-more-btn')
		.text('Show more')
		.removeClass('active');
}

function toggleShow(e) {
	const $this = $(this);
	const $thisShowMoreSection = $this.closest('.show-more-section');
	const isOpen = $thisShowMoreSection.find('.show-more-container[style]').length;

	if (isOpen) {
		showLess($thisShowMoreSection);
	} else {
		showMore($thisShowMoreSection);
	}
}

// Handle show more
$showMoreBtn.on('click', toggleShow);
