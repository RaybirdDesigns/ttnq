// jQuery is loaded by default
$(document).ready(function($){
        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');
            var _id = $(this).data('id');
            var newurl = addParameterToURL("tab="+_id);
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
    
            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
            window.history.pushState({path:newurl},'',newurl);
            $('.deals-second-load, .deals-first-load, .see-more').hide();
        });
        $('.filter-selection').click(function(e){
            if($( ".tabs" ).css('display') == 'block') {
                $(this).find('span').text('Show filters');
                $(this).find('i').removeClass('fa-times').addClass('fa-angle-down');
            }else{
                $(this).find('span').text('Close filters');
                $(this).find('i').removeClass('fa-angle-down').addClass('fa-times');
            }
            $( ".tabs" ).toggle( "slow", function() {
            });
        })

        $(window).resize(function() {
            var height = parseFloat($(window).height()) - 300;
            if($(window).width() >= 991 && $(window).height() < 1024){
                $('.landing-page-template video#videoBG, .landing-page-template .banner-wrapper').attr('style','height:'+height+"px !important;"); 
            }else{
                $('.landing-page-template video#videoBG, .landing-page-template .banner-wrapper').attr('style','height:auto !important;');  
            }
            if($(window).width() >= 1300 && $(window).height() < 1024){                 
                $('.landing-page-template .embed-container').attr('style','background-position-y:-220px');
                $('.landing-page-template .banner-wrapper iframe').attr('style', 'top:-210px');           
            }else{
                $('.landing-page-template .embed-container').attr('style','background-position-y:0');
                $('.landing-page-template .banner-wrapper iframe').attr('style', 'top:0');  
            }
        });
        
        $(window).trigger('resize');

});

function addParameterToURL(param){
    var _url = window.location.href.split('?')[0];
    _url += (_url.split('?')[1] ? '&':'?') + param;
    return _url;
}