import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';

// header js
const $document = $(document);
const $body = $('body');
const $header = $('header.header');
const $headerSearchBtn = $header.find('.header-search-btn');
const $headerSearchWrapper = $header.find('.header-search-wrapper');
const $searchField = $header.find('.search-field');
const $headerSearchCloseBtn = $header.find('.header-search-close-btn');
const $searchwpLiveResults = $('.searchwp-live-search-results-showing');

// manage visibility of search dropdown
$headerSearchBtn.on('click', () => {
    $body.addClass('search-active');
    $searchField.focus();
});

// close search boxes
$headerSearchCloseBtn.on('click', () => {
    $body.removeClass('search-active');
});

// hide search dropdown on  not search dropdown boxes
$document.on('mouseup', e => {
    if (
        !$headerSearchWrapper.is(e.target) &&
        !$('.searchwp-live-search-results-showing').is(e.target) &&
        $headerSearchWrapper.has(e.target).length === 0 &&
        $('.searchwp-live-search-results-showing').has(e.target).length === 0
    ) {
        $body.removeClass('search-active');
    }
});

// hide search boxes on esc key up
$document.on('keyup', e => {
    if (e.keyCode === 27) {
        $body.removeClass('search-active');
    }
});

// get the value of the search field
let inputValue = '';

$searchField.on('keyup', function() {
    inputValue = this.value;

    // run polling on keyup
    if (inputValue.split('').length > 2) {
        // console.log('should poll');
        pollForResults();
    }
});

// poll for search results to we can highligh search term
let pollForResults = function() {
    // console.log('polling');

    // check if results loaded
    if ($('.searchwp-live-search-result').length > 0) {
        // console.log('should bolden');
        const $result = $('.searchwp-live-search-result');
        const regex = new RegExp(inputValue.replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1'), 'ig');

        $result.each((i, item) => {
            const $item = $(item);
            const $heading = $item.find('h3');
            const $para = $item.find('.result-item-para');

            const headingText = $heading.text();
            const paraText = $para.text();

            const newHeading = headingText.replace(
                regex,
                `<span class="result-item-heading-bold">${inputValue}</span>`,
            );
            const newPara = paraText.replace(regex, `<span class="result-item-p-bold">${inputValue}</span>`);
            // console.log(inputValue);
            $heading.html(newHeading);
            $para.html(newPara);
        });
        // console.log('ended');
        return;
    }

    let timer = setTimeout(pollForResults, 100);

    // cancel polling if search dropdown closed
    if (!$body.hasClass('search-active') || inputValue.split('').length < 2) {
        clearTimeout(timer);
    }
};
