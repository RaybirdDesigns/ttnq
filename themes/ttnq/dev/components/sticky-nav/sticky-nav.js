import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import enquire from 'enquire.js';

const $stickyNav = $('#sticky-nav');
const $window = $(window);
const $overview = $('.overview');
const $stickyWrapper = $('.sticky-wrapper');

if ($stickyNav.length) {
	let distance = $stickyNav.offset().top;
	const stickyNavHeight = $stickyNav.outerHeight();

	$window.on('resize', () => {
		distance = $stickyNav.offset().top;
	});

	$window.scroll(() => {
		if ($window.scrollTop() >= distance) {
			$stickyNav.css({
				position: 'fixed',
				top: '0px',
				left: '0px',
				zIndex: '9999',
			});

			if (!$('.packer').length) {
				$stickyNav.after(`<div class="packer" style="height:${stickyNavHeight}px;"></div>`);
			}
		} else if ($window.scrollTop() <= distance) {
			$stickyNav.removeAttr('style');
			$('.packer').remove();
		}
	});
}

// activate dropdown on mobile
$overview.on('click', function(e) {
	let $this = $(this);

	enquire.register('(max-width: 768px)', () => {
		$this.toggleClass('active');

		$stickyWrapper.slideToggle();
		e.preventDefault();
	});
});

enquire.register('(min-width: 768px)', () => {
	$stickyWrapper.removeAttr('style');
});
