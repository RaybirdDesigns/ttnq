// Epic jquery madness
import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';

import enquire from 'enquire.js';
import Hammer from 'hammerjs';

const $body = $('body');
const $menuMainMenu = $('#menu-main-menu');
const $mainMenuItem = $menuMainMenu.find('.main-menu-item > a');
const $menuItemHasChildren = $('.main-menu-item.menu-item-has-children');
const $subMenuWrapper = $menuMainMenu.find('.sub-menu-wrapper');

// Mobile nav vars
const $headerWrapper = $('.header-wrapper');
const $mainNav = $headerWrapper.find('.main-nav');
const $mainMenuOpenBtn = $headerWrapper.find('.main-menu-btn');
const $mainMenuCloseBtn = $headerWrapper.find('.menu-header-close-btn');

// wrappers
const itemTextWrapper = "<div class='item-text'></div>";
const overlay = "<div class='overlay'></div>";
const column = "<div class='multi-col-menu small-12 medium-6 large-3 columns'></div>";

// dark overlay when menu with submenu is hovered
$menuItemHasChildren.on('mouseover', function() {

	enquire.register("(min-width: 1025px)", () => {

		$body.addClass('menu-active');
		$mainMenuItem.not($(this).find('a')).addClass('dim-active');
	});
});

$menuItemHasChildren.on('mouseleave', () => {

	enquire.register("(min-width: 1025px)", () => {

		$body.removeClass('menu-active');
		$mainMenuItem.removeClass('dim-active');
	});
});

// ++++++++++++ Wrapping elements for desktop +++++++++++++++++++++++++++++++++++++++++
/*
	In order to achieve the layout for the desktop menu we need to
	wrap certian menu elements with jquery.
	We don't need these elements wrapped for mobile so we need to
	unwrap them. Bit of spaghetti going on here.
*/

// Wrap elements for where to go nav item
const $whereToGoParent = $menuMainMenu.find('.where-to-go-parent');
const $whereToGoMenuItemLink = $whereToGoParent.find('.menu-item a');

// Wrap elements for whats on nav item
const $whatsOnParent = $menuMainMenu.find('.whats-on-parent');
const $whatsOnMenuItemLink = $whatsOnParent.find('.menu-item a');

// Wrap elements for things to do
// make columns
const $thingsToDoHighlights = $menuMainMenu.find('.things-to-do-highlights');
const $planYourTripHighlights = $menuMainMenu.find('.plan-your-trip-highlights');

// Wrap highlights links text
const $thingsToDoHighlightsLink = $thingsToDoHighlights.find('.menu-item a');
const $planYourTripHighlightsLink = $planYourTripHighlights.find('.menu-item a');

// Only run this on desktop
enquire.register("(max-width: 1024px)", () => {

	$subMenuWrapper.removeAttr('style');

	// upwrap stuff for where to go
	const $ctaAdded = $('.cta');

	$ctaAdded.unwrap();
	$ctaAdded.remove();
	$whereToGoMenuItemLink.find('.overlay').remove();

	// upwrap stuff for things to do
	if ($('.multi-col-menu').length > 0) {
		$('.col-start').unwrap();
		$thingsToDoHighlights.unwrap();
	}
	
	$thingsToDoHighlightsLink.each(function() {
		let $this = $(this);
		let text = $this.text();

		$this.find('span').replaceWith(text);
	});

	// upwrap stuff for whats on
	$whatsOnMenuItemLink.find('.overlay').remove();
	$whatsOnMenuItemLink.each(function() {
		let $this = $(this);
		let text = $this.text();

		$this.find('.item-text').replaceWith(text);
	});
	// upwrap stuff for plan your trip
	if ($('.plan-your-trip-highlights-wrapper').length) {
		$planYourTripHighlights.unwrap();
	}
	$planYourTripHighlightsLink.each(function() {
		let $this = $(this);
		let text = $this.text();

		$this.find('span').replaceWith(text);
	});

})
.register("(min-width: 1025px)", () => {

	$subMenuWrapper.removeAttr('style');

	$whereToGoMenuItemLink.wrapInner(itemTextWrapper);
	$('.item-text').append( '<div class="cta">Visit destination</div>' );
	$whereToGoMenuItemLink.append(overlay);

	$whatsOnMenuItemLink.wrapInner(itemTextWrapper);
	$whatsOnMenuItemLink.append(overlay);

	$('.col-start').each(function() {
		$(this).nextUntil('.col-end + *').addBack().wrapAll(column);
	});

	$thingsToDoHighlights.wrapAll('<div class="things-to-do-highlights-wrapper small-12 medium-6 large-3 columns"></div>');
	$planYourTripHighlights.wrapAll('<div class="plan-your-trip-highlights-wrapper small-12 medium-6 columns"></div>');

	$thingsToDoHighlightsLink.wrapInner('<span></span>');
	$planYourTripHighlightsLink.wrapInner('<span></span>');
});


/* +++++++++++++++++++++++++++++++++++++++++
	Start mobile nav functionality
++++++++++++++++++++++++++++++++++++++++++++ */

$mainMenuOpenBtn.on('click', function(){

	$body.addClass('nav-active');
});

$mainMenuCloseBtn.on('click', function(){

	$body.removeClass('nav-active');
});

// Hammer.js for slipe to close nav
const nav = document.querySelector('.main-nav');

if (nav) {
	const mc = new Hammer(nav);

	mc.on("swipeleft", function(ev) {
	    $body.removeClass('nav-active');
	});

	$mainMenuItem.on('click', function(e) {
		
		enquire.register("(max-width: 1024px)", () => {

			let $this = $(this);

			if ($this.text() !== 'Home') {

				$this.toggleClass('active');
			
				$this.next().slideToggle();
				e.preventDefault();
			}		
		});
	});

}






