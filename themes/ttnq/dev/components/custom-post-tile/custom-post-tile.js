let $postTile = $('.custom-post-tile');

$postTile.on('mouseenter', function() {
	const $this = $(this);
	const $tileContentDesc = $this.find('.custom-post-tile-text-view');

	$tileContentDesc.slideDown(320);
});

$postTile.on('mouseleave', function() {
	const $this = $(this);
	const $tileContentDesc = $this.find('.custom-post-tile-text-view');

	setTimeout(() => {
		$tileContentDesc.slideUp(320);
	}, 10);
});
