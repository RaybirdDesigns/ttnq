let $articleTile = $('.article-tile');

$articleTile.on('mouseenter', function() {
	const $this = $(this);
	const $tileContentDesc = $this.find('.article-tile-content-desc');

	$tileContentDesc.slideDown(320);
});

$articleTile.on('mouseleave', function() {
	const $this = $(this);
	const $tileContentDesc = $this.find('.article-tile-content-desc');

	setTimeout(() => {
		$tileContentDesc.slideUp(320);
	}, 1);
});
