import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import 'gsap/ScrollToPlugin';

// smooth scroll to top
const $backToTopBtn = $('.back-to-top-btn');

$backToTopBtn.on('click', () => {
	TweenLite.to(window, 2, { scrollTo: 'header', ease: Power1.easeInOut });
});

// mobile footer
const $menuItemHeaderMobile = $('.menu-item-header-mobile');

function toggleMenu() {
	let $this = $(this);
	let $thisMenu = $this.next('div').find('.menu');

	$this.toggleClass('active');

	$thisMenu.slideToggle();
}

$menuItemHeaderMobile.on('click', toggleMenu);
