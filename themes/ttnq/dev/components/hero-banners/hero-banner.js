import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';

// Poll for when the high res hero image loads. Add
// active class when it does. Achieves a fade in of hi res hero image

const heroBanner = document.getElementById('hero-banner');
const heroImg = document.querySelector('.hero-img');

let pollForHeroImg = () => {
	// don't run if no image
	if (heroImg) {

		if (heroImg.complete) {

			setTimeout(()=>{
				$(heroBanner).addClass('active');
			},500);
	    	
	        return;
	    }
	    setTimeout(pollForHeroImg, 200);
	}
	return;
};
pollForHeroImg();