import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import slick from 'slick-carousel';

const svgItems = [
	'location-1',
	'location-2',
	'location-3',
	'location-4',
	'location-5',
	'location-6',
	'location-7',
	'location-8',
	'location-9',
	'location-10',
];

// setup carousel
$('.map-carousel-carousel').slick({
	infinite: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	// fade: true,
	dots: false,
	prevArrow: "<div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div>",
	nextArrow: "<div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
	responsive: [
		{
			breakpoint: 1460,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				infinite: false,
			},
		},
	],
});

// set first item active on load
$('.item-location-1').addClass('active');

// Add class to active nav item
$('.map-carousel-carousel').on('afterChange', function(event, slick, currentSlide, nextSlide) {
	let id = $('.slick-current')
		.find('.carousel-item')
		.attr('id');

	svgItems.forEach(item => {
		$(`.item-${item}`).removeClass('active');
	});

	$(`.item-${id}`).addClass('active');
});

// control carousel with map items
svgItems.forEach((item, i) => {
	let $item = $(`.item-${item}`);

	$item.on('click', () => {
		svgItems.forEach(item => {
			$(`.item-${item}`).removeClass('active');
		});

		$('.map-carousel-carousel').slick('slickGoTo', i);
		$item.addClass('active');
	});
});
