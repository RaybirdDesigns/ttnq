import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import slick from 'slick-carousel';

$('.prod-gallery-carousel').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
  centerMode: true,
  variableWidth: true,
	dots: false,
  rows: 0,
	prevArrow:"<div class='arrow-wrapper'><div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div></div>",
	nextArrow:"<div class='arrow-wrapper'><div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div></div>",
	responsive: [
    {
      breakpoint: 1024,
      centerMode: true,
      variableWidth: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      }
    },
    {
      breakpoint: 640,
      centerMode: true,
      variableWidth: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    	dots: true,
    	infinite: false,
      }
    }
  ]
});

$('.prod-gallery-carousel-control').slick({
  infinite: false,
  slidesToShow: 10,
  slidesToScroll: 1,
  rows: 0,
  asNavFor: '.prod-gallery-carousel',
  dots: false,
  arrows: false,
  focusOnSelect: true,
});

$('.prod-gallery-carousel').on('beforeChange', function(event,slick,slide,nextSlide) {
  $('.prod-gallery-carousel-control').find('.slick-slide').removeClass('slick-current').eq(nextSlide).addClass('slick-current');
  $('.prod-gallery-carousel-control').slick('slickGoTo', nextSlide);
});