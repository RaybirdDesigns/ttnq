import 'expose-loader?$!jquery';
import 'expose-loader?jQuery!jquery';
import slick from 'slick-carousel';

$('.influencer-post-carousel').slick({
	infinite: true,
	slidesToShow: 5,
	slidesToScroll: 2,
	dots: false,
	prevArrow:"<div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div>",
	nextArrow:"<div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
	responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    	dots: true,
    	infinite: false,
      }
    }
  ]
});