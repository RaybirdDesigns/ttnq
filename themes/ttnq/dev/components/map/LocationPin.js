import React, { Component } from 'react';
import autoBind from 'auto-bind';

class LocationPin extends Component {
    constructor() {
        super();
        autoBind(this);
    }

    componentDidMount() {
        if (this.props.index === 0 && this.props.isDesktop) {
            this.pin.click();
        }
    }

    getActiveListing(index) {
        this.props.setListingIndex(index);
        this.props.showDetailMobile();
    }

    render() {
        const isDesktop = this.props.isDesktop;
        const { index, listingIndex } = this.props;
        const { title, image, category } = this.props.listing;

        return (
            <div className={`map-pin ${index === listingIndex ? 'active' : ''}`}>
                <div className="map-pin__info">
					<p>{title}</p>
                    <div className="image" style={{ backgroundImage: `url(${image})` }} />
                </div>
                <button
                    aria-label={`See detail about ${title}`}
                    ref={pin => (this.pin = pin)}
                    onClick={() => this.getActiveListing(this.props.index)}
                    className={`map-pin__dot ${category}`}
                />
            </div>
        );
    }
}

export default LocationPin;
