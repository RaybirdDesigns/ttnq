import React from 'react';
import ReactDOM from 'react-dom';
import MapApp from './MapApp.js';

// render app to dom
ReactDOM.render(<MapApp />, document.getElementById('map-root'));
