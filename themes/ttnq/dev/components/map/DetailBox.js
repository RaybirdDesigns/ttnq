import React, { Component } from 'react';
import autoBind from 'auto-bind';
import includes from 'lodash.includes';

class DeatilaBox extends Component {
    constructor() {
        super();
        autoBind(this);
    }

    handleClose() {
        this.props.hideDetailMobile();
    }

    handleAddFavourite(id) {
        this.props.setFavourites(id);
    }

    render() {
        const props = this.props;
        let { category, excerpt, id, image, link, title, bookeasy } = props.listing;
        const ctaText =
            bookeasy && category !== 'destinfo' ? 'Book Now' : category === 'accomm' ? 'Read More' : 'See More';

        return (
            <div className="map-detail">
                <div className="map-detail__image" style={{ backgroundImage: `url(${image})` }} />
                <div className="map-detail__body">
                    <h2 dangerouslySetInnerHTML={{ __html: title }} />
                    <div dangerouslySetInnerHTML={{ __html: excerpt }} />
                    <a target="_blank" className={`btn btn-medium btn-ghost green btn-margin ${category}`} href={link}>
                        {ctaText}
                    </a>
					<a onClick={() => this.handleClose()} className={`btn btn-medium btn-ghost green gotomap btn-margin ${category}`}>
                        Go To Map
                    </a>
                </div>
            </div>
        );
    }
}

export default DeatilaBox;
