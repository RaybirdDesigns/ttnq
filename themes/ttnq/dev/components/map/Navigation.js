import React, { Component } from 'react';
import autoBind from 'auto-bind';
import isEmpty from 'lodash/isempty';

class Navigation extends Component {
    constructor() {
        super();
        autoBind(this);

        this.state = {
            activeCat: '',
            activeSubCat: '',
        };
    }

    componentDidMount() {
        const props = this.props;

        if (!isEmpty(props.navData.bespoke)) {
            console.log('Bespoke Being Used');
            this.setState({
                activeCat: 'bespoke',
            });
        } else {
            const cat = Object.keys(props.navData.listings)[0];
            this.setState({
                activeCat: cat,
            });
            if( typeof props.navData.listings[cat][1] !== "undefined") {
                this.setState({
                    activeSubCat: props.navData.listings[cat][1].slug,
                });
            }

        }
    }

    handleBespokeClick(e) {
        const activeCat = e.target.dataset.bespoke;
        this.setState({ activeCat }, () => {
            this.props.getBespokeListings();
        });
    }

    handleCategoryClick(cat, e) {
        const state = this.state;
        const activeCat = e.target.dataset.cat;
        this.setState(
            {
                activeCat,
                activeSubCat: cat !== state.activeCat ? '' : state.activeSubCat, // only clear if different parent cat clicked
            },
            () => {
                this.props.getCategoryListings(cat);
            },
        );
    }

    handleSubCategoryClick(cat, subCat, e) {
        const activeSubCat = e.target.dataset.subcat;
        this.setState({ activeSubCat }, () => {
            this.props.getSubCategoryListings(cat, subCat);
        });
    }

    handleFavouriteClick(e) {
        const activeCat = e.target.dataset.cat;
        this.setState({ activeCat }, () => {
            this.props.getFavourites();
        });
    }

    renderMarkup(string) {
        return { __html: string };
    }

    render() {
        const state = this.state;
        const { bespoke, listings } = this.props.navData;
        const listingSlugs = Object.keys(listings);
        const noListings = isEmpty(listings);
        // const favouriteCount = this.props.favourites.length;
        let categoryListingsPath;
        switch (state.activeCat) {
            case 'accommodation':
                categoryListingsPath = '/listing/accommodation/';
                break;
            case 'product':
                categoryListingsPath = '/things-to-do/';
                break;
            case 'event':
                categoryListingsPath = '/whats-on/';
                break;
            default:
                categoryListingsPath = '/listings/';
        }

        const bespokeTab = !isEmpty(bespoke) ? (
            <li className={`bespoke ${state.activeCat === 'bespoke' ? 'active' : ''}`}>
                <button data-bespoke="bespoke" onClick={e => this.handleBespokeClick(e)}>
                    {bespoke.buttonText}
                </button>
            </li>
        ) : null;

        return (
            <div
                className={`map-nav ${
                    noListings || state.activeCat === 'bespoke' || state.activeCat === 'favourite' ? 'active' : ''
                } ${listingSlugs.length > 0 ? 'has-listings' : ''}`}>
                <ul className="map-nav__cat">
                    {bespokeTab}
                    {listingSlugs.map((cat, i) => {
                        // console.log(cat);
                        return (
                            <li key={i} className={`category-parent ${state.activeCat === cat ? 'active' : ''}`}>
                                <button
                                    data-cat={cat}
                                    onClick={e => this.handleCategoryClick(cat, e)}
                                    // dangerouslySetInnerHTML={cat !== 'product' ?
                                    //     this.renderMarkup(listings[cat][0]) :
                                    //     this.renderMarkup('Activities & Tours')}
                                    dangerouslySetInnerHTML={this.renderMarkup(listings[cat][0])}
                                />
                                <ul className={`row map-nav__subcat ${cat}`}>
                                    {listings[cat].map((item, i) => {
                                        if (i > 0) {
                                            return (
                                                <li
                                                    key={i}
                                                    className={`sub-category ${
                                                        state.activeSubCat === item.slug ? 'active' : ''
                                                    }`}>
                                                    <button
                                                        data-subcat={item.slug}
                                                        onClick={e => this.handleSubCategoryClick(cat, item.slug, e)}
                                                        dangerouslySetInnerHTML={this.renderMarkup(item.label)}
                                                    />
                                                </li>
                                            );
                                        }
                                    })}
                                </ul>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

export default Navigation;
