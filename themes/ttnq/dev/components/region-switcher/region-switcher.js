/* global $ */
import Cookies from 'js-cookie';

const $body = $('body');
// The region as reported by cloudflare
const actualLocale = $body.data('locale');
// The value of the locale cookie
let cookieLocale = Cookies.get('locale');
// Main region
const primaryLocale = 'AU';

const $geoSwitcherFooterSelect = $('#geo-switcher-footer-select');
const $geoSwitcherFooterMask = $('.geo-switcher-footer-mask');

// Object for mapping region id to flag img and full country name
const localeMap = {
	AU:
		'<img class="locale-flag" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABvklEQVR42o2Tv0tCURTHrRdBSkTSIBKhQVHQqEtDodXU7NOK13j8UUo/JaSIhoqeS2Mh0dBQtklQLQ3i9CD/hDerLdL2FB+ne6Qu4vVBFz7D497v975zvufauhcAjDCWGHHG8S8xRoAxLAi6xMuH6+uqOrdYcts3a/390CLsdqh6vVAMh0GlM1bi1UKhkG82mwZ2LLbDkSQwfD7I01nh5hufL/9ps2EXJBQgE/4nVFdallVNkoz/GLjdaVTVNyMSiWapX2QQKJfLRey9BIOxsT1MpZ5wZmarRM0mg1i9Xq+6XAd4dvaClco36rf3+DExi2vjERL1hDW2RkmRwbFpmq2/jcHBBG5s3KH2/ol6KMQFCwtZnJ+/4t99fdBi2pO2AUXF3S1IJh8R4KEjkWiDaTPtEihnUWQNlRsMXlb/SgjQkPj9F+3NgYGYtZjfHsOpqThv4vD29n5W03Tj/PwVnc7dfxiAIcvQjpEPEk2hrn/h9PSJpXB0dAeHhrb4IAmjnMlcP09OHjVEMXU9irlcsaEop8Iod5qs0IPxeKBEObOoTJZQ0+GACvVJUeIqnRGEFs85QTlTVIwoI9jrOf8AhwJ4EhHUi7gAAAAASUVORK5CYII=" alt="australia" />Australia',
	JP:
		'<img class="locale-flag" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABJklEQVR42qVTS07DMBTsBQJcogeAG7SlKzacIv0cgdgXKCcpi+QESFRdJdkgIRb0AjTJhkU+NgKZNxIPqY9600QaKfKbGdvz/AbyC8PwnDAhLAjqF3PCiBBIvhRfR1G0yvN8W1VV0XXdF1CW5T5N0w1q4PjEN3Ecr+u6NtZadwxN0xhwwP23MwrGGCZ7AQ640LA4UEqt4M6kj9ede75duKezSwD/WDs4idb6HnnBYIT7/Ylf3tzm4so9DoYHwBpqzENOCBsGc4TEBezGIgnUmIeQ0SkYKCTNBRzZZ4Aa86AhrT7ZoG1bS9q7PlfY8xV6hxigJae2sd9Dkk85SZIHcrc+MWryKUuTqRimb0r7syiK9yzLeJimQuYd5yVBo1WEGWF8bJx/AHMUWte8+VKaAAAAAElFTkSuQmCC" alt="japan" />Japan',
	DE:
		'<img class="locale-flag" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzBJREFUeNrEl89PE0EUx3eX0nSxkW4xNhhKa2K3VRMDISk/QoyGEDz5gwRiooYb+Cd49Ogf4KW9aTyoJPy4aElFBWr642ARG5tujVpKQSLsgilsg+3W92BJSi1xpe36kpdNOtPvZ97MvJl55PDwMKHASPBmcBbcDG4Ep+U2EZwHT4Jz4Itutzt/mFA+v9dE/gWMwA7o3BeLxSyJRMImCEKzKIrGbDarww4ajSZD0zRvMBiSVquVs9vtCZIkp6ApUGoASsCtkiRdD4VCLRzHdWcyGaOSqdHpdDzLsj6n0zlPUdQEwMP/Ar68urp6bXp6eiCdTp8ijmB6vX65p6dn1GQyTQL8jRLwlWg0etPv9w/AdNYRZRgsw3ZXV9dzh8PxDOCeQjBV1Lc3Eonc8vl8t8uFoqHG3NzcHdSEAHsPDKpwTVOp1NVgMDgIo6ohKmSoFQgEBhmG2QD4GvwULoyYwo00MzPTn8vltESFDbS1qI2MkZERqhDcjrv3qBtJiaE2zGYLsvbBJOYppgxRZYvH493IgqhJXGNLCxwO9xTmaVkGDA+wPjgcFozYdgZOJEIlk1k2BJtPCIJZLbDMakKwsU4UG9QCy6wGBNO18oGvhsksmiL+kyFY/AVXm1pAmSUimN+m6XW1wDJrHcHJdYNhSS3wGsPgS2WJ2j1Q4OWgFvizxRJHJp5ciYWz9sTQgxB/TFfd02sro+M/PmYXCYlIUHs3FzkV4Gy+akeLDEmiPC6XK7+fTsGxQPu8kNYvVwsK2ivIQFbhtShlJWri0duL4/DdqTQUNUF7DL6TEK1U/PQJR5eaJscDzlEpT+Yq9ggALdREbddd9/tSTx8076uFC7vPnhsdoQENJWnLjRShoPkEoN7D3lz75oGOuRRv3Bi6NNvP6NONR11TnF45Um9xe01bW1up/31Z+3n8++vI+S1dbXbTVL95UqvJ0gpTRpj9dM778GXfix+b9U8B+u5Ah8b7ikuYToqS+jpZrrn19De20SiY9ZDvMJDdG20nq8mkIT9XeCYZ/mrl/By7CCmDJYwfoH/WUK3Kaqfios0uF21MUdEmyEVbDIu2ksAi8G8BBgD1D55il/kmTgAAAABJRU5ErkJggg==" alt="germany" />Germany',
};
// The html for the header switcher
const geoSwitcher = `<aside class="geo-switcher-header">
						<p>Choose another region to see content specific to your location.</p>
						<div class="geo-switcher-header-wrapper">
							<div class="geo-switcher-header-mask">Select a region</div>
							<select name="geo-switcher" class="geo-switcher-header-select" id="geo-switcher-header-select">
								<option selected="true" disabled="disabled" value="">Select your region</option>
								<option value="AU">Australia</option>
								<option value="JP">Japan</option>
								<option value="DE">Germany</option>
							</select>
						</div>
						<div class="geo-switcher-header-btn-wrapper">
							<button disabled class="btn geo-switcher-header-btn">Continue</button>
							<button class="geo-switcher-header-close"></button>
						</div>
					</aside>`;

// If no cookie set or cookie is different to cloudflare show switcher
if (cookieLocale === undefined) {
	$body.prepend(geoSwitcher);
}

// Set the footer switcher to show either the cookie location or
// if not set the real location if it's one of the available if not
// just show AU
const availableLocales = Object.keys(localeMap);
const userLocale = availableLocales.indexOf(actualLocale) > -1 ? actualLocale : 'AU';
$geoSwitcherFooterMask.html(localeMap[userLocale]);

// Get selection header
let selectLocaleHeader;
$('#geo-switcher-header-select, #geo-switcher-footer-select').on('change', function() {
	const $this = $(this);
	const $geoSwitcherHeaderBtn = $('.geo-switcher-header-btn');
	selectLocaleHeader = $this.val();
	// inject the region name and flag img
	$('.geo-switcher-header-mask').html(localeMap[selectLocaleHeader]);

	// if no select disbale button
	if (selectLocaleHeader !== '') {
		$geoSwitcherHeaderBtn.removeAttr('disabled');
	} else {
		$geoSwitcherHeaderBtn.addAttr('disabled');
	}
});

// Get selection footer
let selectLocaleFooter;
$geoSwitcherFooterSelect.on('change', function() {
	const $this = $(this);
	const $geoSwitcherFooterBtn = $('.geo-switcher-footer-btn');
	selectLocaleFooter = $this.val();
	$geoSwitcherFooterMask.html(localeMap[selectLocaleFooter]);

	// if no slect disbale button
	if (selectLocaleFooter !== '') {
		$geoSwitcherFooterBtn.removeAttr('disabled');
	} else {
		$geoSwitcherFooterBtn.addAttr('disabled');
	}
});

// Set cookie and send back to home page.
function setCookieRedirectHeader() {
	Cookies.remove('locale');
	Cookies.set('locale', selectLocaleHeader, { expires: 365 });
	// window.location.href = window.location.origin;
	goToRegion(selectLocaleHeader);
}

// Set cookie and send back to home page.
function setCookieRedirectFooter() {
	Cookies.remove('locale');
	Cookies.set('locale', selectLocaleFooter, { expires: 365 });

	// window.location.href = window.location.origin;
	goToRegion(selectLocaleHeader);
}

function goToRegion(selectedLocation) {

	const path = window.location.pathname;

	switch (selectedLocation) {
		case 'AU':
			window.location.href = 'http://www.tropicalnorthqueensland.org.au' + path;
			break;
		case 'JP':
			window.location.href = 'https://www.tropicalnorthqueensland.org/jp';
			break;
		case 'DE':
			window.location.href = 'https://www.tropicalnorthqueensland.org/de';
			break;
		default:
			window.location.href = 'http://www.tropicalnorthqueensland.org.au/';
	}
}

// Remove the switcher
$('.geo-switcher-header-close').on('click', function() {
	let resetLocal;
	if (actualLocale === 'JP') {
		resetLocal = 'JP';
	} else if (actualLocale === 'DE') {
		resetLocal = 'DE';
	} else {
		resetLocal = 'AU';
	}

	Cookies.remove('locale');
	Cookies.set('locale', resetLocal, { expires: 365 });
	$('.geo-switcher-header').remove();
});

// Header switch: Set the cookie and redirect
$('.geo-switcher-header-btn').on('click', setCookieRedirectHeader);

// Header switch: Set the cookie and redirect
$('.geo-switcher-footer-btn').on('click', setCookieRedirectFooter);
