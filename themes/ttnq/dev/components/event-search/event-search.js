const $eventSearchForm = $('.event-search-form');
const $input = $eventSearchForm.find('.search-field');
const $submitButton = $eventSearchForm.find('button.btn');

// disable submit button if input in empty
$submitButton.prop('disabled', 'true');

$input.on('keyup', function() {

	$submitButton.prop('disabled', this.value === "" ? true : false);
});