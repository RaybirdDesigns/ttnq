import slick from 'slick-carousel';

$('.itineraries-carousel').slick({
	infinite: true,
	slidesToShow: 2,
	slidesToScroll: 1,
	dots: false,
	prevArrow: "<div class='slick-prev-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></div>",
	nextArrow: "<div class='slick-next-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
	responsive: [
		// {
		// 	breakpoint: 1024,
		// 	settings: {
		// 		slidesToShow: 2,
		// 		slidesToScroll: 1,
		// 		dots: false,
		// 	},
		// },
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 1.3,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				infinite: false,
			},
		},
	],
});
