import slick from 'slick-carousel';
import 'gsap/ScrollToPlugin';

$('.itinerary-scroll-nav-list').slick({
	infinite: false,
	slidesToShow: 5,
	slidesToScroll: 1,
	dots: false,
	prevArrow:
		"<button class='itinerary-scroll-nav-more nav-more-prev'><i class='fa fa-chevron-left' aria-hidden='true'></i></button>",
	nextArrow:
		"<button class='itinerary-scroll-nav-more nav-more-next'><i class='fa fa-chevron-right' aria-hidden='true'></i></button>",
	responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: true,
				dots: false,
				infinite: false,
			},
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: true,
				dots: false,
				infinite: false,
			},
		},
		{
			breakpoint: 370,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: true,
				dots: false,
				infinite: false,
			},
		},
	],
});

const $stickyNav = $('#itinerary-scroll-nav');
const $window = $(window);

if ($stickyNav.length) {
	let distance = $stickyNav.offset().top;
	const stickyNavHeight = $stickyNav.outerHeight();

	let lastId;
	const topMenu = $('#itinerary-scroll-nav');
	const topMenuHeight = topMenu.outerHeight();
	// All list items
	const menuItems = topMenu.find('a');
	// Anchors corresponding to menu items
	const scrollItems = menuItems.map(function() {
		var item = $($(this).attr('href'));
		if (item.length) {
			return item;
		}
	});

	$window.on('resize', () => {
		distance = $stickyNav.offset().top;
	});

	$window.scroll(function() {
		// Get container scroll position
		let fromTop = $(this).scrollTop() + topMenuHeight;

		// Get id of current scroll item
		let cur = scrollItems.map(function() {
			if ($(this).offset().top < fromTop) return this;
		});
		// Get the id of the current element
		cur = cur[cur.length - 1];
		let id = cur && cur.length ? cur[0].id : '';

		if (lastId !== id) {
			lastId = id;
			// Set/remove active class
			menuItems
				.parent()
				.removeClass('active')
				.end()
				.filter("[href='#" + id + "']")
				.parent()
				.addClass('active');
		}

		if ($window.scrollTop() >= distance) {
			$stickyNav.addClass('active');

			if (!$('.packer').length) {
				$stickyNav.after(`<div class="packer" style="height:${stickyNavHeight}px;"></div>`);
			}
		} else if ($window.scrollTop() <= distance) {
			$stickyNav.removeClass('active');
			$('.packer').remove();
		}
	});
}

// Scroll to element
const $scrollBtn = $('[data-nav-scroll-to]');

$scrollBtn.on('click', function(e) {
	let $this = $(this);
	let sectionId = `#${$this.data('nav-scroll-to')}`;

	let theOffSet = 45;

	TweenLite.to(window, 2, { scrollTo: { y: sectionId, offsetY: theOffSet, ease: Power1.easeInOut } });

	e.preventDefault();
});
