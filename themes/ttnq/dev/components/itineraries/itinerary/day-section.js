const $dayOneSection = $('#day-1');
const itineraryLeftTextdayOne = $dayOneSection.find('> .row .itinerary-distance-left').text();
const itineraryRightTextdayOne = $dayOneSection.find('> .row .itinerary-distance-right').text();
const $itineraryStart = $('.itinerary-start');
const $itineraryLeftTextItineraryStart = $itineraryStart.find('.itinerary-distance-left');
const $itineraryRightTextItineraryStart = $itineraryStart.find('.itinerary-distance-right');

// console.log(itineraryLeftTextdayOne);

// Polyfill for object-fit
const siteImages = document.querySelectorAll('img.site-image');

// Copy distance and time to itinerary head
$itineraryLeftTextItineraryStart.text(itineraryLeftTextdayOne);
$itineraryRightTextItineraryStart.text(itineraryRightTextdayOne);
