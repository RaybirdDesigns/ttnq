import React, { Component } from 'react';
import Dotdotdot from 'react-dotdotdot';
import SVG from 'react-inlinesvg';
// import objectFitImages from 'object-fit-images';

import Highlights from './Highlights';

class Itinerary extends Component {
	constructor() {
		super();
		this.renderIcons = this.renderIcons.bind(this);
	}

	renderIcons() {
		// const props = this.props.data;
		const icons = this.props.data.icons;

		if (icons && icons.length > -1) {
			return (
				<div className="itinerary-icon-wrapper">
					{icons.map((icon, i) => {
						if (icon !== null) {
							return <SVG key={i} className="itinerary-icon" src={icon} />;
						}
					})}
				</div>
			);
		}
	}

	render() {
		const {id, location, distance, alt, list_image, list_image_medium, map, excerpt, title, link, highlights} = this.props.data;
		// const acf = this.props.data.acf;
		const locationRender = location ? <p className="location">{location}</p> : null;
		const distanceRender = distance ? <p className="distance">{distance}</p> : null;

		return (
			<div className="itinerary-item clearfix">
				<div className="itinerary-item-image">
					<img
						src={list_image}
						srcSet={`${list_image} 1024w, ${list_image_medium} 520w`}
						alt={alt}
					/>
					<Highlights highlights={highlights} id={id} map={map} />
				</div>
				<div className="itinerary-item-text">
					<div className="itinerary-item-text-content">
						<h2>{title}</h2>
						{locationRender} {distanceRender}
						<Dotdotdot clamp={5}>
							<div dangerouslySetInnerHTML={{ __html: excerpt }} />
						</Dotdotdot>
						{this.renderIcons()}
						<a href={link}>View Itinerary</a>
					</div>
				</div>
			</div>
		);
	}
}

export default Itinerary;
