import React, { Component } from 'react';

class Highlights extends Component {
	constructor() {
		super();
	}

	render() {
		const highlights = this.props.highlights;
		const map = this.props.map;
		const mapImage = map ? <img src={map} /> : null;

		return (
			<div className="itinerary-highlights">
				{mapImage}
				<div className="highlights-text">
					<h3>Highlights</h3>
					<ul className="highlights-text-list">
						{highlights.map((highlight, i) => {
							return <li key={i}>{highlight.highlight}</li>;
						})}
					</ul>
				</div>
			</div>
		);
	}
}

export default Highlights;
