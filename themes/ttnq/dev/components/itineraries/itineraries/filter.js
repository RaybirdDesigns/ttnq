import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import $ from 'jquery';

class Filter extends Component {
	constructor() {
		super();
		this.handleOpenDropwDown = this.handleOpenDropwDown.bind(this);
		this.renderlengthDropdown = this.renderlengthDropdown.bind(this);
		this.renderSightsDropdown = this.renderSightsDropdown.bind(this);
		this.handleClickOutside = this.handleClickOutside.bind(this);
		this.handleClickLength = this.handleClickLength.bind(this);
		this.handleClickSights = this.handleClickSights.bind(this);
		this.listWrapper = this.listWrapper.bind(this);
		this.state = {
			lengthOpen: false,
			sightsOpen: false,
		};
	}

	componentWillReceiveProps(nextProps) {
		// console.log(nextProps);
		this.setState({
			lengthQuery: nextProps.lengthQuery,
			sightsQuery: nextProps.sightsQuery,
			lengthText: nextProps.lengthText,
			sightsText: nextProps.sightsText,
		});
	}

	componentDidMount() {
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	listWrapper(node) {
		this.listWrapperNode = node;
	}

	handleClickOutside(event) {
		if (this.state.lengthOpen && !$.contains(this.listWrapperNode, event.target)) {
			this.setState({ lengthOpen: false });
		}

		if (this.state.sightsOpen && !$.contains(this.listWrapperNode, event.target)) {
			this.setState({ sightsOpen: false });
		}
	}

	handleOpenDropwDown(dropDown) {
		if (dropDown == 'sights') {
			this.setState({ sightsOpen: true, lengthOpen: false });
		} else {
			this.setState({ sightsOpen: false, lengthOpen: true });
		}
	}

	handleClickLength(e) {
		const lengthQuery = e.target.getAttribute('data-time');
		const lengthText = e.target.innerHTML;
		this.setState(
			{
				lengthQuery,
				lengthText,
				lengthOpen: false,
			},
			() => {
				this.props.filter({
					lengthQuery: lengthQuery,
					lengthText: lengthText,
					sightsQuery: this.state.sightsQuery,
					sightsText: this.state.sightsText,
				});
			},
		);
	}

	handleClickSights(e) {
		const sightsQuery = e.target.getAttribute('data-tax-id');
		const sightsText = e.target.innerHTML;
		this.setState(
			{
				sightsQuery,
				sightsText,
				sightsOpen: false,
			},
			() => {
				this.props.filter({
					sightsQuery: sightsQuery,
					sightsText: sightsText,
					lengthQuery: this.state.lengthQuery,
					lengthText: this.state.lengthText,
				});
			},
		);
	}

	renderlengthDropdown() {
		return (
			<div className="filter-wrapper" ref={this.listWrapper}>
				<ul className="list-reset length-hours">
					<li>
						<button onClick={e => this.handleClickLength(e)} data-time="any">
							any length
						</button>
					</li>
					<li>
						<button onClick={e => this.handleClickLength(e)} data-time="1_day">
							1 day
						</button>
					</li>
					<li>
						<button onClick={e => this.handleClickLength(e)} data-time="2_3_days">
							2 - 3 days
						</button>
					</li>
					<li>
						<button onClick={e => this.handleClickLength(e)} data-time="1_week">
							1 week
						</button>
					</li>
					<li>
						<button onClick={e => this.handleClickLength(e)} data-time="week_plus">
							more than 1 week
						</button>
					</li>
				</ul>
			</div>
		);
	}

	renderSightsDropdown() {
		const taxNames = Object.keys(this.props.taxObj);

		return (
			<div className="filter-wrapper" ref={this.listWrapper}>
				<ul className="list-reset length-hours">
					<li>
						<button onClick={e => this.handleClickSights(e)} data-tax-id="any">
							any sights
						</button>
					</li>
					{taxNames.map((name, i) => {
						return (
							<li key={i}>
								<button
									dangerouslySetInnerHTML={{ __html: name }}
									onClick={e => this.handleClickSights(e)}
									data-tax-id={this.props.taxObj[name]}
								/>
							</li>
						);
					})}
				</ul>
			</div>
		);
	}

	render() {
		const lengthDropdown = this.state.lengthOpen ? this.renderlengthDropdown() : null;
		const sightsDropdown = this.state.sightsOpen ? this.renderSightsDropdown() : null;
		const lengthText = this.state.lengthText ? this.state.lengthText : 'any length';
		const sightsText = this.state.sightsText ? this.state.sightsText : 'any sights';

		return (
			<div className="itinerary-filter">
				<div className="row">
					<div className="small-12 columns text-center">
						<span className="filter-sentence">
							{this.props.filterLengthText}{' '}
							<div className="itinerary-filter-text length">
								<button onClick={() => this.handleOpenDropwDown('length')}>{lengthText}.</button>
								<ReactCSSTransitionGroup
									transitionName="filter-dropdown"
									transitionEnterTimeout={200}
									transitionLeaveTimeout={200}>
									{lengthDropdown}
								</ReactCSSTransitionGroup>
							</div>
						</span>
						<span className="filter-sentence">
							{this.props.filterSightsText}{' '}
							<div className="itinerary-filter-text sights">
								<button onClick={() => this.handleOpenDropwDown('sights')}>{sightsText}</button>
								<ReactCSSTransitionGroup
									transitionName="filter-dropdown"
									transitionEnterTimeout={200}
									transitionLeaveTimeout={200}>
									{sightsDropdown}
								</ReactCSSTransitionGroup>
							</div>
						</span>
					</div>
				</div>
			</div>
		);
	}
}

export default Filter;
