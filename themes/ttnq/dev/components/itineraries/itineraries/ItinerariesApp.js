import React, { Component } from 'react';
import { FadeLoader } from 'react-spinners';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import $ from 'jquery';
import uniq from 'lodash/uniq';

import { lengthFilter, sightsFilter } from './data.js';

import Filter from './Filter.js';
import Itinerary from './Itinerary.js';

const Fragment = React.Fragment;

// Taxonomy array from wp
let taxObj = {};
sightsArr.forEach(item => {
	taxObj[item.name] = item.term_id;
});

// console.log(taxObj);

class ItinerariesApp extends Component {
	constructor() {
		super();
		this.appElement = document.getElementById('itineraries-root');
		this.baseUrl = this.appElement.dataset.base;
		this.baseApiUrl = this.baseUrl + '/wp-json/custom/v1/itineraries';
		this.itineraryType = this.appElement.dataset.itinerarytype;
		this.itineraryName = this.appElement.dataset.itineraryname;
		this.itineraryLengthText = this.appElement.dataset.lengthtext;
		this.itinerarySightsText = this.appElement.dataset.sightstext;
		this.ititiallyShow = Number(this.appElement.dataset.initiallyshow);
		this.filters = `?itinerary_type=${this.itineraryType}`;
		this.getData = this.getData.bind(this);
		this.renderSpinner = this.renderSpinner.bind(this);
		this.renderNoResults = this.renderNoResults.bind(this);
		this.renderLoadMoreButton = this.renderLoadMoreButton.bind(this);
		this.handleLoadMore = this.handleLoadMore.bind(this);
		this.filter = this.filter.bind(this);
		this.state = {
			itineraryType: this.itineraryName,
			loading: true,
			resultCount: 0,
			results: [],
			filteredResults: [],
			noResults: false,
			renderLimit: this.ititiallyShow,
			taxObj,
			lengthQuery: 'any',
			lengthText: 'any length',
			sightsQuery: 'any',
			sightsText: 'any sights',
			clean: true,
			filterLengthText: this.itineraryLengthText,
			filterSightsText: this.itinerarySightsText,
		};
	}

	componentWillMount() {
		this.getData();
	}

	getData() {
		this.setState(
			{
				loading: true,
			},
			() => {
				$.ajax({
					url: this.baseApiUrl + this.filters,
					dataType: 'json',
					success: rawData => {
						// count number of results
						const resultCount = rawData.length;
						// set state
						this.setState({
							results: rawData,
							filteredResults: rawData,
							resultCount,
							loading: false,
							noResults: resultCount === 0 ? true : false,
						});
					},
					error: error => {
						alert('error:', error);
					},
				});
			},
		);
	}

	filter(queryObj) {
		// console.log(queryObj);
		const data = this.state.results;
		const filteredResults = uniq(sightsFilter(lengthFilter(data, queryObj), queryObj));
		const noResults = filteredResults.length === 0 ? true : false;
		const lengthQuery = queryObj.lengthQuery;
		const lengthText = queryObj.lengthText;
		const sightsQuery = queryObj.sightsQuery;
		const sightsText = queryObj.sightsText;

		this.setState({
			filteredResults,
			noResults,
			lengthQuery,
			sightsQuery,
			lengthText,
			sightsText,
			resultCount: filteredResults.length,
			renderLimit: this.ititiallyShow,
			clean: false,
		});
	}

	renderNoResults() {
		const showAllObj = {
			lengthQuery: 'any',
			lengthText: 'any length',
			sightsQuery: 'any',
			sightsText: 'any sights',
		};
		const showSightsObj = {
			lengthQuery: 'any',
			lengthText: 'any length',
			sightsQuery: this.state.sightsQuery,
			sightsText: this.state.sightsText,
		};
		const noneOnLoad =
			this.state.noResults && this.state.clean ? (
				<div className="no-results-text text-center">
					<p>Sorry, we don't have any {this.state.itineraryType.toLowerCase()} itineraries yet.</p>
				</div>
			) : null;
		const noneOfLength =
			this.state.noResults && !this.state.clean && this.state.sightsQuery === 'any' ? (
				<div className="no-results-text text-center">
					<p>
						Sorry, we don't have any {this.state.lengthText} {this.state.itineraryType.toLowerCase()}'s yet.
					</p>
					<button onClick={() => this.filter(showAllObj)} className="btn btn-medium btn-ghost">
						See {this.state.itineraryType}'s
					</button>
				</div>
			) : null;
		const noneOfSights =
			this.state.noResults && !this.state.clean && this.state.lengthQuery === 'any' ? (
				<div className="no-results-text text-center">
					<p>
						Sorry, we don't have any {this.state.itineraryType.toLowerCase()}'s with{' '}
						{this.state.sightsText.toLowerCase()} yet.
					</p>
					<button onClick={() => this.filter(showAllObj)} className="btn btn-medium btn-ghost">
						See {this.state.itineraryType}'s
					</button>
				</div>
			) : null;
		const noneOfSightsOfLength =
			this.state.noResults &&
			!this.state.clean &&
			this.state.sightsQuery !== 'any' &&
			this.state.lengthQuery !== 'any' ? (
				<div className="no-results-text text-center">
					<p>
						Sorry, we don't have any {this.state.lengthText} {this.state.itineraryType.toLowerCase()}'s with{' '}
						{this.state.sightsText.toLowerCase()} yet.
					</p>
					<button onClick={() => this.filter(showSightsObj)} className="btn btn-medium btn-ghost">
						See {this.state.itineraryType}'s with {this.state.sightsText.toLowerCase()}
					</button>
				</div>
			) : null;

		if (this.state.noResults) {
			return (
				<Fragment>
					{noneOnLoad}
					{noneOfLength}
					{noneOfSights}
					{noneOfSightsOfLength}
				</Fragment>
			);
		}
	}

	renderSpinner() {
		if (this.state.loading) {
			return (
				<div className="loading-overlay">
					<div className="loading-overlay-spinner">
						<FadeLoader color={'#b5b5b5'} loading={this.state.loading} height={10} width={10} radius={10} />
					</div>
				</div>
			);
		}
	}

	handleLoadMore() {
		this.setState({
			renderLimit: this.state.renderLimit + 2,
		});
	}

	renderLoadMoreButton() {
		if (this.state.renderLimit < this.state.resultCount) {
			return (
				<div className="itineraries-load-more-btn-wrapper text-center">
					<button onClick={() => this.handleLoadMore()} className="btn btn-medium btn-ghost">
						Load More Itineraries
					</button>
				</div>
			);
		}
	}

	render() {
		const filteredResults = this.state.filteredResults;

		const itineraryItems = filteredResults.slice(0, this.state.renderLimit).map(result => {
			return (
				<div key={result.id} className="itinerary-item-wrapper">
					<Itinerary data={result} />
				</div>
			);
		});

		return (
			<Fragment>
				<Filter
					taxObj={this.state.taxObj}
					filter={this.filter}
					lengthQuery={this.state.lengthQuery}
					sightsQuery={this.state.sightsQuery}
					sightsText={this.state.sightsText}
					lengthText={this.state.lengthText}
					filterLengthText={this.state.filterLengthText}
					filterSightsText={this.state.filterSightsText}
				/>
				<ReactCSSTransitionGroup
					transitionName="itinerary-items"
					transitionEnterTimeout={500}
					transitionLeaveTimeout={500}>
					{itineraryItems}
				</ReactCSSTransitionGroup>
				{this.renderSpinner()}
				{this.renderNoResults()}
				{this.renderLoadMoreButton()}
			</Fragment>
		);
	}
}

export default ItinerariesApp;
