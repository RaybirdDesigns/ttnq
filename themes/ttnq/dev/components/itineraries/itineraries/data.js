import includes from 'lodash/includes';

export function lengthFilter(data, queryObj) {
	const filteredByLength = data.filter(post => {
		if (queryObj.lengthQuery === 'any') {
			return post;
		} else if (post.time === queryObj.lengthQuery) {
			return post;
		}
	});

	return filteredByLength;
}

export function sightsFilter(data, queryObj) {
	const filteredBySights = data.filter(post => {
		if (queryObj.sightsQuery === 'any') {
			return post;
		} else if (includes(post.sights, Number(queryObj.sightsQuery))) {
			return post;
		}
	});
	return filteredBySights;
}
