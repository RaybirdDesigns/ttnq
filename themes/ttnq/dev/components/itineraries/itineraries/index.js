import React from 'react';
import ReactDOM from 'react-dom';
import ItinerariesApp from './ItinerariesApp.js';

// render app to dom
ReactDOM.render(<ItinerariesApp />, document.getElementById('itineraries-root'));
