// Used on single itinerary template.
// Due to size file is injected into itinerary template. Click event listener is
// set in product.js

import pdfMake from 'pdfmake/build/pdfmake.js';
import font from 'pdfmake/build/vfs_fonts.js';

pdfMake.vfs = font.pdfMake.vfs;
const $pdfButton = $('.itineraries-pdf');
const title = $('.hero-standard-content h1').text();
const $quickInfoItems = $('.quick-info-item');
const $overviewP = $('.overview-body p');
const $heroImage = $('#main').find('style')[0];
const $sections = $('.itinerary-section');
const pdfName = title + ' Itinerary.pdf';
const isInspirational = itinerarySettings.type === 'inspirational' ? true : false;

// Get hero image data uri
const start = $heroImage.innerHTML.split('url(')[1];
const endIndex = start.indexOf("');");
const heroDataUri = start.substring(1,endIndex);


// Get quick info text
let quickInfoText = [];
if (!isInspirational) {
	
	$quickInfoItems.each(function(i, item) {
		const $item = $(item);
		const $itemP = $item.find('p');
		const itemText = $itemP[0].innerText;
		const heading = itemText.split('\n')[0];
		const subheading = itemText.split('\n')[1];
		const iconsObj = itinerarySettings.icons;

		const quickInfoTextObj = [
			{image: iconsObj[itinerarySettings.iconNames[i]], style: 'icon',fit: [25, 25], margin: [0,5]},
			{text: heading, style: 'quickInfoHeading', margin: [0,0,0,2]},
			{text: subheading, style: 'quickInfoSubheading', margin: [0,0,0,20]}
		];

		quickInfoText.push(quickInfoTextObj);
	});
}

// Get overview text
const overviewText = [];
$overviewP.each(function(i, para){
	const paraText = $(para)[0].innerText;

	const overviewPara = { text: paraText + '\n\n', style: 'overview' };

	overviewText.push(overviewPara);
});

// Get section text
const sectionText = [];
$sections.each(function(i, section) {
	const $section = $(section);
	const h3 = $section.find('h3').text().toUpperCase();
	const h2 = $section.find('h2').text();
	const sectionHeading = `${h3} – ${h2}`;
	const p = $section.find('p');
	const distanceKm = $section.find('.itinerary-distance-left')[0].innerText;
	const distanceTime = $section.find('.itinerary-distance-right')[0].innerText;
	const distance = distanceKm.length > 0 && distanceTime.length > 0 ? `${distanceKm}, ${distanceTime}` : '';
	const $detour = $section.next('.itinerary-detour');
	let detourHeading;
	let $detourP;
	let detourText;

	if ($detour.length > 0) {
		detourHeading = $detour.find('h3')[0].innerText;
		$detourP = $detour.find('p');

		const detourParas = [];
		$detourP.each(function(i, detourP) {
			detourParas.push([{text: $(detourP).text(),style: 'detourText', fillColor: '#e7e7e8', margin: [0,0,0,10]}]);
		});

		detourText = {
			table: {
				body: [
					[{
						text: detourHeading, style: 'detourHeading',
						fillColor: '#e7e7e8',
						margin: [0, 10, 0, 8]
					}],
					...detourParas
				]
			},
			layout: {
				dontBreakRows: true,
				defaultBorder: false,
				paddingLeft: function(i, node) { return 15; },
				paddingRight: function(i, node) { return 15; }
			}
		}
	}

	const sectionParas = [];
	$(p).each(function(i, p) {
		sectionParas.push({text: $(p).text() + '\n\n', fontSize: 10, margin: [0, 0] });
	});

	const textArr = [
		{
			table: {
				widths: ['*'],
				body: [
					[''],
				]
			},
			margin: [0,10,0,0],
			layout: {
				dontBreakRows: true,
				defaultBorder: false,
				paddingTop: function(i, node) { return -1.5; },
				paddingRight: function(i, node) { return 0; },
				paddingLeft: function(i, node) { return 0; },
				paddingBottom: function(i, node) { return 0.00001; },
				fillColor: itinerarySettings.color
			}
		},
		{ text: sectionHeading, style: 'sectionHead', margin: [0, 12, 0, -3] },
		{ text: distance , style: 'sectionDistance', margin: [0, 8] },
		...sectionParas,
		detourText
	];

	sectionText.push(textArr);
});

// Construct doc
const docDefinition = {
	footer: {
		text: 'See more itineraries at: tropicalnorthqueensland.org.au/plan-your-trip/roadtrips',
		alignment: 'center',
		color: itinerarySettings.color,
		fontSize: 9,
		margin: [10,10]
	},
	content: [
		{
			table: {
				widths: [379,120],
				body: [
					[{image: heroDataUri, fit: [379, 250]},
					{
						table: {
							body: [
								[{image: itinerarySettings.map, fit: [120, 120], margin: [0, 0, 0, 0]}],
								[{
									stack: [
										{text: 'Highlights', style: 'highlightsHeader', margin: [6,5,0,3]},
										{ul: itinerarySettings.highlights,style: 'highlightsList', margin: [6, 0, 0, 0]}
									]
								}],
							],
						},
						layout: {
							defaultBorder: false,
							paddingTop: function(i, node) { return -1; },
							paddingLeft: function(i, node) { return -2; },
							paddingRight: function(i, node) { return 0; },
							paddingBottom: function(i, node) { return 0; },
						}
					}]
				] 
			},
			layout: {
				dontBreakRows: true,
				defaultBorder: false,
				paddingTop: function(i, node) { return 0; },
				paddingLeft: function(i, node) { return 0; },
				paddingRight: function(i, node) { return 0; },
				paddingBottom: function(i, node) { return 0; }
			}
		},
		{text: title, style: 'header', margin: [0,10,0, 16] },
		{alignment: 'justify', columns: [
			...quickInfoText
		]},
		...overviewText,
		...sectionText,
	],
	defaultStyle: {
		font: 'roboto'
	},
	styles: {
		header: {
			fontSize: 24,
		},
		sectionHead: {
			fontSize: 12,
			color: itinerarySettings.color,
			bold: true
		},
		sectionDistance: {
			color: '#000000',
			fontSize: 10,
			bold: true
		},
		overview: {
			fontSize: 10,
			bold: true
		},
		quickInfoHeading: {
			fontSize: 10,
			alignment: 'center'
		},
		quickInfoSubheading: {
			fontSize: 10,
			bold: true,
			alignment: 'center'
		},
		icon: {
			alignment: 'center'
		},
		highlightsHeader: {
			color: itinerarySettings.color,
			bold: true,
			fontSize: 12,
		},
		highlightsList: {
			fontSize: 9,
		},
		detourHeading: {
			bold: true,
			color: itinerarySettings.color
		},
		detourText: {
			fontSize: 10,
		}
	}
};

pdfMake.fonts = {
   roboto: {
     normal: 'Roboto-Regular.ttf',
     bold: 'Roboto-Medium.ttf',
     italics: 'Roboto-Italic.ttf',
     bolditalics: 'Roboto-MediumItalic.ttf'
   }
}

function itineraryPDFGen() {
	pdfMake.createPdf(docDefinition).download(pdfName, ()=>{
		$pdfButton.removeClass('loading');
		$pdfButton.find('img').remove();
	});
}
window.itineraryPDFGen = itineraryPDFGen;
