<?php
/*
Single Post Template: Product Article
Description: Template for a product article
*/

// Category name and count
$primary_category = get_post_primary_category($post->ID, $term='category', $return_all_categories=false);
$first_category = $primary_category['primary_category']->slug;

$tripadvisor_loc_id = get_field('trip_advisor_id');

$theme_colour = get_field('theme_colour_product') ? get_field('theme_colour_product')[0] : '639083';

$prod_atdw_id = get_field('product_id') ? 'data-atdw-id="'.get_field('product_id').'"' : null;

// gallery background colour
switch($theme_colour) {
	case '639083' :
		$gallery_bg_colour = '274f2c';
		break;
	case 'CE975C' : 
		$gallery_bg_colour = '2f2920';
		break;
	case '6fa0ae' :
		$gallery_bg_colour = '1f5668';
		break;
	default:
		$gallery_bg_colour = '2f2920';
}

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-product/hero', 'product') ?>

<style>

	#facilities .facilities-list li .facilities-icon {
		fill: #<?php echo trim($theme_colour); ?>!important;
	}

	h2:before {
		border-bottom: solid 3px #<?php echo trim($theme_colour) ?>!important;
	}

	.btn:not(#tripadvisor-reviews-link):not(.citizen-btn):not(.ninja-forms-field) {
		border: solid 3px #<?php echo trim($theme_colour) ?>!important;
	}

	.btn:hover:not(#tripadvisor-reviews-link):not(.citizen-btn):not(.ninja-forms-field) {
		background-color: #<?php echo trim($theme_colour) ?>!important;
		color: #ffffff!important;
	}

	.btn:focus:not(#tripadvisor-reviews-link):not(.citizen-btn):not(.ninja-forms-field) {
		background-color: #<?php echo trim($theme_colour) ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	#gallery {
		background-color: #<?php echo $gallery_bg_colour ?>;
	}
	
</style>

<section <?php echo $prod_atdw_id; ?> id="description">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center component">
			<h2><?php the_title(); ?></h2>
			<span itemprop="description"><?php
			if (have_posts()) :
				while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile;
			endif;
			?></span>
		</div>
	</div>

	<?php
	$is_citizen = get_field('citizen_of_the_reef');
	$tripAdvisor_id = get_field('trip_advisor_id');
	$layout = $is_citizen && $tripAdvisor_id ? 'large-6' : null;

	$product_address_1 = get_field('product_address_1');
	$product_address_2 = get_field('product_address_2');
	$product_city_name = get_field('product_city_name');
	$product_state_name = get_field('product_state_name');
	$product_post_code = get_field('product_post_code');
	$product_country_name = get_field('product_country_name');

	$image_0 = get_field('product_image_0');
	$image_1 = get_field('product_image_1');
	$image_2 = get_field('product_image_2');
	$image_3 = get_field('product_image_3');
	$image_4 = get_field('product_image_4');
	$image_5 = get_field('product_image_5');
	$image_6 = get_field('product_image_6');
	$image_7 = get_field('product_image_7');
	$image_8 = get_field('product_image_8');
	$image_9 = get_field('product_image_9');
	?>

	<div class="row">
		<div class="small-12 medium-8 medium-offset-2">
			<?php if ($tripAdvisor_id) : ?>
			<div class="small-12 <?php echo $layout; ?> columns text-center">
				<h4 class="tripadvisor-heading">TripAdvisor Traveller Rating</h4>
				<div id="TA_cdsratingsonlynarrow474" class="TA_cdsratingsonlynarrow">
				<ul id="dtaV5yYadd" class="TA_linksdd z4ArtTPjdd">
				<li id="6o5WmSyadd" class="Yx3qJoh1dd">
				<a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png" alt="TripAdvisor"/></a>
				</li>
				</ul>
				</div>
				<a id="tripadvisor-reviews-link" target="_blank" class="btn btn-medium btn-ghost tripadvisor" href="#">Read the reviews <i class="fa fa-window-restore" aria-hidden="true"></i></a>
			</div>
			<?php endif; ?>
			<?php if ($is_citizen) : ?>
			<div class="small-12 <?php echo $layout; ?> columns text-center">
				<div>
					<img class="citizens-logo" src="<?php echo get_template_directory_uri() . '/images/logos/citizens-logo-abyss.svg'; ?>" alt="">
				</div>
				<a class="btn btn-medium btn-ghost blue citizen-btn scroll-down-btn-js" data-scroll-to="citizen" href="#">Learn more <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php 
$location = get_field('google_map_location');
if( !empty($location) ): ?>
<section id="map" class="collapse">
	<div class="row">
		<div class="columns text-center">
			<h2>Location</h2>
		</div>
	</div>
	<div class="clearfix">
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
		<div class="address-overlay">
			<h4><?php the_title(); ?></h4>
			<p>
			<?php echo $product_address_1; ?>
			<?php echo $product_address_2; ?><br/>
			<?php echo $post_code = $product_city_name ? $product_city_name . "," : null; ?>
			<?php the_field('prod_suburb'); ?><br/>
			<?php echo $product_state_name; ?>
			<?php echo $post_code = $product_post_code ? $product_post_code . "," : null; ?>
			<?php echo $product_country_name; ?>
			</p>
			<div>
				<a href="http://maps.google.com/?q=<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>" target="_blank" class="btn btn-medium btn-ghost lite-blue" href="">Open in google maps <i class="fa fa-window-restore" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php
$field = get_field_object('product_facilities');
$values = $field['value'];

if ($values) : ?>
<section id="facilities">
	<div class="row">
		<div class="small-12 columns text-center">
			<h2>Facilities</h2>
		</div>
	</div>
	<div class="row">
		<div class="small-12">
			<ul class="facilities-list list-reset list-inline clearfix">
				<?php foreach( $values as $value ) : ?>
			 	<li>
					<svg class="facilities-icon" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/features-sprite.svg#<?php echo $value['value']; ?>"></use>
	                </svg>
					<p><?php echo $value['label']; ?></p>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>
<?php endif; ?>

<?php
// Custom Gallery Images
$image_gallery = get_field('image_gallery') ? get_field('image_gallery') : array();
$listing_images = array($image_0, $image_1, $image_2, $image_3, $image_4, $image_5, $image_6, $image_7, $image_8, $image_9);
$listing_images_arr = array_filter($listing_images);
$gallery_count = count($image_gallery);
$listing_images_arr_count = count($listing_images_arr);
$image_count = $gallery_count + $listing_images_arr_count;

if ($image_count > 0): ?>
<section id="gallery" class="collapse-bottom">
	<div class="row text-center">
		<div class="small-12 columns component">
			<h2><?php the_title(); ?></h2>
		</div>
	</div>
	<div class="clearfix">
		<div class="prod-gallery-carousel component">
			<?php if (!empty($image_gallery)): ?>
				<?php foreach($image_gallery as $gallery_image): ?>
				<div class="prod-gallery-carousel-item">
					<img class="lazyload" data-object-fit="cover" <?php responsive_image($gallery_image['ID'], 'large', true); ?>>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php
			foreach($listing_images_arr as $image_index => $image): ?>
				<div class="prod-gallery-carousel-item">
					<img data-object-fit="cover" <?php adtw_image(1200, $image_index, false); ?>>
				</div>
			<?php endforeach; ?>
		</div>

		<?php if ($image_count > 1): ?>
		<div class="row">
			<div class="small-12">
				<div class="prod-gallery-carousel-control">
				<?php if ($image_gallery): ?>
					<?php foreach($image_gallery as $gallery_image): ?>
					<div class="prod-gallery-carousel-control-item">
						<img class="lazyload" data-object-fit="cover" <?php responsive_image($gallery_image['ID'], 'large', true); ?>>
					</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php 
				foreach($listing_images_arr as $thumbs_index => $thumb): ?>
					<div class="prod-gallery-carousel-control-item">
						<img data-object-fit="cover" <?php adtw_image(200, $thumbs_index, false); ?>>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>
<?php endif; ?>

<?php
/*
	Set dynamic events tiles
*/

// event priority sort query
$priority_wp_query = priority_event_sort(5, false, $first_category, null);
$priority_wp_query_posts = $priority_wp_query->posts;

// merge the two arrays together
$posts = $priority_wp_query_posts;

$event_count = count($posts);

// accomodate layouts for different numbers of events
$event_columns = $event_count == 1 ? '12' : ($event_count == 2 || $event_count == 3 ? '6' : '3');
$event_height = $event_count == 2 ? 'two-up' : ($event_count == 4 ? 'four-up' : null);
$one_up = $event_count == 1 ? 'one-up' : null;

?>

<?php //Change !$posts to $posts to reinitalise
if (!$posts): ?>
<section id="events">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center component">
			<?php the_field('what_near_by_text', 'option'); ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<ul class="list-reset tiles-5-up <?php echo $event_height; echo $one_up; ?>">
			<?php

				$i = 0;

				foreach( $posts as $post ):
					
					$i++;

					setup_postdata( $post ); ?>
				
				<li class="small-6 medium-6 large-<?php echo $event_columns; ?>">
					<?php include(locate_template('/components/custom-post-tile/custom-post-tile.php')); ?>
				</li>

				<?php endforeach; ?>

				<?php wp_reset_postdata(); ?>
			</ul>
		</div>
	</div>
	
	<?php 
	if ($event_count > 5) :
	$event_URL = add_query_arg(
		array(
			'post-type' => 'event',
			'category_name' => $first_category,
			), get_site_url() . '/post-list/' 
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost green btn-margin" href="<?php echo $event_URL; ?>">Browse all events <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
	<?php endif; ?>
</section>
<?php endif; ?>


<?php 
// check for posts in category before rendering
$args = array(
	'post_type' 	=> 'page',
	'category_name' => $first_category,
	'post_status'	=> 'publish',
	'post_parent'    => 11,
	'order'			=> 'DESC',
	'no_found_rows'	=> true,
	'update_post_term_cache' => false,
	'posts_per_page' => 4,
);

$query = new WP_Query($args);

$posts = $query->posts;

if (count($posts) >= 4) : ?>
<section id="nearby" class="collapse-bottom">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center component">
			<?php the_field('things_to_do_near_by_text', 'option'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="list-reset">
			<?php
				$i = 0;
				foreach( $posts as $post ):
					
					$i++;

					setup_postdata( $post ); ?>
						
				<li class="small-12 medium-6 large-3 float-left">
					<?php get_template_part('components/image-title-tile/image-title', 'tile') ?>
				</li>
					
			<?php endforeach; ?>

			<?php wp_reset_postdata(); ?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php if ($is_citizen) : ?>
<section id="citizen" class="citizen collapse">
	<div class="clearfix">
		<div class="citizen-left small-12 large-6 columns">
			<div class="citizen-content">
				<?php the_field('citizen_of_the_reef_text','option'); ?>
				<a class="btn btn-medium btn-ghost blue btn-margin" target="_blank" href="<?php the_field('citizen_of_the_reef_button_url','option'); ?>">Visit the website <i class="fa fa-window-restore" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="citizen-right small-12 large-6 columns"
			 data-responsive-background-image>
			<img <?php responsive_image(get_field('citizen_of_the_reef_image' , 'option'), 'full') ?> >
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (get_field('stackla_html_snippet')) : ?>
<?php include(locate_template('/components/stackla/stackla.php')); ?>
<?php endif; ?>

<script src="https://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=474&amp;locationId=<?php echo $tripadvisor_loc_id; ?>&amp;lang=en_US&amp;border=false&amp;display_version=2"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-oulh7EDnr1Za7X2z-tsyhX6c8C5Jlsg"></script>

<img class="show-for-sr" src="<?php the_field('product_pixel_url'); ?>">

<?php get_footer()?>