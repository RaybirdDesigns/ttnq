<?php
/**
 * Template Name: Search results
 */

// get thumbnamil
// gets base64 encoded and used in the before pseudo element

if (get_field('search_results_hero', 'option')) {
    $thumb_nail_url = wp_get_attachment_image_src( get_field('search_results_hero', 'option'), "hero-thumb" )[0];
} else {
    $thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'hero-thumb')[0];
}


?>

<?php get_header(); ?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<?php if (have_posts()) : ?>
<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <?php if (get_field('search_results_hero', 'option')) : ?>

        <img class="hero-img" <?php responsive_image(get_field('search_results_hero', 'option'), 'full'); ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

    <div class="hero-bottom-gradient"></div>
</div>
<?php else : ?>
<div id="hero-banner" class="search-hero" data-responsive-background-image>

    <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

</div>          
<?php endif; ?>

<div id="search-listing-root"></div>


<?php get_footer(); ?>