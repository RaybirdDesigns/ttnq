<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="show-for-sr">Search for:</span>
		<input data-swplive="true" type="search" class="search-field" placeholder="Search Tropical North Queensland" value="" name="s" title="Search for:" />
	</label>
</form>
