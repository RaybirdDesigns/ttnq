<?php
/**
 * Template Name: Mountain Bike
 *
 */
$category = get_the_category();
$first_category = !empty($category) ? $category[0]->slug : null;

$theme_colour = get_field('theme_colour');

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-experience/hero', 'experience'); ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.alternating-rows a {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px transparent;
	}

	.alternating-rows a:hover {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.alternating-rows a:focus {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	button.scroll-btn {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<?php if(get_field('text_content')): ?>
<section id="description-1">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('text_content'); ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (get_field('video')): ?>
<section id="video">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 medium-6 columns">
			<?php the_field('video_text'); ?>

			<?php if(get_field('video_btn_url')) : ?>
			<a class="btn btn-medium btn-ghost blue btn-margin" href="<?php the_field('video_btn_url'); ?>"><?php the_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('components/simple-banner/simple-banner'); ?>

<?php if( have_rows('alternating_rows') ): ?>
<section id="alternating-rows" class="collapse">
	<div class="row expanded">
		<?php

		$feature_index = 0;

	    while ( have_rows('alternating_rows') ) : the_row();

	    	$feature_index++;

	        include(locate_template('/components/alternating-rows/alternating-rows.php'));

	    endwhile;

		?>
	</div>
</section>
<?php endif; ?>

<?php

	// Show ad unit based on category
	// randomised if more than one
	$args = array(
	    'post_type'     => 'ad_unit',
	    'category_name' => $first_category,
	    'post_status'   => 'publish',
	    'orderby' 		=> 'rand',
	    'order'         => 'DESC',
	    'posts_per_page' => '1',
	    'no_found_rows' => true,
	    'update_post_term_cache' => false,
	    'posts_per_page' => 1,
	    'paged'          => false,
	);

	$wp_query = new WP_Query($args);

	$posts = $wp_query->posts;

	if ( have_posts() ) : ?>
	<section id="banner-1" class="collapse">
		<div class="small-12">

			<?php foreach( $posts as $post ): 
			
			setup_postdata( $post );
			
			?>
			
			<?php get_template_part('components/ad-units/banner', 'ad') ?>

			<?php endforeach; ?>
						
		</div>
	</section>
<?php
	endif;
	wp_reset_query();
?>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<?php if ( have_rows('related_pages') ): ?>
<section id="related-pages" class="collapse-bottom">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns text-center component">
			<?php the_field('related_page_text'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="list-reset">
			<?php
				$post_count = count(get_field('related_pages'));

				switch ($post_count) {
					case '2':
						$layout = 'medium-6 large-6';
						break;
					case '3':
						$layout = 'medium-4 large-4';
						break;
					case '4':
						$layout = 'medium-6 large-3';
						break;
				}

				while ( have_rows('related_pages') ) {

				the_row();

				$post_object = get_sub_field('featured_page');


				if ( $post_object ) {

					$post = $post_object;
					setup_postdata( $post_object );

					?>
					
					<li class="small-12 <?php echo $layout; ?> float-left">
						<?php get_template_part('components/image-title-tile/image-title', 'tile') ?>
					</li>
					
		<?php		wp_reset_postdata();
				}
			}
		?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('components/full-width-banner/full-width-banner'); ?>

<?php get_footer()?>