<?php
/**
 * Template Name: Post list
 */

// get query string values
$category = isset($_GET['category_name']) ? $_GET['category_name'] : '';
$tag = isset($_GET['tag']) ? $_GET['tag'] : '';
$post_type = isset($_GET['post-type']) ? $_GET['post-type'] : '';

// check if we're listing events
$is_event_list = $post_type == 'event' ? true : false;

// string manipulation for title
$cat_title = str_replace('-', ' ', $category);
$tag_title = str_replace('-', ' ', $tag);
$plural = $post_type != 'accommodation' ? 's' : '';

$title_name = $post_type == 'itinerary' ? 'Itineraries' : ucfirst($post_type) . $plural;

?>

<?php get_header()?>

<?php

$args = array(
    'post_type'     => $post_type,
    'category_name' => $category,
    'tag'           => $tag,
    'post_status'   => 'publish',
    'no_found_rows' => true,
    'update_post_term_cache' => false,
    'posts_per_page' => 1,
    'paged'          => false,
);

$wp_query = new WP_Query($args);

$posts = $wp_query->posts;

$total_posts = $wp_query->found_posts;

// get first post id for use in hero banner
$first_post_id = $posts[0]->ID;

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $first_post_id )) {
    $thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $first_post_id ), "medium" )[0];
} else {
    $thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<?php if ( have_posts() ) : ?>

<div id="hero-banner" class="hero-standard" data-responsive-background-image >

    <?php if (has_post_thumbnail( $first_post_id )) : ?>

        <img class="hero-img" <?php responsive_image(get_post_thumbnail_id( $first_post_id ), 'full'); ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

    <div class="hero-standard-content text-center">
        <?php if ($category) : ?>
        <h1 class="hero-standard-content-text"><?= $title_name ?> in <?php echo ucwords($cat_title); ?></h1>
        <?php elseif ($tag) : ?>
        <h1 class="hero-standard-content-text"><?php echo ucwords($tag_title); ?> <?=$title_name ?></h1>
        <?php else : ?>
        <h1 class="hero-standard-content-text">All <?=$title_name ?></h1>
        <?php endif; ?>
    </div>
    <div class="hero-bottom-gradient"></div>
</div>

<?php else: ?>

<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <div class="search-hero-content text-center">
        <h1 class="pagetitle">Sorry we couldn't find any <?php echo ucwords($post_type).$plural; ?> in <?php echo ucwords($cat_title); ?></h1>
    </div>
</div>

<?php endif; ?>

<?php if($is_event_list): ?>
<div id="event-reorder" class="event-reorder">
    <div class="row">
        <div class="small-12 columns">
            <input class="order-by-date" id="orderByDate" type="checkbox"><label for="orderByDate">Order events by start date</label>
        </div>
    </div>
</div>
<?php endif; ?>

<section id="post-list">
    <div class="row">
        <div id="post-list-container" class="small-12 columns">
            <!-- results ajaxed in here -->
        </div>
    </div>
</section>



<?php get_footer()?>