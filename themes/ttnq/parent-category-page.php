<?php
/**
 * Template Name: Parent Category
 */

$theme_colour = get_field('theme_colour');

?>

<?php get_header()?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.icon-tile-link .icon-tile-wrapper {
		border: solid 3px #<?php echo $theme_colour ?>;
	}

	.icon-tile-link:hover .icon-tile-wrapper,
	.icon-tile-link:focus .icon-tile-wrapper {
		background-color: #<?php echo $theme_colour ?>;
	}

	.icon-tile-link:hover h6,
	.icon-tile-link:focus h6 {
		color: #<?php echo $theme_colour ?>;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}
	
</style>

<?php get_template_part('components/hero-banners/hero-category/hero', 'category') ?>

<?php
if (have_posts()) :
	while (have_posts()) : the_post(); ?>

<section data-localize="notranslate" id="description-1" class="collapse-bottom">
	<div class="row">
		
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('flexible_content') ):

				$section_index = 0;

				// loop through the rows of data
				while ( have_rows('flexible_content') ) : the_row();

					$section_index++;
				    	// +++++++++++++++++++ Start: Text Block +++++++++++++++++++++++

				    if( get_row_layout() == 'text_block' ):

				        $grey_background = get_sub_field('grey_background') ? 'grey-bg' : null;

				        echo '<div class="flexible-text-wrapper flexible-text-wrapper-'.$section_index.' '.$grey_background.'">
				        		<div>
				        			<div class="text-block clearfix small-12 medium-12 large-8 large-offset-2">';

				        	if( get_sub_field('heading') ):

				        		echo '<h2>'.get_sub_field('heading').'</h2>';

				        	endif;

				        	// ++++++++++++++++++++++++

				        	if(get_sub_field('text') && get_sub_field('columns') == 1):
				        	
								echo '<div class="small-12 columns">';
				        			the_sub_field('text');
				        		echo '</div>';


				        	elseif(get_sub_field('text') && get_sub_field('columns') == 2):

				        		echo '<div class="clearfix"><div class="small-12 medium-12 large-6 columns">';

				        			the_sub_field('text');

				        		echo '</div>';

				        	endif;

				        	// +++++++++++++++++++++++++

				        	if(get_sub_field('text_2') && get_sub_field('columns') == 2):

				        		echo '<div class="small-12 medium-12 large-6 columns">';

				        			the_sub_field('text_2');

				        		echo '</div></div>';

				        	endif;

				        	// +++++++++++++++++++++++++

				        echo '</div></div></div>';

				        endif;

					        // +++++++++++++++++++ Start: Text and Image +++++++++++++++++++++++

					        if( get_row_layout() == 'text_and_image' ):

					        	$grey_background = get_sub_field('grey_background') ? 'grey-bg' : null;

					        	echo '<div class="flexible-text-and-image-wrapper '.$grey_background.'">';

					        		if( get_sub_field('heading') ):

						        		echo '<div class="component">
						        				<h2 class="text-center small-12 medium-12 large-6 large-offset-3">'.get_sub_field('heading').'</h2></div>';

						        	endif;

						        	// +++++++++++++++++++++++++

						        	// check if the nested repeater field has rows of data
						        	if( get_sub_field('image_content') ):

						        		$image_dir = get_sub_field('image_direction') ? 'direction-right' : null;
						        		$count = count(get_sub_field('image_content'));
						        		$i = 0;

									 	echo '<div class="clearfix flexible-image-wrapper"><div class="small-12 large-10 large-offset-1">';

									 	// loop through the rows of data
									    while ( have_rows('image_content') ) : the_row();

									    	$i++;
											$caption = get_sub_field('caption') && $count > 1 ? '<div class="flexible-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
											$single_caption = $count == 1 && get_sub_field('caption') ? '<div class="flexible-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : '<div class="flexible-image-caption-below-mobile"><p>'.get_sub_field('caption').'</p></div>';

											switch($count) {
												case 1 :
													$layout = 'one-up-';
													break;
												case 2 : 
													$layout = 'two-up-';
													break;
												case 3 :
													$layout = 'three-up-';
													break;
												case 4 :
													$layout = 'four-up-';
													break;
												default:
													$layout = 'one-up-';
											}

											echo '<div class="lightbox-image flexible-image '.$image_dir.' '.$layout.$i.' flexible-image-'.$i.'" data-responsive-background-image>
													<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

										endwhile;

										echo '</div></div>';

									endif;

									if( get_sub_field('video') ):

										$has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

										echo '<div><div class="small-12 large-10 large-offset-1"><div class="flexible-video-wrapper '.$has_caption.'">';
										the_sub_field('video');
										echo '</div></div></div>';

										if (get_sub_field('video_caption')) :
											echo '<div class="flexible-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
										endif;

									endif;

									// +++++++++++++++++++++++++++++++++++++++++++++++++

									if( get_sub_field('text') ):

						        		echo '<div class="row">
						        				<div class="clearfix"><div class="small-12 medium-12 large-8 large-offset-2 columns">';

					        			the_sub_field('text');

					        			echo '</div></div>';

						        	endif;

						        	// +++++++++++++++++++++++++++++++++++++++++++++++++

						        	if( get_sub_field('button_url') || get_sub_field('button_link') ):

						        		$button_url = get_sub_field('button_url') ? get_sub_field('button_url') : get_sub_field('button_link');
						        		$icon = get_sub_field('button_url') ? 'window-restore' : 'chevron-right';
						        		$target = get_sub_field('button_url') ? 'target="_blank"' : null;

						        		echo '<div class="text-center"><a '.$target.' class="flexible-button btn btn-medium btn-ghost" href="'.$button_url.'">'.get_sub_field('button_text').' <i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></div>';

						        	endif;

					        	echo '</div></div>';

					        endif;

					    	// +++++++++++++++++++ End: Text and image ++++++++++++++++++++++++++++++++++++

				endwhile;

			endif;?>

	</div>
</section>
<?php 
	endwhile;

endif; ?>

<section data-localize="notranslate" id="looking-for-nav" class="category" data-responsive-background-image>
	<img <?php responsive_image(get_field('looking_for_section_bg'), 'full') ?> >
	<div class="row looking-for-content">
		<?php if (get_field('in_page_nav_heading')) : ?>
		<div class="small-12 columns text-center component">
			<?php the_field('in_page_nav_heading'); ?>
		</div>
		<?php endif; ?>
		<ul class="list-reset clearfix">
		<?php
			if ( have_rows('looking_for_things_to_do_items') ) {
				$show_icons = get_field('hide_icons');

				while ( have_rows('looking_for_things_to_do_items') ) {

					the_row();

					$post_object = get_sub_field('thing_to_do');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object ); ?>

						<li class="looking-for-nav-item small-6 medium-4 large-2">
							<div class="icon-tile clearfix">
								<a class="icon-tile-link" href="<?php the_permalink(); ?>">
									<?php if (!$show_icons) : ?>
									<div class="icon-tile-wrapper">
										<svg class="icon-tile-icon" role="presentation">
											<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#<?php echo $post_slug = get_post_field( 'post_name', get_post() ); ?>"></use>
		        </use>

									    </svg>
									</div>
									<?php endif; ?>
								    <h6><?php the_title(); ?>&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
								</a>
							</div>
						</li>


		<?php			wp_reset_postdata();
					}
				}
			}
		?>
		</ul>
	</div>
</section>

<?php
if (have_posts()) :
	while (have_posts()) : the_post(); ?>

<section data-localize="notranslate" id="description-2" class="collapse-bottom">
	<div class="row">
		
			<?php

			// check if the flexible content field has rows of data
			if( have_rows('flexible_content_2') ):

				$section_index = 0;

				// loop through the rows of data
				while ( have_rows('flexible_content_2') ) : the_row();

					$section_index++;
				    	// +++++++++++++++++++ Start: Text Block +++++++++++++++++++++++

				    if( get_row_layout() == 'text_block' ):

				        $grey_background = get_sub_field('grey_background') ? 'grey-bg' : null;

				        echo '<div class="flexible-text-wrapper flexible-text-wrapper-'.$section_index.' '.$grey_background.'">
				        		<div>
				        			<div class="text-block clearfix small-12 medium-12 large-8 large-offset-2">';

				        	if( get_sub_field('heading') ):

				        		echo '<h2>'.get_sub_field('heading').'</h2>';

				        	endif;

				        	// ++++++++++++++++++++++++

				        	if(get_sub_field('text') && get_sub_field('columns') == 1):
				        	
								echo '<div class="small-12 columns">';
				        			the_sub_field('text');
				        		echo '</div>';


				        	elseif(get_sub_field('text') && get_sub_field('columns') == 2):

				        		echo '<div class="clearfix"><div class="small-12 medium-12 large-6 columns">';

				        			the_sub_field('text');

				        		echo '</div>';

				        	endif;

				        	// +++++++++++++++++++++++++

				        	if(get_sub_field('text_2') && get_sub_field('columns') == 2):

				        		echo '<div class="small-12 medium-12 large-6 columns">';

				        			the_sub_field('text_2');

				        		echo '</div></div>';

				        	endif;

				        	// +++++++++++++++++++++++++

				        echo '</div></div></div>';

				        endif;

					        // +++++++++++++++++++ Start: Text and Image +++++++++++++++++++++++

					        if( get_row_layout() == 'text_and_image' ):

					        	$grey_background = get_sub_field('grey_background') ? 'grey-bg' : null;

					        	echo '<div class="flexible-text-and-image-wrapper '.$grey_background.'">';

					        		if( get_sub_field('heading') ):

						        		echo '<div class="component">
						        				<h2 class="text-center small-12 medium-12 large-6 large-offset-3">'.get_sub_field('heading').'</h2></div>';

						        	endif;

						        	// +++++++++++++++++++++++++

						        	// check if the nested repeater field has rows of data
						        	if( get_sub_field('image_content') ):

						        		$image_dir = get_sub_field('image_direction') ? 'direction-right' : null;
						        		$count = count(get_sub_field('image_content'));
						        		$i = 0;

									 	echo '<div class="clearfix flexible-image-wrapper"><div class="small-12 large-10 large-offset-1">';

									 	// loop through the rows of data
									    while ( have_rows('image_content') ) : the_row();

									    	$i++;
											$caption = get_sub_field('caption') && $count > 1 ? '<div class="flexible-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
											$single_caption = $count == 1 ? '<div class="flexible-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : '<div class="flexible-image-caption-below-mobile"><p>'.get_sub_field('caption').'</p></div>';

											switch($count) {
												case 1 :
													$layout = 'one-up-';
													break;
												case 2 : 
													$layout = 'two-up-';
													break;
												case 3 :
													$layout = 'three-up-';
													break;
												case 4 :
													$layout = 'four-up-';
													break;
												default:
													$layout = 'one-up-';
											}

											echo '<div class="lightbox-image flexible-image '.$image_dir.' '.$layout.$i.' flexible-image-'.$i.'" data-responsive-background-image>
													<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

										endwhile;

										echo '</div></div>';

									endif;

									if( get_sub_field('video') ):

										$has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

										echo '<div><div class="small-12 large-10 large-offset-1"><div class="flexible-video-wrapper '.$has_caption.'">';
										the_sub_field('video');
										echo '</div></div></div>';

										if (get_sub_field('video_caption')) :
											echo '<div class="flexible-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
										endif;

									endif;

									// +++++++++++++++++++++++++++++++++++++++++++++++++

									if( get_sub_field('text') ):

						        		echo '<div class="row">
						        				<div class="clearfix"><div class="small-12 medium-12 large-8 large-offset-2 columns">';

					        			the_sub_field('text');

					        			echo '</div></div>';

						        	endif;

						        	// +++++++++++++++++++++++++++++++++++++++++++++++++

						        	if( get_sub_field('button_url') || get_sub_field('button_link') ):

						        		$button_url = get_sub_field('button_url') ? get_sub_field('button_url') : get_sub_field('button_link');
						        		$icon = get_sub_field('button_url') ? 'window-restore' : 'chevron-right';
						        		$target = get_sub_field('button_url') ? 'target="_blank"' : null;

						        		echo '<div class="text-center"><a '.$target.' class="flexible-button btn btn-medium btn-ghost" href="'.$button_url.'">'.get_sub_field('button_text').' <i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></div>';

						        	endif;

					        	echo '</div></div>';

					        endif;

					    	// +++++++++++++++++++ End: Text and image ++++++++++++++++++++++++++++++++++++

				endwhile;

			endif;?>

	</div>
</section>
<?php 
	endwhile;

endif; ?>

<?php get_footer()?>