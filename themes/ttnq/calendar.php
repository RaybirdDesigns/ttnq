<?php
/**
 * Template Name: Calendar
 */

// Get selected tags from acf field
// and return associative array of tag id => name
function calendar_filters() {
    $filters = get_field('tags_to_filter');
    $tags = get_tags();

    // create array of category id => name
    foreach($tags as $tag) {
        $tagname = $tag->name;
        $tagId = (string)$tag->term_id;
        $tagArr[$tagId] = $tagname;
    }

    // filter by the selected categories
    $filtered = array_filter(
        $tagArr,
        function ($key) use ($filters) {
            return in_array($key, $filters);
        },
        ARRAY_FILTER_USE_KEY
    );

    $filtered['all'] = 'All';

    return $filtered;
}


// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (get_post_thumbnail_id($post->ID)) {
    $thumb_nail_url = wp_get_attachment_image_url( get_post_thumbnail_id($post->ID), "hero-thumb" );
} else {
    $thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'hero-thumb')[0];
}

?>

<?php get_header(); ?>

<style>
    .search-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<script>
    //<![CDATA[ 
    var tag_filter_calendar = <?php echo json_encode(calendar_filters()); ?>
    //]]>
</script>

<div id="hero-banner" class="search-hero" data-responsive-background-image >

    <?php if (get_post_thumbnail_id($post->ID)) : ?>

        <img class="hero-img" <?php responsive_image_post('full'); ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

    <div class="hero-bottom-gradient"></div>
</div>

<?php if (have_posts()) : ?>
<section id="calendar-header" class="calendar-header">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns text-center">
			<h1><?php the_title(); ?></h1>
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
</section>
<?php endif; ?>

<div id="calendar-root"
	 data-month="<?php echo date('M'); ?>"
	 data-year="<?php echo date('Y'); ?>"
	 data-posts-per-page="<?php the_field('posts_per_page'); ?>"></div>

<?php get_footer(); ?>