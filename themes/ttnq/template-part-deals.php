<?php
  $args = array(
    'post_type' => 'deal',
    'numberposts' => -1,
    'tax_query' => [
                      [
                          'taxonomy' => 'deal_category',
                          'terms' => $deal_category_id,
                          'include_children' => false // Remove if you need posts from term 7 child terms
                      ],
                    ],
    'sort_custom' => true,
    'meta_query' => array(
                        array(
                            'key' => 'member_level',
                        ),
                        array(
                            'key' => 'provider',
                        )
                    ),
    'orderby'    => array(
        'meta_value' => 'ASC'
    )
);
$myposts = get_posts($args);
$ctr = 0;
echo "<div class='large-12'>";
foreach ($myposts as $post) : ?>
             <?php    
             
            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url($post->ID,'large'); 
            
            $post_title = $post->post_title;
            $provider = get_field('provider', $post->ID);
            $location = get_field('location', $post->ID);
            $slug = get_post_field( 'post_name', $post->ID );
            ?>
            <div class="large-3 columns">
                <a href="<?php echo esc_url( get_permalink($post->ID) ); ?>">
                    <div class="grid-image">
                    <img <?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?> src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                    </div>
                    <div class="grid-details">
                        <h4><a href="<?php echo esc_url( get_permalink($post->ID) ); ?>"><?php echo $post_title; ?></a></h4>
                        <h5><strong><?php echo $provider; ?></strong></h5>
                        <?php if($location): ?><span class="location-icon"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?></span><?php endif; ?>
                        <a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" id="<?php echo $slug; ?>" class="view-deal">View Deal</a>
                    </div>
                </a>
            </div>
            
    <?php  
             $ctr++; 
             endforeach; ?>

    <?php
echo "</div>";
wp_reset_postdata();
?>