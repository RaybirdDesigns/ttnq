<?php
/**
 * Template Name: Things to do
 */

global $post;

$page_slug = $post->post_name;

$category = get_the_category();

$first_category = $category[0]->slug;

$theme_colour = get_field('theme_colour');

// check if page has children
// use to show sticky nav
function has_children() {
    global $post;

    $children = get_pages( array( 'child_of' => $post->ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }
}

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-things/hero', 'things') ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<?php if (has_children()) : ?>
<?php get_template_part('components/sticky-nav/sticky-nav'); ?>
<?php endif; ?>

<section id="description-1">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php
				have_posts();
				if (have_posts()) :
				    while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
				    <?php
				    endwhile;
			    endif;
				wp_reset_query();
			?>
		</div>
	</div>

	<?php
	// multi image
	if( have_rows('image_content') ):

		$count = count(get_field('image_content'));

		$i = 0;

	 	echo '<div class="row things-image-wrapper"><div class="small-12 large-10 large-offset-1">';

	 	// loop through the rows of data
	    while ( have_rows('image_content') ) : the_row();

	    	$i++;
			$caption = get_sub_field('caption') && $count > 1 ? '<div class="things-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
			$single_caption = $count == 1 && get_sub_field('caption') ? '<div class="things-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : '<div class="things-image-caption-below-mobile"><p>'.get_sub_field('caption').'</p></div>';

			switch($count) {
				case 1 :
					$layout = 'one-up-';
					break;
				case 2 : 
					$layout = 'two-up-';
					break;
				case 3 :
					$layout = 'three-up-';
					break;
				case 4 :
					$layout = 'four-up-';
					break;
				default:
					$layout = 'one-up-';
			}

			echo '<div class="lightbox-image things-image '.$layout.$i.' things-image-'.$i.'" data-responsive-background-image>
					<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

		endwhile;

		echo '</div></div>';

	endif;
	?>
</section>

<?php if(get_field('video')) : ?>
<section id="video">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 medium-6 columns">
			<?php the_field('video_text'); ?>

			<?php if(get_field('video_btn_url')) : ?>
			<a target="_blank"  class="btn btn-medium btn-ghost btn-margin" href="<?php the_field('video_btn_url'); ?>"><?php the_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<?php

// get subpages
$id = get_the_ID();

if ( have_posts() && has_children() ) : the_post();
  $gp_args = array(
      'post_type' => 'page',
      'post_parent' => $id,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
  );
  $locations = get_posts($gp_args);

  $item_index = 0;
  $item_count = count($locations);

?>

<section id="in-page-nav" class="collapse-bottom">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center component">
			<?php the_field('explore_text'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="in-page-nav-list item-count-<?php echo $item_count; ?>">
			<?php foreach ($locations as $location) {

				$current_post_id = $location->ID;

				$image_id = get_post_thumbnail_id($current_post_id);
   
			   // set the default src image size
				$image_src = wp_get_attachment_image_url($image_id, 'large');

			   // set the srcset with various image sizes
				$image_srcset = wp_get_attachment_image_srcset($image_id, 'large');

				$title = $location->post_title;
				$item_index++;

	  			echo '<li class="in-page-nav-item in-page-nav-item-'.$item_index.'">
	  					<a class="in-page-nav-tile" href="'.get_permalink($location->ID).'">
	  					<img src="'.$image_src.'" srcset="'.$image_srcset.'" >
		  					<div class="tile-content">
								<h4>'.$title.'</h4>
							</div>
	  					</a>
	  				 </li>';
	  		} ?>
		</ul>
	</div>
</section>
<?php endif; ?>

<?php
// Show ad unit based on category
// randomised if more than one
get_template_part('components/ad-units/dyn-banner-wrapper');
?>


<?php
/*
	Set dynamic attractions tiles with custom override
*/

// set vars
// $tile_spaces = has_children() ? 4 : 16;
$row_name = 'attractions_row';
$subfield_name = 'attraction';
$selected_post_count = 0;

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {

		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// priority sort query
// priority_sort($posts_per_page, $paged, $post_type, $category, $tag)
$use_category = get_field('use_category') ? $first_category : null;
$priority_wp_query = priority_sort(-1, false, 'product', $use_category, $page_slug);
$priority_wp_query_posts = $priority_wp_query->posts;

// merge the two arrays together
if ($selected_post_count > 0) {
	$posts = array_unique(array_merge($clean_selected_posts_obj, $priority_wp_query_posts), SORT_REGULAR);
} else {
	$posts = $priority_wp_query_posts;
}

?>

<?php if (is_user_logged_in() && !$posts): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Attractions section doesn't have enough posts to be visible.</p>
</div>
<?php endif; ?>

<?php if ($posts && $posts[0]): ?>
<section id="explore-attractions" class="show-more-section">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('attractions_text'); ?>
		</div>
	</div>
	<div class="row show-more-container">
		<div class="small-12 show-more-container-inner">
			<?php include(locate_template('/components/tiles-4-up/tiles-4-up.php')); ?>
		</div>
	</div>

	<?php if (count($posts) > 8) : ?>
	<div class="row text-center">
		<button class="btn btn-medium btn-ghost btn-margin show-more-btn">See more</button>
	</div>
	<?php endif; ?>

</section>
<?php endif; ?>

<?php get_footer()?>