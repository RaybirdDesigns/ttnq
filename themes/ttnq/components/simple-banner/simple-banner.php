<?php 
$desktop_banner = get_sub_field('desktop_banner') ? get_sub_field('desktop_banner') : get_field('desktop_banner');
$mobile_banner = get_sub_field('mobile_banner') ? get_sub_field('mobile_banner') : get_field('mobile_banner');
?>

<?php if($desktop_banner && $mobile_banner): ?>
<div class="simple-banner">
	<img class="simple-banner__desktop" <?php echo responsive_image($desktop_banner, 'full'); ?>>
	<img class="simple-banner__mobile" <?php echo responsive_image($mobile_banner, 'full'); ?>>
</div>
<?php endif; ?>