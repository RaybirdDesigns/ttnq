<?php

$crisis_colour = get_field('crisis_banner_colour') ? get_field('crisis_banner_colour') : '#f30019';
$show_crisis_banner = get_field('show_crisis_banner'); 
$crisis_text = get_field('crisis_text');
?>

<?php if($show_crisis_banner && $crisis_text): ?>
<section id="crisis-notification" class="crisis-notification" style="background-color: <?php echo $crisis_colour; ?>">
	<div class="row">
		<div class="small-12 large-10 large-offset-1">
			<div class="crisis-notification__icon-container small-12 medium-1 columns">
				<?php if (get_field('crisis_type') == 'general') : ?>
				<svg class="crisis-notification__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 301.761 301.761" style="enable-background:new 0 0 301.761 301.761;" xml:space="preserve">
				<path d="M150.881,17.519L0,284.241h301.761L150.881,17.519z M168.072,104.046l-6.85,105.73h-20.678l-6.85-105.73H168.072z   M150.88,255.995c-9.755,0-17.663-7.908-17.663-17.663c0-9.755,7.908-17.663,17.663-17.663c9.755,0,17.663,7.908,17.663,17.663  C168.543,248.087,160.635,255.995,150.88,255.995z" fill="#FFFFFF"/>
				</svg>
				<?php elseif (get_field('crisis_type') == 'fire'): ?>
				<svg class="crisis-notification__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 304.983 304.983" style="enable-background:new 0 0 304.983 304.983;" xml:space="preserve">
				<g>
					<path d="M0.334,153.205l152.563,151.778l151.753-152.298L152.575,0L0.334,153.205z M174.976,155.65   c3.862-3.64,13.564-6.834,16.047-11.774c5.286,15.507,14.383,51.621-27.717,72.165c7.265-12.473,4.426-23.867,3.14-29.271   c-3.616,3.6-7.01,3.032-7.01,3.032c2.962-8.913-3.273-17.272-6.705-22.943c-3.515,12.393-21.452,19.418-21.452,32.606   c0,6.335,2.106,12.404,4.824,17.608c-14.208-6.643-34.929-21.139-32.959-48.755c2.428-34.042,54.599-33.637,43.375-77.559   C158.749,95.885,182.094,112.53,174.976,155.65z" fill="#FFFFFF"/>
				</g>
				</svg>
				<?php endif; ?>
			</div>
			<div class="crisis-notification__text-container small-12 medium-11 columns">
				<span role="alert"><?php echo $crisis_text; ?></span>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>