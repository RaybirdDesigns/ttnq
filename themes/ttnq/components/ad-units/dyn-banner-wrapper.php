<?php

$category = get_the_category();

$first_category = !empty($category) ? $category[0]->slug : null;

// Show ad unit based on category
// randomised if more than one
$args = array(
    'post_type'     => 'ad_unit',
    'category_name' => $first_category,
    'post_status'   => 'publish',
    'orderby' 		=> 'rand',
    'order'         => 'DESC',
    'posts_per_page' => '1',
    'no_found_rows' => true,
    'update_post_term_cache' => false,
    'posts_per_page' => 1,
    'paged'          => false,
);

$wp_query = new WP_Query($args);

$posts = $wp_query->posts;

if ( have_posts() && !empty($first_category) ) : ?>
<section id="banner-1" class="collapse">
	<div class="small-12">

		<?php foreach( $posts as $post ): 
		
		setup_postdata( $post );
		
		?>
		
		<?php get_template_part('/components/ad-units/banner-ad') ?>

		<?php endforeach; ?>
					
	</div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>