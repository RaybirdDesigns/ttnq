<?php

// Feature narrative tile
// @params: $feature_index, $alternate

if (!isset($alternate_tiles) || !$alternate_tiles) {

	$float = ($feature_index == 3 || $feature_index == 7) ? 'float-right' : null;

	$split_layout = '<div class="feature-narrative-tile clearfix">
						<div class="feature-narrative-tile-image small-12 medium-5 large-6 xlarge-7 columns '.$float.'">
							<img class="lazyload" data-object-fit="cover" '.responsive_image_return(get_sub_field('image'), 'large', true).' />
						</div>
						<div class="feature-narrative-tile-text small-12 medium-7 large-6 xlarge-5 columns">
							<div class="feature-narrative-tile-text-content">'
		.get_sub_field('text').
		'</div>
						</div>
					</div>';

	$center_layout = '<div class="feature-narrative-tile feature-narrative-tile-center clearfix">
					<div class="feature-narrative-tile-image small-12 columns">
						 <img class="lazyload" data-object-fit="cover" '.responsive_image_return(get_sub_field('image'), 'full', true).'/>
						<div class="feature-narrative-tile-text-content text-center">'
		.get_sub_field('text').
		'</div>
					</div>
				 </div>';

	switch($feature_index) {
		case 1 :
			echo $split_layout;
			break;
		case 2 :
			echo $center_layout;
			break;
		case 3 :
			echo $split_layout;
			break;
		case 4 :
			echo $center_layout;
			break;
		case 5 :
			echo $split_layout;
			break;
		case 6 :
			echo $center_layout;
			break;
		case 7 :
			echo $split_layout;
			break;
		case 8 :
			echo $center_layout;
			break;
		case 9 :
			echo $split_layout;
			break;
		case 10 :
			echo $center_layout;
			break;
		default:
			echo $center_layout;
	}

} else {

	if ($feature_index % 2 == 1) {
		$float = 'float-left';
	} else {
		$float = 'float-right';
	}

	$split_layout = '<div class="feature-narrative-tile clearfix">
							<div class="feature-narrative-tile-image small-12 medium-5 large-6 xlarge-7 columns '.$float.'">
								<img class="lazyload" data-object-fit="cover" '.responsive_image_return(get_sub_field('image'), 'large', true).' />
							</div>
							<div class="feature-narrative-tile-text small-12 medium-7 large-6 xlarge-5 columns">
								<div class="feature-narrative-tile-text-content">'
									.get_sub_field('text').
								'</div>
							</div>
						</div>';

	echo $split_layout;

}

?>

