<?php

// Custom post tile.
// Shows feature image and a snippet of the content.
// If it's an event it's show the date

// get date to split up
$date = strtotime(get_field('time_of_event'));
$date_end = strtotime(get_field('end_time_of_event'));

$image_0 = get_field('product_image_0');

// get date type
$date_type = get_field('date_type');

// show the start date month if different from end date month
$start_month = date('M', $date) !== date('M', $date_end) ? date('M', $date) : null;

if (get_the_tags( $id )) {
	$tag_name = get_the_tags( $id )[0]->name == 'blank' ? get_the_tags( $id )[1]->name : get_the_tags( $id )[0]->name;
} else {
	$tag_name = null;
}

?>

<a href="<?php the_permalink(); ?>"
   class="custom-post-tile">
    <?php if ($image_0) : ?>
    	<img class="lazyload" data-object-fit="cover" <?php adtw_image($size = 600, 0, true); ?>>
    <?php elseif(has_post_thumbnail( $post->ID )) : ?>
    	<img class="lazyload" data-object-fit="cover" <?php responsive_image_post('large', true); ?> >
    <?php else : ?>
       <img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full', true); ?> />
    <?php endif; ?>

   <?php

   $post_type_name = get_post_type();

   // If post type is event use the acf field event_category
   // otherwise use the first tag
   if ($post_type_name == 'event') {
		if (get_field('event_category')) {
			echo '<div class="custom-post-tile-tag">' . get_field('event_category') . ' ' . '</div>';
		}
	} else {
		if ($tag_name) {
			echo '<div class="custom-post-tile-tag">' . $tag_name . ' ' . '</div>';
		}
	}

	if (is_user_logged_in()) {
		$priority_num = get_field('priority') > -1 ? get_field('priority') : 'Not set';
		echo '<div class="priority priority-'.get_field('priority').'">'.$priority_num.'</div>';
	}

	?>

	<div class="custom-post-tile-text">
		<?php if (get_field('time_of_event')) : ?>
			<?php if ($date_type == 'single-date') : ?>
				<span class="custom-post-tile-text-date"><?php the_field('time_of_event'); ?></span>
				<?php elseif ($date_type == 'date-range') : ?>
				<span class="custom-post-tile-text-date">
					<?php echo date('j', $date).' '.$start_month;?> - <?php echo date('j', $date_end).' '.date('M', $date_end).' '.date('Y', $date_end); ?>
				</span>
				<?php else : ?>
				<span class="custom-post-tile-text-date"><?php echo date('M', $date).' '.date('Y', $date);?></span>
			<?php endif; ?>
		<?php endif; ?>
		<h5><?php the_title(); ?></h5>
		<?php if($post_type_name == 'itinerary'): ?>
		<p class="itinerary-location"><?php the_field('location'); ?></p>
		<?php elseif(has_excerpt()) : ?>
		<?php the_excerpt(); ?>
		<?php endif; ?>
		<div class="custom-post-tile-text-view">
			<?php if($post_type_name == 'itinerary' && has_excerpt()): ?>
			<?php the_excerpt(); ?>
			<?php endif; ?>
			View <?php echo $post_type_name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i>
		</div>
		<div class="overlay"></div>
	</div>
</a>