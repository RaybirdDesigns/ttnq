<?php

// Alternating Rows
// @params: $feature_index
?>

<div class="alternating-rows clearfix">
	<div class="alternating-rows-image small-12 medium-6 large-6 xlarge-7 columns">
		<img data-object-fit="cover" <?php echo responsive_image_return(get_sub_field('image'), 'large') ?> />
	</div>
	<div class="alternating-rows-text small-12 medium-6 large-6 xlarge-5 columns">
		<div class="alternating-rows-text-content">
			<?php the_sub_field('text'); ?>
		</div>
	</div>
</div>