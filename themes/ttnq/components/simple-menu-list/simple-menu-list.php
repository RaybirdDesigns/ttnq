<?php

// Simple menu list.
// Used in:
// - under hero on homepage

?>

<div class="simple-menu-list">
	<ul class="list-reset simple-menu-list-list">
		<?php
			if ( have_rows('menu_list') ) {

				while ( have_rows('menu_list') ) { 

					the_row();

					$post_object = get_sub_field('list_item');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object );
						
						echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';

						wp_reset_postdata();
					}
				}
			}
		?>
	</ul>
</div>