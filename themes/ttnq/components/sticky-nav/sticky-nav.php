<?php

// Sticky nav

$theme_colour = get_field('theme_colour');

$id = get_the_ID();

if ( have_posts() ) : the_post();
  $gp_args = array(
      'post_type' => 'page',
      'post_parent' => $id,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
  );
  $locations = get_posts($gp_args);

  // gallery background colour
  switch($theme_colour) {
    case '639083' :
      $sticky_bg_colour = '274f2c';
      break;
    case 'CE975C' : 
      $sticky_bg_colour = '2f2920';
      break;
    case '6fa0ae' :
      $sticky_bg_colour = '1f5668';
      break;
    default:
      $sticky_bg_colour = '2f2920';
  }

?>

<style>
  
  #sticky-nav .sticky-nav-list li a {
    color: #<?php echo $sticky_bg_colour; ?>;
  }

  #sticky-nav .sticky-nav-list li.overview a {
    color: #<?php echo $theme_colour; ?>;
    border-bottom: 3px solid #<?php echo $theme_colour; ?>;
  }

  #sticky-nav .sticky-nav-list li a:hover,
  #sticky-nav .sticky-nav-list li a:focus {
    color: #<?php echo $theme_colour; ?>;
    border-bottom: 3px solid #<?php echo $theme_colour; ?>;
  }

</style>

<div id="sticky-nav">
	<ul class="sticky-nav-list">
		<li class="overview"><a href="#">overview <i class="fa fa-chevron-down" aria-hidden="true"></i></a></li>
    <div class="sticky-wrapper">
      <?php foreach ($locations as $location) {
        echo '<li><a href="'.get_permalink($location->ID).'">'.$location->post_title.'</a></li>';
      } ?>
    </div>
	</ul>
</div>

<?php endif; ?>