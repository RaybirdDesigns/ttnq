<?php

// Event tile.
// Shows feature image and a snippet of the content.

// remove images and truncate post
// $cleanPost = preg_replace("/<img[^>]+\>/i", "", get_the_content()); 
// $truncatedPost = (strlen($cleanPost) > 180) ? substr($cleanPost, 0, 180) . '...' : $cleanPost;

?>

<a href="<?php the_permalink(); ?>"
   class="event-tile"
   data-responsive-background-image >

   <img <?php responsive_image_post('large') ?> >

   <?php

	if (get_field('event_category')) {
		echo '<div class="event-tile-tag">' . get_field('event_category') . ' ' . '</div>'; 
	}

	?>

	<div class="event-tile-text">
		<span class="event-tile-text-date"><?php the_field('time_of_event'); ?></span>
		<h5><?php the_title(); ?></h5>
		<?php if($i == 1) : ?>
		<p><?php the_excerpt(); ?></p>
		<?php endif; ?>
		<div class="event-tile-text-view">View event <i class="fa fa-angle-right" aria-hidden="true"></i></div>
	</div>
</a>