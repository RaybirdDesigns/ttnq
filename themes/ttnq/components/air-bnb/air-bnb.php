<?php

$category = get_the_category();

$location = $category[0]->slug;

// Airbnb API endpoint
$url = 'https://api.airbnb.com/v2/search_results?client_id=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en-AU&currency=AUD&_format=for_search_results_with_minimal_pricing&_limit=4&_offset=1&fetch_facets=false&guests=1&ib=false&ib_add_photo_flow=false&location='.$location.'%2C%20QLD%2C%20AU';

//  Initiate curl
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);

// Will dump a beauty json :3
// var_dump(json_decode($result, true));
$data = json_decode($result, true);

// print_r($data['search_results']);

?>

<?php if(!empty($data['search_results'])): ?>
<section id="airbnb">
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_field('airbnb_text'); ?>
		</div>
	</div>
	<div class="row">

		<ul class="list-reset airbnb-list">
		
		<?php foreach($data['search_results'] as $item): ?>

			<li class="small-6 medium-6 large-3 columns">
				<a target="_blank"
				   href="https://www.airbnb.com/rooms/<?php echo $item['listing']['id']; ?>"
				   class="custom-post-tile"
				   style="background-image: url(<?php echo $item['listing']['picture_url']; ?>);">
				   <div class="custom-post-tile-tag"><?php echo $item['listing']['property_type']; ?></div>
					<div class="custom-post-tile-text">
					<h5><?php echo $item['listing']['name']; ?></h5>
					<div class="custom-post-tile-text-view">View on Airbnb <i class="fa fa-window-restore" aria-hidden="true"></i></div>
					</div>
					<div class="overlay"></div>	
				</a>
			</li>

		<?php endforeach; ?>

		</ul>

	</div>

	</div> 
</section>
<?php endif; ?>