<?php

// Used in:
// Hero experience

?>

<div class="hero-exp-three-image-text">
	
	<div class="clearfix">
		<div class="section-1-image small-12 large-7 columns"
			 data-responsive-background-image>
			<img <?php responsive_image(get_field('narrative_image_1'), 'large') ?> >
		</div>
		<div class="section-1-text small-12 large-5 columns">
			<div class="section-1-text-content">
				<?php the_field('narrative_text_1'); ?>
			</div>
		</div>
	</div>

	<div class="clearfix">
		<div class="section-2 small-12 columns"
			 data-responsive-background-image>
			 <img <?php responsive_image(get_field('narrative_image_2'), 'full') ?> >
			<div class="section-2-text-content text-center">
				<?php the_field('narrative_text_2'); ?>
			</div>
		</div>
	</div>

	<div class="clearfix">
		<div class="section-3-text small-12 large-5 columns">
			<div class="section-3-text-content">
				<?php the_field('narrative_text_3'); ?>
			</div>
		</div>
		<div class="section-3-image small-12 large-7 columns"
			 data-responsive-background-image>
			<img <?php responsive_image(get_field('narrative_image_3'), 'large') ?> >
		</div>
	</div>

</div>