<?php
/*
	Set dynamic itineraries tiles with custom override
*/

// set vars
$tile_spaces = -1;
$selected_post_count = 0;
$row_name = 'featured_itineraries';
$subfield_name = 'featured_itinerary';
$itineraryType = get_field('itinerary_type') ? get_field('itinerary_type') : get_sub_field('itinerary_type');

$itineraries_heading = get_field('itineraries_heading') ? get_field('itineraries_heading') : get_sub_field('itineraries_heading');
$itineraries_sub_heading = get_field('itineraries_sub_heading') ? get_field('itineraries_sub_heading') : get_sub_field('itineraries_sub_heading');

$call_to_action_link = get_field('call_to_action_link') ? get_field('call_to_action_link') : get_sub_field('call_to_action_link');
$call_to_action_text = get_field('call_to_action_text') ? get_field('call_to_action_text') : get_sub_field('call_to_action_text');

// count number of selected posts
if ( have_rows($row_name) ) {

	// $selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name) ? get_field($row_name) : get_sub_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {

		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// merge the two arrays together if there are selected tiles
// otherwise just return dynamic posts
if ($selected_post_count > 0) {

	if ($itineraryType == 'all') {
		$priority_wp_query = priority_sort(-1, false, 'itinerary', $first_category, null);
	} else {
	// $priority_wp_query = priority_sort_itinerary(-1, false, $first_category, 'all');
		$priority_wp_query = priority_sort_itinerary(-1, false, $first_category, $itineraryType);
	}
	
	$priority_wp_query_posts = $priority_wp_query->posts;
	
	$posts = array_unique(array_merge($clean_selected_posts_obj, $priority_wp_query_posts), SORT_REGULAR);

} else {

	if ($itineraryType == 'all') {
		$priority_wp_query = priority_sort(-1, false, 'itinerary', $first_category, null);
	} else {
	// $priority_wp_query = priority_sort_itinerary(-1, false, $first_category, 'all');
		$priority_wp_query = priority_sort_itinerary(-1, false, $first_category, $itineraryType);
	}

	$priority_wp_query_posts = $priority_wp_query->posts;
	$posts = $priority_wp_query_posts;

}
?>

<?php if (is_user_logged_in() && empty($posts)): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Itinerary section doesn't have enough posts to be visible. A minimum of 1 post is needed.</p>
</div>
<?php endif; ?>

<?php if ($posts): ?>
<section id="itinerary-carousel">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<h2><?php echo $itineraries_heading; ?></h2>
			<?php if($itineraries_sub_heading): ?>
				<p><?php echo $itineraries_sub_heading; ?></p>
			<?php endif; ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php

			$tile_index = 0;

			echo '<ul class="itineraries-carousel list-reset clearfix">';
			foreach( $posts as $post ):
				setup_postdata( $post );
					$tile_index++;
					echo '<li class="itineraries-carousel-item">';
							if (is_user_logged_in() && $tile_index <= $selected_post_count) {
								echo '<div class="authored">Authored</div>';
							}
							get_template_part('components/custom-post-tile/custom-post-tile');
					echo '</li>';
			endforeach;
			wp_reset_postdata();
			echo '</ul>';

			?>
		</div>
	</div>
	<?php if($call_to_action_link): ?>
		<div class="row text-center">
			<a class="btn btn-medium btn-ghost green" href="<?php echo $call_to_action_link; ?>"><?php echo $call_to_action_text; ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
	<?php else : ?>
		<?php
		$events_URL = add_query_arg(
			array(
				'post-type' => 'itinerary',
				), get_site_url() . '/post-list/'
			);
		?>
		<div class="row text-center">
			<a class="btn btn-medium btn-ghost green" href="<?php echo $events_URL; ?>">Browse all itineraries <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
	<?php endif; ?>
</section>
<?php endif; ?>