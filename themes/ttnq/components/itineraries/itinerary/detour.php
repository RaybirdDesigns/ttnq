<?php

$detour_image = get_sub_field('image');
$detour_heading = get_sub_field('heading');
$detour_text = get_sub_field('text');
$detour_link = get_sub_field('link_url');

?>

<div class="itinerary-detour">
	<div class="row">
		<div class="itinerary-detour-image" style="background-image: url(<?php echo wp_get_attachment_image_url($detour_image, 'large'); ?>);">
		</div>
		<div class="itinerary-detour-text">
			<div>
				<h3>Optional Detour – <?php echo $detour_heading; ?></h3>
				<?php echo $detour_text; ?>
			 	<?php if($detour_link): ?>
			 		<a href="<?php echo $detour_link ?>">Learn More</a>
			 	<?php endif; ?>
			</div>
		</div>
		<div class="itinerary-line">
        	<svg id="dashed-line">
				<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
			</svg>
        </div>
	</div>
</div>