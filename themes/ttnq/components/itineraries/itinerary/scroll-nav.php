<?php if( have_rows('day') ): ?>
<div id="itinerary-scroll-nav" class="itinerary-scroll-nav">
	<div class="row columns">

		<p class="itinerary-scroll-nav-key">Day</p>

		<ul class="list-reset list-inline itinerary-scroll-nav-list">
		<?php $nav_index = 0; ?>
		<?php while ( have_rows('day') ) : the_row(); ?>
			<?php if( get_row_layout() == 'day_section' ): ?>
			<?php $nav_index++; ?>
			<li>
				<a class="itinerary-scroll-nav-link" href="#day-<?php echo $nav_index; ?>" data-nav-scroll-to="day-<?php echo $nav_index; ?>"><span><?php echo $nav_index; ?></span></a>
			</li>
			<?php endif; ?>
		<?php endwhile; ?>
		</ul>
	</div>
</div>
<?php endif; ?>