<?php

$title_override = get_sub_field('tile_title_text');
$tile_title = $title_override ? $title_override : get_the_title();
$image_url = get_post_thumbnail_id( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large" )[0] : wp_get_attachment_image_src(get_field('hero_banner_image'), 'large')[0];

?>

<a href="<?php the_permalink(); ?>"
   class="image-title-tile"
   style="background-image: url('<?php echo $image_url; ?>');" >

	<div class="tile-content">
		<h4><?php echo $tile_title; ?></h4>
	</div>
</a>