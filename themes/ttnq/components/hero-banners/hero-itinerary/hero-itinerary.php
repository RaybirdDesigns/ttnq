<?php

// Standard hero banner

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
	$thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

?>

<style>
	.hero-standard:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}
</style>

<div id="hero-banner" class="hero-standard" data-responsive-background-image>

	<?php if (has_post_thumbnail( $post->ID )) : ?>

        <img class="hero-img" <?php responsive_image_post('full') ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

	<div class="row">
		<div class="hero-standard-content">
			<div class="hero-standard-content-text">
				<p>Plan Your Trip</p>
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>

	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<div class="hero-bottom-gradient"></div>
</div>