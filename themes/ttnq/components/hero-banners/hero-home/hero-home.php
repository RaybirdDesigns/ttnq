<?php
/* ============================================================ */
/* Hero used on homepage template and event category templates */
/* ============================================================ */

// Banner image
$global_fallback_id = get_field('global_fallback_image', 'option');
$desktop_image_id = get_field('hero_banner_image') ? get_field('hero_banner_image') : $global_fallback_id;
$mobile_image_id = get_field('hero_banner_mobile') ? get_field('hero_banner_mobile') : $desktop_image_id;

$desktop_image_url = wp_get_attachment_image_url( $desktop_image_id, 'full' );
$mobile_image_url = wp_get_attachment_image_url( $mobile_image_id, 'mobile-banner' );

// Hero video component
$video_url = get_field('video_url') ? get_field('video_url') : get_field('video_file');

$cta_text = get_field('cta_text');

$crisis_banner_active = get_field('show_crisis_banner') && get_field('crisis_text') ? true : false; 

// This banner uses the responsive background class
$has_bg_image = get_field('use_video_bg') ? '' : 'data-responsive-background-image';

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
$thumb_nail_url = $desktop_image_id ? wp_get_attachment_image_url($desktop_image_id, 'hero-thumb' ) : wp_get_attachment_image_url($global_fallback_id, 'hero-thumb' );

$page_template = get_page_template_slug();

?>

<?php if (!get_field('use_video_bg')) : ?>
<style>
	
	.hero-home:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

	.hero-img {
		display: none;
	}

	.hero-home {
		background-image: url(<?php echo $desktop_image_url; ?>);
		<?php if ($crisis_banner_active) : ?>
			height: 82.5vh;
		<?php endif; ?>
	}

	@media (max-width: 640px) {
		.hero-home {
			background-image: url(<?php echo $mobile_image_url; ?>);
			height: auto;
		}

		.hero-home:before {
			content: none;
		}
	}

	#hero-banner .btn {
		border: 0!important;
	}

	#hero-banner .btn:hover,
	#hero-banner .btn:focus {
		background-color: transparent!important;
	}

	<?php if($page_template == 'events-category-page.php'): ?>
		
	.scroll-down-btn {
		bottom: 100px;
	}

	@media (max-width: 640px) {
		.scroll-down-btn {
			bottom: 70px;
		}
	}

	<?php endif; ?>

</style>

<?php endif; ?>

<div id="hero-banner" class="hero-home">

	<?php if(!get_field('use_video_bg')) : ?>
	<img class="hero-img" src="<?php echo wp_get_attachment_image_url( get_field('hero_banner_image'), 'full' ) ?>" >
	<?php endif; ?>

	<?php if(get_field('use_video_bg')) : ?>
	<div class="video-bg" data-novideo="<?php the_field('no_video_image_url'); ?>" data-video="<?php echo $video_url; ?>" data-poster="<?php the_field('poster_image'); ?>"></div>
	<?php endif; ?>

	<div class="row">
		<div class="hero-home-content columns">
			<?php

				$post_object = get_field('post_text');

				if ( $post_object ) {

					$post = $post_object;
					setup_postdata( $post );

					// This banner is used on the Home page and the Events category pages.
					// The context heading above the main heading for each page is different.
					$allposttags = get_the_tags();

					$first_tag = $allposttags ? $allposttags[0]->name : null;

					$first_cat = get_the_category( $id )[0]->name;

					$context_heading = $page_template == 'home.php' ? $first_cat : $first_tag.' events in '.$first_cat;

				?>
				
				<span class="hero-home-content-cat"><?php echo $context_heading; ?></span>
				<h1><?php the_title(); ?></h1>
				<?php if (has_excerpt()): ?>
				<p class="hero-home-content-excerpt"><?php echo get_the_excerpt(); ?></p>
				<?php endif; ?>
				<a class="hero-home-content-cta" href="<?php the_permalink(); ?>">
					<?php echo $cta_text; ?><i class="fa fa-chevron-right" aria-hidden="true"></i>
				</a>
							
			<?php	wp_reset_postdata();
				}
			?>
		</div>
		<?php if (get_field('scroll_to_id')) : ?>
		<button class="btn scroll-down-btn scroll-down-btn-js" data-scroll-to="<?php the_field('scroll_to_id'); ?>">
			<svg class="down-arrow" role="presentation">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#arrow-ico-white"></use>
            </svg>
		</button>
		<?php endif; ?>
	</div>

	<?php if ($page_template == 'events-category-page.php'): ?>
	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<?php endif; ?>

	<div class="hero-bottom-gradient"></div>
</div>