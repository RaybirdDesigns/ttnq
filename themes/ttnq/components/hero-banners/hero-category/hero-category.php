<?php

// Standard hero banner
$video_url = get_field('video_url') ? get_field('video_url') : get_field('video_file');
$cta_text = get_field('cta_text');

// This banner uses the responsive background class
$has_bg_image = get_field('use_video_bg') ? '' : 'data-responsive-background-image';

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
$thumb_nail_url = get_field('hero_banner_image') ?
    wp_get_attachment_image_url(get_field('hero_banner_image'), 'medium' ) :
    get_the_post_thumbnail_url();

?>

<?php if (!get_field('use_video_bg')) : ?>
<style>
	
	.hero-category:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

</style>
<?php endif; ?>

<div data-localize="notranslate" id="hero-banner" class="hero-category" <?php echo $has_bg_image; ?>>

	<?php if(!get_field('use_video_bg')) : ?>
        <?php if(get_field('hero_banner_image')) { ?>
	        <img class="hero-img" <?php responsive_image(get_field('hero_banner_image'), 'full') ?> >
        <?php } else { ?>
            <img class="hero-img" src="<?=get_the_post_thumbnail_url()?>" >
        <?php } ?>
	<?php endif; ?>

	<?php if(get_field('use_video_bg')) : ?>
	<div class="video-bg" data-novideo="<?php the_field('no_video_image_url'); ?>" data-video="<?php echo $video_url; ?>" data-poster="<?php the_field('poster_image'); ?>"></div>
	<?php endif; ?>

	<div class="row">
		<div class="hero-category-content columns">
			<div class="hero-category-content-text">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		<?php if (get_field('scroll_to_id')) : ?>
		<button class="scroll-down-btn scroll-down-btn-js" data-scroll-to="<?php the_field('scroll_to_id'); ?>">
			<svg class="down-arrow" role="presentation">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#arrow-ico-white"></use>
            </svg>
		</button>
		<?php endif; ?>
	</div>
	<div class="hero-bottom-gradient"></div>
</div>