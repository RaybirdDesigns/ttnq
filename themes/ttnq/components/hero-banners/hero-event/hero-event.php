<?php

// Event hero banner
// This banner uses the responsive background class

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
	$thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

?>

<style>
    .hero-event:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<div id="hero-banner" class="hero-event"
		data-responsive-background-image >

	<?php if (has_post_thumbnail( $post->ID )) : ?>

        <img class="hero-img" <?php responsive_image_post('full') ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

	<div class="row">
		<div class="hero-event-content columns">
			<div class="hero-event-content-text">
				<p><?php the_field('event_category'); ?></p>
				<h1 itemprop="name"><?php the_title(); ?></h1>
				<?php 
				// get raw date string to use in data schema
				$raw_start_date = get_field('time_of_event', false, false);
				$raw_end_date = get_field('end_time_of_event', false, false);

				// get date to split up
				$date = strtotime(get_field('time_of_event'));
				$date_end = strtotime(get_field('end_time_of_event'));

				// get date type
				$date_type = get_field('date_type');

				// show the start date month if different from end date month
				$start_month = date('F', $date) !== date('F', $date_end) ? date('F', $date) : null;

				?>
				
				<?php if ($date_type == 'single-date') : ?>
				<p itemprop="startDate" content="<?php echo date('c', strtotime($raw_start_date)); ?>"><?php the_field('time_of_event'); ?></p>
				<?php elseif ($date_type == 'date-range') : ?>
				<p>
					<span itemprop="startDate" content="<?php echo date('c', strtotime($raw_start_date)); ?>"><?php echo date('j', $date).' '.$start_month;?></span> - 
					<span itemprop="endDate" content="<?php echo date('c', strtotime($raw_end_date)); ?>"><?php echo date('j', $date_end).' '.date('F', $date_end).' '.date('Y', $date_end); ?></span>
				</p>
				<?php else : ?>
				<p itemprop="startDate" content="<?php echo date('Y', $date).'-'.date('m', $date).'-00'; ?>"><?php echo date('F', $date).' '.date('Y', $date);?></p>
				<?php endif; ?>

				<?php
				$date_type = get_field('date_type');
				$today = date('Ymd');
				switch ($date_type) {
				    case 'date-range':
				        $expiredate = $raw_end_date ? $raw_end_date : null;
				        break;
				    default:
				    	$expiredate = $raw_start_date ? $raw_start_date : null;
				}
				if ($expiredate && $expiredate < $today): ?>
				<div class="expired-event-messaging">
					<p>This event has ended. <a href="<?php echo site_url().'/plan-your-trip/events-calendar/' ?>">See our latest Events</a></p>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if (get_field('scroll_to_id')) : ?>
		<button class="scroll-down-btn scroll-down-btn-js" data-scroll-to="<?php the_field('scroll_to_id'); ?>">
			<svg class="down-arrow" role="presentation">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#arrow-ico-white"></use>
            </svg>
		</button>
		<?php endif; ?>
	</div>

	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<div class="hero-bottom-gradient"></div>
</div>