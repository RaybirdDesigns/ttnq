<?php

// Standard hero banner
$video_url = get_field('video_url') ? get_field('video_url') : get_field('video_file');

// This banner uses the responsive background class
$has_bg_image = get_field('use_video_bg') ? '' : 'data-responsive-background-image';

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
$thumb_nail_url = wp_get_attachment_image_url(get_field('hero_banner_image'), 'medium' );

$category = get_the_category();
$first_category = $category[0]->slug;

?>

<?php if (!get_field('use_video_bg')) : ?>
<style>

	.hero-things {
		width: 100%;
		height: 100vh;
	}
	
	.hero-things:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

</style>
<?php endif; ?>

<div id="hero-banner" class="hero-things" <?php echo $has_bg_image; ?>>

	<?php if(!get_field('use_video_bg')) : ?>
	<img class="hero-img" <?php responsive_image(get_field('hero_banner_image'), 'full') ?> >
	<?php endif; ?>

	<?php if(get_field('use_video_bg')) : ?>
	<div class="video-bg" data-novideo="<?php the_field('no_video_image_url'); ?>" data-video="<?php echo $video_url; ?>" data-poster="<?php the_field('poster_image'); ?>"></div>
	<?php endif; ?>

	<div class="row">
		<div class="hero-things-content columns">
			<?php if(have_rows('hero_icon')) : ?>
				<ul class="hero-things-icons-list list-inline">
				<?php while(have_rows('hero_icon')) : the_row(); ?>
					<li>
						<svg class="hero-things-icon" role="presentation">
							<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#<?php the_sub_field('icon'); ?>"></use>
			            </svg>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
			<div class="hero-things-content-text">
				<?php if(is_page_template( 'destination-things-to-do.php' )): ?>
				<p><?php echo $first_category; ?></p>
				<?php else: ?>
				<p>Things to do</p>
				<?php endif; ?>
				<h1><?php the_title(); ?></h1>
				<?php the_field('hero_description') ?>
			</div>
		</div>
		<?php if (get_field('scroll_to_id')) : ?>
		<button class="scroll-down-btn scroll-down-btn-js" data-scroll-to="<?php the_field('scroll_to_id'); ?>">
			<svg class="down-arrow" role="presentation">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/global-sprite.svg#arrow-ico-white"></use>
            </svg>
		</button>
		<?php endif; ?>
	</div>
	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<div class="hero-bottom-gradient"></div>
</div>