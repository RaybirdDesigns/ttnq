<?php

// Product hero banner

$allposttags = get_the_tags();
// if first tag is 'blank' use second tag.
// The use of blank as a tag is a work around so
// we can use tags in the breadcrumb but ommit them if
// we don't want to use them.
$primary_tag = get_field('primary_tag');
$tag_in_hero = $primary_tag ? $primary_tag->name : ($allposttags[0]->name !== 'blank' ? $allposttags[0]->name : $allposttags[1]->name);


// process rating value to drive star rating
$star_rating = get_field('product_star_rating');
$rating_perc = (int)$star_rating*20;
$product_email = get_field('product_email');
$product_primary_phone = get_field('product_primary_phone') ? get_field('product_primary_phone') : get_field('product_secondary_phone');
$product_enquiries_url = get_field('product_enquiries_url') ? get_field('product_enquiries_url') : get_field('product_booking_url');
$product_address_1 = get_field('product_address_1');
$product_address_2 = get_field('product_address_2');
$product_city_name = get_field('product_city_name');
$product_post_code = get_field('product_post_code');
$product_country_name = get_field('product_country_name');
$product_facebook = get_field('product_facebook');
$product_state_name = get_field('product_state_name');
$product_instagram = get_field('product_instagram');
$product_youtube = get_field('product_youtube');
$product_twitter = get_field('product_twitter');

$image_0 = get_field('product_image_0');
$image_1 = get_field('product_image_1');
$image_2 = get_field('product_image_2');
$image_3 = get_field('product_image_3');
$image_4 = get_field('product_image_4');
$image_5 = get_field('product_image_5');
$image_6 = get_field('product_image_6');
$image_7 = get_field('product_image_7');
$image_8 = get_field('product_image_8');
$image_9 = get_field('product_image_9');
$image_arr = array_filter(array($image_0, $image_1, $image_2, $image_3, $image_4, $image_5, $image_6, $image_7, $image_8, $image_9));

?>

<style>
	.details-rating .star-wrapper span {
	    background-image: url(<?php echo get_template_directory_uri() ?>/images/icons/ico-stars.svg),none;
	}
</style>

<div id="hero-banner" class="hero-product">
	<?php if (has_post_thumbnail( $post->ID )) : ?>
        <img data-object-fit="cover" <?php responsive_image_post('full') ?> >
    <?php elseif(!empty($image_arr)) : ?>
    	<?php
    	$first_image_key = array_key_first($image_arr);
    	?>
    	<img data-object-fit="cover" <?php adtw_image($size = 1800, $first_image_key, $is_lazy = false); ?>>
    <?php else : ?>
       <img data-object-fit="cover" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />
    <?php endif; ?>

	<div class="row">
		<div class="hero-product-content columns">
			<div class="hero-product-content-text">
				<?php if ($allposttags) : ?>
				<span class="hero-product-type"><?php echo $tag_in_hero; ?></span>
				<?php endif; ?>
				<h1 itemprop="name"><?php the_title(); ?></h1>
				<?php if ($product_email || $product_primary_phone || get_field('product_enquiries_url') || $star_rating): ?>
				<ul class="hero-product-details hero-product-details-row-1 list-reset list-inline">
					<?php if($product_email) : ?>
					<li>
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<p class="details-title">Email</p>
						<p class="details-detail"><a class="details-detail-email-link" href="mailto:<?php echo $product_email; ?>"><?php echo $product_email; ?></a></p>
					</li>
					<?php endif; ?>
					<?php if($product_primary_phone) : ?>
					<li>
						<i class="fa fa-phone" aria-hidden="true"></i>
						<p class="details-title">Phone</p>
						<p class="details-detail"><a href="tel:<?php echo $product_primary_phone; ?>"><?php echo $product_primary_phone; ?></a></p>
					</li>
					<?php endif; ?>
					<?php if(get_field('product_enquiries_url')) : ?>
					<?php 
						$website_url = get_field('product_enquiries_url');
						$website_url = preg_replace('#^https?://#', '', $website_url);
						$website_url_no_params = explode('/?',$website_url);
					?>
					<li>
						<i class="fa fa-mouse-pointer" aria-hidden="true"></i>
						<p class="details-title">Website</p>
						<p class="details-detail"><a class="details-detail-website-link" target="_blank" href="<?php echo addhttp($website_url); ?>"><?php echo $website_url_no_params[0]; ?></a></p>
					</li>
					<?php endif; ?>
					<?php if($star_rating) : ?>
					<li>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<p class="details-title">Hotel Class</p>
						<div class="details-rating">
						    <strong class="show-for-sr">Rating <?php echo $star_rating; ?></strong>
						    <span class="star-wrapper"><span style="width:<?php echo $rating_perc; ?>%;"></span></span>
						</div>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
				<?php if($product_address_1 || $product_address_2 || $product_city_name || $product_post_code) : ?>
				<ul class="hero-product-details hero-product-details-row-2 list-reset list-inline">
					<li>
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<p class="details-title">Address</p>
						<p class="details-detail">
						<?php echo $product_address_1; ?>
						<?php echo $product_address_2; ?><br/>
						<?php echo $post_code = $product_city_name ? $product_city_name . "," : null; ?>
						<?php echo $product_state_name; ?>
						<?php echo $post_code = $product_post_code ? $product_post_code . "," : null; ?>
						<?php echo $product_country_name; ?>
						</p>
					</li>
					<?php endif; ?>
					<?php if(get_field('trip_advisor_id')) : ?>
					<li>
						<i class="fa fa-tripadvisor" aria-hidden="true"></i>
						<p class="details-title">TripAdvisor Traveller Rating</p>
						<div id="trip-advisor-hero-rating"></div>
					</li>
					<?php endif; ?>
					<?php if($product_facebook || $product_instagram || $product_youtube || $product_twitter) : ?>
					<li>
						<i class="fa fa-share" aria-hidden="true"></i>
						<p class="details-title">Social</p>
						<div class="details-social">
							<?php if($product_facebook) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp($product_facebook); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if($product_twitter) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp($product_twitter); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if($product_instagram) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp($product_instagram); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if($product_youtube) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp($product_youtube); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
							<?php endif; ?>
						</div>
					</li>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<div class="hero-bottom-gradient"></div>
</div>