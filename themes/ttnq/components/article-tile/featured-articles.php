<?php if ( have_rows('featured_articles') ) : ?>

<section id="articles-1">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php if (get_field('articles_text')): ?>
				<?php the_field('articles_text'); ?>
			<?php else :?>
			<h2><?php the_field('articles_heading'); ?></h2>
			<?php if(get_field('articles_sub_heading')): ?>
				<p><?php the_field('articles_sub_heading'); ?></p>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="article-carousel">
		<?php

			while ( have_rows('featured_articles') ) {

				the_row();

				$post_object = get_sub_field('featured_article');

				if ( $post_object ) {

					$post = $post_object;
					setup_postdata( $post_object ); ?>
					
						<?php get_template_part('components/article-tile/article', 'tile') ?>

	<?php			wp_reset_postdata();
				}
			}
		?>
		</div>
	</div>

	<?php 
	$post_URL = add_query_arg(
		array(
			'post-type' => 'post',
			), get_site_url() . '/post-list/' 
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost blue" href="<?php echo $post_URL; ?>">Browse all posts <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>

<?php endif; ?>