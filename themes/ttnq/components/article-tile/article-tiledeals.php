<?php
$feature_image_id = get_post_thumbnail_id( $post->ID );
$featured_img_url = get_the_post_thumbnail_url($post->ID,'large'); 
$featured_img_url_fnl = ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png";

$provider = get_field('provider', $post->ID);
$terms = get_field('terms', $post->ID);
$terms = strlen($terms) > 100 ? substr($terms,0,100)."..." : $terms;
$category = get_the_terms($post->ID, "deal_category");
$category_name = $category[0]->name;
$post_title = $post->post_title;
$post_title = strlen($post_title) > 50 ? substr($post_title,0,50)."..." : $post_title;
$link = get_field('cta', $post->ID);
?>

<a target="blank" href="<?php echo $link; ?>" class="article-tile" >
   <div class="article-tile-image">
   	<?php if($feature_image_id): ?>
   		<img class="lazyload" data-object-fit="cover" <?php responsive_image($feature_image_id, 'large', true); ?>>
   	<?php else: ?>
   		<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('global_fallback_image', 'options'), 'large', true); ?>>
   	<?php endif; ?>
   </div>
   <div class="article-tile-cat">
   		<?php echo $category_name; ?>
   </div>
   <div class="article-tile-content">
	   	<h5><?php echo $post_title; ?></h5>
	   	<div class="article-tile-content-desc">
	   		<p><?php echo $terms; ?></p>
			<div class="article-tile-content-desc-cta">
				Read more <i class="fa fa-angle-right" aria-hidden="true"></i>
			</div>
	   	</div>
   </div>
</a>