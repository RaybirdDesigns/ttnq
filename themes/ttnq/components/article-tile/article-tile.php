<?php

// Article tile.
// Shows feature image and a snippet of the content.
$carousel_image_id = get_field('carousel_image');
$feature_image_id = get_post_thumbnail_id( $post->ID );
$carousel_image = $carousel_image_id ? wp_get_attachment_image_url($carousel_image_id, 'large') : wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large" )[0];
?>

<a href="<?php the_permalink(); ?>" class="article-tile" >
   <div class="article-tile-image">
   	<?php if ($carousel_image_id): ?>
   		<img class="lazyload" data-object-fit="cover" <?php responsive_image($carousel_image_id, 'large', true); ?>>
   	<?php elseif($feature_image_id): ?>
   		<img class="lazyload" data-object-fit="cover" <?php responsive_image($feature_image_id, 'large', true); ?>>
   	<?php else: ?>
   		<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('global_fallback_image', 'options'), 'large', true); ?>>
   	<?php endif; ?>
   </div>
   <div class="article-tile-cat">
   		<?php echo get_the_category( $id )[0]->name; ?>
   </div>
   <div class="article-tile-content">
	   	<h5><?php the_title(); ?></h5>
	   	<div class="article-tile-content-desc">
	   		<?php the_excerpt(); ?>
			<div class="article-tile-content-desc-cta">
				Read more <i class="fa fa-angle-right" aria-hidden="true"></i>
			</div>
	   	</div>
   </div>
</a>