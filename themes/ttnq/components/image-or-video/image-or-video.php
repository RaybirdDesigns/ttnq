<?php
// check if the nested repeater field has rows of data
if( get_sub_field('image_content') ):

	$image_dir = get_sub_field('image_direction') ? 'direction-right' : null;
	$count = count(get_sub_field('image_content'));
	$i = 0;

 	echo '<div class="row blog-image-wrapper"><div class="small-12 large-10 large-offset-1">';

 	// loop through the rows of data
    while ( have_rows('image_content') ) : the_row();

    	$i++;
		$caption = get_sub_field('caption') && $count > 1 ? '<div class="blog-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
		$single_caption = $count == 1 ? '<div class="blog-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : null;

		switch($count) {
			case 1 :
				$layout = 'one-up-';
				break;
			case 2 : 
				$layout = 'two-up-';
				break;
			case 3 :
				$layout = 'three-up-';
				break;
			case 4 :
				$layout = 'four-up-';
				break;
			default:
				$layout = 'one-up-';
		}

		echo '<div class="blog-image '.$image_dir.' '.$layout.$i.' blog-image-'.$i.'" data-responsive-background-image>
				<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

	endwhile;

	echo '</div></div>';

endif;

if( get_sub_field('video') ):

	$has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

	echo '<div class="row"><div class="small-12 large-10 large-offset-1"><div class="blog-video-wrapper '.$has_caption.'">';
	the_sub_field('video');
	echo '</div></div></div>';

	if (get_sub_field('video_caption')) :
		echo '<div class="blog-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
	endif;

endif;
?>