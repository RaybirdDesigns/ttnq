<?php

$current_post_type = get_post_type();

// used to get the parent of a page
$ancestors = get_post_ancestors( $post );
$ancestor_count = count($ancestors);

// get first tag
if (get_the_tags()) {
	$allposttags = get_the_tags();
	$first_tag = $allposttags[0]->name;
	$first_tag_slug = $allposttags[0]->slug;
}

?>

<div class="breadcrumb">
	<ul class="list-reset list-inline breadcrumb-list">
		<li><a href="<?php echo get_home_url(); ?>">Home</a></li>

		<?php global $post;
		  if ( $post->post_parent && !is_page_template( 'things-to-do-page.php' ) ) { ?>
		  <li>
		    <a href="<?php echo get_permalink( $post->post_parent ); ?>" >
		    <?php echo get_the_title( $post->post_parent ); ?>
		    </a>
		   </li>
		<?php } ?>

		<?php if ($current_post_type == 'itinerary') : ?>
		<li><a href="<?php echo get_home_url(); ?>/plan-your-trip">Plan your trip</a></li>
			<?php if (get_field('itinerary_type') == 'driving'): ?>
				<li><a href="<?php echo get_home_url(); ?>/plan-your-trip/driving-itineraries">Driving Itineraries</a></li>
			<?php elseif(get_field('itinerary_type') == 'walking'): ?>
				<li><a href="<?php echo get_home_url(); ?>/plan-your-trip/walking-itineraries">Walking Itineraries</a></li>
			<?php elseif(get_field('itinerary_type') == 'biking'): ?>
				<li><a href="<?php echo get_home_url(); ?>/plan-your-trip/biking-itineraries">Biking Itineraries</a></li>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if ($current_post_type == 'accommodation') : ?>
		<li><a href="<?php echo get_home_url(); ?>/plan-your-trip">Plan your trip</a></li>
		<li><a href="<?php echo get_home_url(); ?>/plan-your-trip/where-to-stay">Where to stay</a></li>
		<?php endif; ?>

		<?php if ($current_post_type == 'product') : ?>
		<li><a href="/things-to-do">Things to do</a></li>
			<?php if ($first_tag !== 'blank') : ?>
			<li><a href="<?php echo get_home_url().'/things-to-do/'.$first_tag_slug; ?>"><?php echo $first_tag; ?></a></li>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ($current_post_type == 'post') : ?>
		<li><a href="<?php echo get_home_url(); ?>/post-list/?post-type=post">Articles</a></li>
		<?php endif; ?>

		<?php if ($current_post_type == 'event') : ?>
		<li><a href="<?php echo get_home_url() ?>/whats-on">What's on</a></li>

		<?php if (get_field('event_category') == 'Arts & Culture') : ?>
		<li><a href="<?php echo get_home_url() ?>/whats-on/arts-culture">Art's &amp; Culture</a></li>
		<?php endif; ?>
		<?php if (get_field('event_category') == 'Food & Wine') : ?>
		<li><a href="<?php echo get_home_url() ?>/whats-on/food-wine">Food &amp; Wine</a></li>
		<?php endif; ?>
		<?php if (get_field('event_category') == 'Music') : ?>
		<li><a href="<?php echo get_home_url() ?>/whats-on/music">Music</a></li>
		<?php endif; ?>
		<?php if (get_field('event_category') == 'Sports') : ?>
		<li><a href="<?php echo get_home_url() ?>/whats-on/sports">Sports</a></li>
		<?php endif; ?>

		<?php endif; ?>

		<?php if (is_page_template( 'things-to-do-page.php' )) : ?>
		<li><a href="<?php echo get_home_url() ?>/things-to-do">Things to do</a></li>
		<?php endif; ?>

		<?php if (is_page_template( 'things-to-do-page.php' ) && $ancestor_count >= 2) : ?>
		<li><a href="<?php echo get_the_permalink( $ancestors[0], false ); ?>"><?php echo get_the_title( $ancestors[0] ); ?></a></li>
		<?php endif; ?>

		<li class="current-page"><?php the_title(); ?></li>
	</ul>
</div>