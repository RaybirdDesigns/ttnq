<?php if(!empty(get_field('related_full_width_bg'))):  ?>

    <?php
    $cta_url = get_field('related_pages_full_width_external_url') ?: get_field('related_pages_full_width_url');
    $cta_open_blank = get_field('related_pages_full_width_external_url') ? 'target="_blank"' : null;
    ?>
<section id="full-width-related" class="collapse">
	<div class="full-width-related clearfix text-center">
		<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('related_full_width_bg'), 'full', true); ?> >
		<div class="full-width-related-content">
			<?php the_field('related_pages_full_width_text'); ?>
			<?php if(!empty(get_field('related_pages_full_width_url'))): ?>
			<a class="btn btn-medium btn-ghost lite-blue btn-margin"
               href="<?=$cta_url?>"
                <?=$cta_open_blank?>>
                <?php the_field('related_pages_button_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>