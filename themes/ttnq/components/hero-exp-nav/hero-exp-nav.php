<?php
$wet_tropics_image = get_field('wet_tropics_image');
$tropical_lifestyle_image = get_field('tropical_lifestyle_image');
$great_barrier_reef_image = get_field('great_barrier_reef_image');
$adventure_image = get_field('adventure_image');
$indigenous_culture_image = get_field('indigenous_culture_image');

$wet_tropics_post = get_field('wet_tropics_url');
$tropical_lifestyle_post = get_field('tropical_lifestyle_url');
$great_barrier_reef_post = get_field('great_barrier_reef_url');
$adventure_reef_post = get_field('adventure_reef_url');
$indigenous_culture_post = get_field('indigenous_culture_url');

$has_two_col_1 = $wet_tropics_image && $tropical_lifestyle_image ? 'two-up' : null;
$has_two_col_3 = $adventure_image && $indigenous_culture_image ? 'two-up' : null;


?>

<div class="hero-experience-nav clearfix">
	<div class="small-12 columns">
		<div class="small-12 large-4 columns hero-experience-nav-col column-1 <?php echo $has_two_col_1; ?>">
			<?php if($wet_tropics_image): ?>
			<a href="<?php echo get_the_permalink( $wet_tropics_post->ID ); ?>"
			   class="hero-experience-tile medium-height">
			   <img class="lazyload" data-object-fit="cover" <?php responsive_image($wet_tropics_image, 'full', true); ?> >
				<div class="hero-experience-tile-content">
					<svg class="hero-exp-ico" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#tnqwettropicsrainforest"></use>
                    </svg>
					<h3><?php echo get_the_title( $wet_tropics_post->ID ); ?></h3>
					<div class="hero-experience-tile-content-cta">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
				</div>
			</a>
			<?php endif; ?>
			<?php if($tropical_lifestyle_image): ?>
			<a href="<?php echo get_the_permalink( $tropical_lifestyle_post->ID ); ?>"
			   class="hero-experience-tile small-height">
			   <img class="lazyload" data-object-fit="cover" <?php responsive_image($tropical_lifestyle_image, 'full', true); ?> >
				<div class="hero-experience-tile-content">
					<svg class="hero-exp-ico" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#tropical-lifestyle"></use>
                    </svg>
					<h3><?php echo get_the_title( $tropical_lifestyle_post->ID ); ?></h3>
					<div class="hero-experience-tile-content-cta">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
				</div>
			</a>
			<?php endif; ?>
		</div>
		<div class="small-12 large-4 columns hero-experience-nav-col column-2">
			<?php if($great_barrier_reef_image): ?>
			<a href="<?php echo get_the_permalink( $great_barrier_reef_post->ID ); ?>" class="hero-experience-tile full-height">
				<img class="lazyload" data-object-fit="cover" <?php responsive_image($great_barrier_reef_image, 'full', true); ?> >
				<div class="hero-experience-tile-content">
					<svg class="hero-exp-ico" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#reef-isalnds-and-beaches"></use>
                    </svg>
					<h3><?php echo get_the_title( $great_barrier_reef_post->ID ); ?></h3>
					<div class="hero-experience-tile-content-cta">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
				</div>
			</a>
			<?php endif; ?>
		</div>
		<div class="small-12 large-4 columns hero-experience-nav-col column-3 <?php echo $has_two_col_3; ?>">
			<?php if($adventure_image): ?>
			<a href="<?php echo get_the_permalink( $adventure_reef_post->ID ); ?>" class="hero-experience-tile small-height">
				<img class="lazyload" data-object-fit="cover" <?php responsive_image($adventure_image, 'full', true); ?> >
				<div class="hero-experience-tile-content">
					<svg class="hero-exp-ico" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#adventure"></use>
                    </svg>
					<h3><?php echo get_the_title( $adventure_reef_post->ID ); ?></h3>
					<div class="hero-experience-tile-content-cta">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
				</div>
			</a>
			<?php endif; ?>
			<?php if($indigenous_culture_image): ?>
			<a href="<?php echo get_the_permalink( $indigenous_culture_post->ID ); ?>" class="hero-experience-tile medium-height">
				<img class="lazyload" data-object-fit="cover" <?php responsive_image($indigenous_culture_image, 'full', true); ?> >
				<div class="hero-experience-tile-content">
					<svg class="hero-exp-ico" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#indigenous-art"></use>
                    </svg>
					<h3><?php echo get_the_title( $indigenous_culture_post->ID ); ?></h3>
					<div class="hero-experience-tile-content-cta">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
				</div>
			</a>
			<?php endif; ?>
		</div>
	</div>
</div>