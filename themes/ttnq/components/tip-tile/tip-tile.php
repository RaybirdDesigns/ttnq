<div class="tip-tile clearfix">
	<div class="tip-icon-wrapper small-12 medium-2 float-left">
		<svg class="tip-icon" role="presentation">
			<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/tips-sprite.svg#<?php the_sub_field('icon'); ?>"></use>
	    </svg>
	</div>
	<div class="tip-text small-12 medium-10 float-left">
		<?php the_sub_field('text'); ?>
	</div>
</div>