<?php

switch(get_field('theme_colour')) {
	case '639083' :
		$stackla_bg_colour = '274f2c';
		break;
	case 'CE975C' : 
		$stackla_bg_colour = '2f2920';
		break;
	case '6fa0ae' :
		$stackla_bg_colour = '1f5668';
		break;
	default:
		$stackla_bg_colour = '2f2920';
}

?>

<style>
	
	#stackla {
		background-color: #<?php echo $stackla_bg_colour; ?>;
	}
	
</style>

<section id="stackla">
	<div class="row">
		<div class="small-12 columns text-center">
			<h2>#exploreTNQ #explorecairnsGBR</h2>
		</div>
	</div>
	<div class="clearfix">
		<?php the_field('stackla_html_snippet'); ?>
	</div>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost lite-brown btn-margin" href="/social">View social feed <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>

<?php the_field('stackla_snippet', 'option'); ?>

