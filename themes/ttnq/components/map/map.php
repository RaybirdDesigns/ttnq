<?php
$map_heading = get_sub_field('map_heading') ? get_sub_field('map_heading') : get_field('map_heading'); 
$map_remove_padding = get_sub_field('map_remove_padding') ? get_sub_field('map_remove_padding') : get_field('map_remove_padding');
$map_region_filter = get_sub_field('map_region_filter') ? get_sub_field('map_region_filter') : get_field('map_region_filter');
$map_initial_zoom = get_sub_field('map_initial_zoom') ? get_sub_field('map_initial_zoom') : get_field('map_initial_zoom');
$map_initial_zoom = $map_initial_zoom ? $map_initial_zoom : 120;

// Get all selected page regions and create slug array for use in wp_query
// TODO define this in a model
$region_slugs = '';
if (!empty($map_region_filter)) {
	$regions = array();
	foreach ($map_region_filter as $region) {
		$regions[] = $region->slug;
	}
	$region_slugs = implode(',',$regions);
}

$bespoke_listings = get_field('map_bespoke_feature_listings');
if(!empty($bespoke_listings)) {
	$bespoke_listing_ids = array();
	foreach($bespoke_listings as $listing) {
		$bespoke_listing_ids[] = $listing->ID;
	}
}

$featured_listings = get_field('map_listings_categories');

if (!empty($featured_listings)) {
	$featured_listings_button_text = array();
	$featured_listings_button_slug = array();

	foreach($featured_listings as $listing) {
		$featured_listings_button_text[] = $listing['map_listing_category_name'] ? '"'.$listing['map_listing_category_name'].'"' : '"'.$listing['map_listing_category']['label'].'"';
		$featured_listings_button_slug[] = '"'.$listing['map_listing_category']['value'].'"';
	}
}

?>

<script>
    // TODO Fix this system, needs a model to control this script items
	var mapSettings = {
		bespoke: {
		<?php if (!empty($bespoke_listings)): ?>
			buttonText: "<?php echo get_field('bespoke_feature_heading'); ?>",
			listingIds: [<?php echo implode(',',$bespoke_listing_ids); ?>],
		<?php endif; ?>
		},
		region : "<?php echo $region = $region_slugs ? $region_slugs : null; ?>",
		listings: {
		<?php if(!empty($featured_listings)): ?>
			<?php foreach($featured_listings as $listing_category): ?>
                <?php echo $listing_category['map_listing_category']['value']; ?>: [
                    "<?php echo $listing_category['map_listing_category_name'] ? $listing_category['map_listing_category_name'] : $listing_category['map_listing_category']['label']; ?>",
                    <?php
                    if($listing_category['map_listing_category']['value'] == 'product'){
                        $cat_value = 'attraction';
                    }elseif($listing_category['map_listing_category']['value'] == 'accommodation'){
                        $cat_value = 'accomm';
                    }else{
                        $cat_value = $listing_category['map_listing_category']['value'];
                    }
                    $sub_categories = $listing_category['map_'.$cat_value.'_sub_categories']; ?>
                    <?php if(!empty($sub_categories)): ?>
                        <?php foreach ($sub_categories as $sub_category): ?>
                            <?php if($sub_category->count > 0): ?>
                            {
                                label: "<?php echo $sub_category->name; ?>",
                                slug: "<?php echo $sub_category->slug; ?>"
                            },
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                ],
			<?php endforeach; ?>
		<?php endif; ?>
		}
	};
</script>

<section class="map">
	<?php if ($map_heading): ?>
    <!-- TODO Add Template colour scheme -->
    <div class="map__header">
        <div class="row">
            <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2 columns text-center bm underline">
                <?php echo $map_heading; ?>
            </div>
        </div>
    </div>
	<?php endif; ?>
	<div class="map-app collapse-top collapse-bottom"
	id="map-root"
	data-zoom="<?php echo $map_initial_zoom; ?>"
	data-base="<?php echo site_url(); ?>"
	data-key="AIzaSyA-oulh7EDnr1Za7X2z-tsyhX6c8C5Jlsg"></div>
</section>

<style>
    .map__header {
        padding: 40px 0;
        background: #8fafa6;
        color: #fff;
    }

    .map__header h2:before {
        border-bottom-color: #639083;
    }

    .map__header h2,
    .map__header p {
        color: #fff;
    }
</style>