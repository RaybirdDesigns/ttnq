<?php

// Influencers tile
// Used in:
// Hero experience

$bg_image = has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'small' )[0] : null;

?>

<div class="influencer-post component">
	
	<div class="row">
		<div class="small-12 columns text-center">
			<h3><?php the_title(); ?></h3>
			<p><?php the_field('influencer_statistics'); ?></p>
			<div class="profile-pic" style="background-image: url('<?php echo $bg_image; ?>');"></div>
		</div>
	</div>
	<?php if( have_rows('influencer_images') ): ?>
		<div class="clearfix component">
			<ul class="list-reset list-inline influencer-post-carousel">

			<?php while( have_rows('influencer_images') ): the_row(); 

				$image = get_sub_field('image');

				?>
					
				<li>
					<div style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></div>
				</li>

				<?php endwhile; ?>

			</ul>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="small-12 columns text-center">
			<a class="btn btn-medium btn-ghost lite-blue" href="/social">View social feed <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
	</div>
</div>