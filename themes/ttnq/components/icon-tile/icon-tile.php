<div class="icon-tile clearfix">
	<a class="icon-tile-link" href="<?php the_permalink(); ?>">
		<div class="icon-tile-wrapper">
			<svg class="icon-tile-icon" role="presentation">
				<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#<?php echo $post_slug = get_post_field( 'post_name', get_post() ); ?>"></use>
		        </use>
		    </svg>
		</div>
	    <h6><?php the_title(); ?>&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
	</a>
</div>