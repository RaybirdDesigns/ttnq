<?php
/**
 * Template Name: Landing page
 *
 * Used by: Hero experience pages and Landing pages
 */
$category = get_the_category();
// $first_category = $category[0]->slug;
$first_category = !empty($category) ? $category[0]->slug : null;

$theme_colour = get_field('theme_colour');

$is_landing_page = get_field('is_landing_page') == true ? true : false;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-experience/hero', 'experience'); ?>

<style>

	.tip-icon {
		fill: #<?php echo $theme_colour ?>;
	}

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.feature-narrative-tile a {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px transparent;
	}

	.feature-narrative-tile a:hover {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	.feature-narrative-tile a:focus {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	button.scroll-btn {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	#influencers {
		background-color: #<?php echo $influencer_bg_colour; ?>
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<?php

// check if the flexible content field has rows of data
if( have_rows('landing_page_content') ):

     // loop through the rows of data
    while ( have_rows('landing_page_content') ) : the_row();

        if( get_row_layout() == 'intro_copy' ): ?>

    	<section id="description-1">
			<div class="row">
				<div class="small-12 medium-8 medium-offset-2 columns text-center">
					<?php the_sub_field('free_text_section'); ?>
				</div>
			</div>
		</section>

		<?php elseif( get_row_layout() == 'tips' ): ?>
        <section id="tips" class="grey-background">
            <div class="row component">
                <div class="small-12 medium-8 medium-offset-2 columns text-center">
                    <h2>Tips</h2>
                </div>
            </div>
            <?php if( have_rows('tips') ): ?>
            <div class="row">
                <ul class="list-reset tips-list">
                    <?php while ( have_rows('tips') ) : the_row();?>
                        <li class="small-12 medium-6 columns">
                            <?php get_template_part('components/tip-tile/tip', 'tile') ?>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
            <?php endif; ?>
        </section>

        <?php elseif( get_row_layout() == 'events' ): ?>
        <section id="events">
            <div class="row component">
                <div class="small-12 medium-8 medium-offset-2 columns text-center">
                    <h2>What's on</h2>
                </div>
            </div>
            <?php if (get_sub_field('events_intro_text')): ?>
                <div class="row component">
                    <div class="small-12 medium-8 medium-offset-2 columns text-center">
                        <?php the_sub_field('events_intro_text'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            $event_start = get_sub_field('time_of_event');
            $event_end = get_sub_field('end_time_of_event');
            $event_category = get_sub_field('event_category');
            $posts = get_events_within($event_start, $event_end, $event_category);
            ?>

            <?php if( count($posts) > 0 ): ?>
            <div class="row">
                <ul class="list-reset events-list">
                    <li class="small-12 columns">
                        <?php get_template_part('components/tiles-5-up/tiles-5-up', null) ?>
                    </li>
                </ul>
            </div>

            <?php endif; ?>

            <?php
            $events_URL = add_query_arg(
                array(
                    'post-type' => 'event',
                    'tag' => $first_category,
                ), get_site_url() . '/post-list/'
            );

            $cta_text = 'Browse all events ';
            ?>
            <div class="row text-center">
                <a class="btn btn-medium btn-ghost green btn-margin" href="<?php echo $events_URL; ?>"><?php echo $cta_text; ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
        </section>

        <?php elseif( get_row_layout() == 'product_row' ): ?>
		
		<section id="products">
			<?php if (get_sub_field('product_text')): ?>
			<div class="row component">
				<div class="small-12 medium-8 medium-offset-2 columns text-center">
					<?php the_sub_field('product_text'); ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="small-12">
					<ul class="list-reset tile-4-list">
					<?php while ( have_rows('products2') ): the_row();?>

					<?php	$post_object = get_sub_field('product2'); ?>

							<?php if ( $post_object ) : 
								
								$post = $post_object;
								setup_postdata( $post_object ); ?>
								
							<li class="small-6 medium-6 large-3 columns">
								<?php get_template_part('components/custom-post-tile/custom-post-tile', null); ?>
							</li>

					<?php	wp_reset_postdata();
							endif;
						endwhile; ?>
					</ul>
				</div>
			</div>
			<?php if (get_sub_field('cta_text')):

			$product_URL = add_query_arg(
				array(
					'post-type' => 'product'
					), get_site_url() . '/post-list/'
				);
			?>
			<div class="row text-center">
				<a class="btn btn-medium btn-ghost btn-margin" href="<?php echo $product_URL; ?>"><?php the_sub_field('cta_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			</div>
			<?php endif; ?>
		</section>

        <?php elseif( get_row_layout() == 'narrative_tiles' ): ?>

		<section id="feature-narrative" class="collapse">
			<div class="clearfix">
				<?php
				if( have_rows('narrative_feature') ):

					$feature_index = 0;
				    $alternate_tiles = true;

				    while ( have_rows('narrative_feature') ) : the_row();

				    	$feature_index++;

				        include(locate_template('/components/feature-narrative-tile/feature-narrative-tile.php'));

				    endwhile;

				endif;
				?>
			</div>
		</section>

		<?php elseif( get_row_layout() == 'itineraries_carousel' ): ?>

		<?php include(locate_template('/components/itineraries/itineraries-carousel/itineraries-carousel.php')); ?>

        <?php elseif( get_row_layout() == 'articles' ): ?>

			<section id="articles-1">
				<div class="row">
					<div class="small-12 medium-6 medium-offset-3 columns text-center component">
						<?php the_sub_field('articles_heading_text'); ?>
					</div>
				</div>
				<div class="row">
					<div class="article-carousel">
					<?php
						if ( have_rows('articles') ) {

							while ( have_rows('articles') ) {

								the_row();

								$post_object = get_sub_field('article');

								if ( $post_object ) {

									$post = $post_object;
									setup_postdata( $post_object ); ?>
									
										<?php get_template_part('components/article-tile/article', 'tile') ?>

					<?php			wp_reset_postdata();
								}
							}
						}
					?>
					</div>
				</div>

				<?php 
				$post_URL = add_query_arg(
					array(
						'post-type' => 'post',
						), get_site_url() . '/post-list/' 
					);
				?>
				<div class="row text-center">
					<a class="btn btn-medium btn-ghost blue btn-margin" href="<?php echo $post_URL; ?>">Browse all posts <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
				</div>
			</section>

        <?php elseif( get_row_layout() == 'cta_banner' ): ?>

		<?php
    	// internal or external link. External overrides internal.
		$full_width_banner_url = get_sub_field('related_pages_full_width_external_url') ? addhttp(get_sub_field('related_pages_full_width_external_url')) : get_sub_field('related_pages_full_width_url');
		$target = get_sub_field('related_pages_full_width_external_url') ? 'target="_blank"' : null;

		// add image below CTA button. Can set max width.
		$image_caption = get_sub_field('banner_image_caption');
		$image_caption_width = get_sub_field('banner_image_caption_width') ? get_sub_field('banner_image_caption_width') : '300';

		$tracking_id = get_sub_field('banner_tracking_id') ? 'data-ttnq-id="' . get_sub_field('banner_tracking_id') . '"' : null;

		$icon = get_sub_field('related_pages_full_width_external_url') ? 'window-restore' : 'chevron-right';
		?>
		<section id="full-width-related-blog" class="collapse">
			<div <?php echo $tracking_id; ?> class="full-width-related clearfix text-center" data-responsive-background-image >
				<img <?php responsive_image(get_sub_field('related_full_width_bg'), 'full') ?> >
				<div class="full-width-related-content">
					<?php the_sub_field('related_pages_full_width_text'); ?>
					<a <?php echo $tracking_id; ?> class="blog-button btn btn-medium btn-ghost btn-margin" <?php echo $target; ?> href="<?php echo $full_width_banner_url; ?>"><?php the_sub_field('related_pages_button_text'); ?> <i class="fa fa-<?php echo $icon; ?>" aria-hidden="true"></i></a>
					<?php if ($image_caption): ?>
					<img style="margin-top: 20px; width:100%; transform: translate3d(0,0,0); max-width: <?php echo $image_caption_width; ?>px;" src="<?php echo $image_caption['url'] ?>" alt="<?php echo $image_caption['alt'] ?>">
					<?php endif; ?>
				</div>
			</div>
		</section>

		<?php elseif( get_row_layout() == 'simple_banner' ): ?>

			<?php get_template_part( 'components/simple-banner/simple-banner' ); ?>

		<?php elseif( get_row_layout() == 'video_section' ): ?>

		<section id="video">
			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php get_template_part('components/video/video') ?>
				</div>
				<div class="small-12 medium-6 columns">
					<?php the_sub_field('video_text'); ?>

					<?php if(get_sub_field('video_btn_url')) : ?>
					<a class="btn btn-medium btn-ghost blue btn-margin" href="<?php the_sub_field('video_btn_url'); ?>"><?php the_sub_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					<?php endif; ?>

				</div>
			</div>
		</section>

		<?php elseif( get_row_layout() == 'influencer_section' ): ?>

			<?php
			// Influencers tile
			// Used in:
			// Hero experience
			// influencer background colour
			switch($theme_colour) {
				case '639083' :
					$influencer_bg_colour = '274f2c';
					break;
				case 'CE975C' : 
					$influencer_bg_colour = '2f2920';
					break;
				case '6fa0ae' :
					$influencer_bg_colour = '1f5668';
					break;
				default:
					$influencer_bg_colour = '2f2920';
			}
			?>
			
			<style>
				#influencers {
					background-color: #<?php echo $influencer_bg_colour; ?>
				}
			</style>

			<section id="influencers">
				<div class="row">
					<div class="small-12 columns text-center">
						<h2>#exploreTNQ</h2>
					</div>
				</div>
				<div class="clearfix">
					<?php

						$post_object = get_sub_field('influencers_post_landing');

						if ( $post_object ) {

							$post = $post_object;
							setup_postdata( $post );
						?>

						<?php get_template_part('components/influencer-post/influencer', 'post') ?>
									
					<?php	wp_reset_postdata();
						}
					?>
				</div>
			</section>

		<?php elseif( get_row_layout() == 'related_content' ): ?>

			<section id="related-pages" class="collapse-bottom">
				<div class="row">
					<div class="small-12 large-8 large-offset-2 columns text-center component">
						<?php the_sub_field('related_page_text'); ?>
					</div>
				</div>
				<div class="clearfix">
					<ul class="list-reset">
						<?php

							while ( have_rows('related_pages') ) {

							the_row();

							$post_object = get_sub_field('featured_page');

							if ( $post_object ) {

								$post = $post_object;
								setup_postdata( $post_object );

								?>
								
								<li class="small-12 medium-6 large-3 float-left">
									<?php get_template_part('components/image-title-tile/image-title', 'tile') ?>
								</li>
								
					<?php		wp_reset_postdata();
							}
						}
					?>
					</ul>
				</div>
			</section>

        <?php endif;

    endwhile;

endif;

?>

<?php get_footer()?>