<?php
$overlay_colour     = get_sub_field('background_overlay')['colour'];
$overlay_opacity    = get_sub_field('background_overlay')['opacity'];
?>
<div class="overlay" style="background: <?=$overlay_colour?>; opacity: <?=$overlay_opacity?>;"></div>