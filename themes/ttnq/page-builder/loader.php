<?php
if(have_rows('page_builder')) :
    echo '<main class="page-builder-wrapper">';
        while(have_rows('page_builder')) :
            the_row();

            include 'components/' . get_row_layout() . '.php';

        endwhile;
    echo '</div>';
endif;