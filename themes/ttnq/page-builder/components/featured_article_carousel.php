<?php
//TODO Replace the component below with a new system

$background_colour  = get_sub_field('background_colour');

// Section Spacing
$padding_top        = get_sub_field('section_padding')['top'];
$padding_bottom     = get_sub_field('section_padding')['bottom'];
$padding_left       = get_sub_field('section_padding')['left'];
$padding_right      = get_sub_field('section_padding')['right'];
?>

<section id="page-builder-<?=get_row_index()?>"
         class="featured-article-carousel background-color--<?=$background_colour?> <?=$padding_top?> <?=$padding_bottom?> <?=$padding_left?> <?=$padding_right?>
            text-center">

    <div class="container button-wrapper">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <?php echo get_sub_field('articles_text'); ?>
            </div>
        </div>
        <?php get_template_part('components/article-tile/featured-articles'); ?>
    </div>
</section>