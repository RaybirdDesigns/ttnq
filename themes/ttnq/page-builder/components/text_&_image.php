<?php
//TODO Replace the component below with a new system

$background_colour  = get_sub_field('background_colour');

// Section Spacing
$padding_top        = get_sub_field('section_padding')['top'];
$padding_bottom     = get_sub_field('section_padding')['bottom'];
$padding_left       = get_sub_field('section_padding')['left'];
$padding_right      = get_sub_field('section_padding')['right'];
?>

<section id="page-builder-<?=get_row_index()?>"
         class="text-and-image button-wrapper background-color--<?=$background_colour?> <?=$padding_top?> <?=$padding_bottom?> <?=$padding_left?> <?=$padding_right?>
            text-center">

<?php
if( have_rows('text_and_image') ):

		$section_index = 0;

		     // loop through the rows of data
		while ( have_rows('text_and_image') ) : the_row();

            $section_index++;

            $grey_background = get_sub_field('grey_background') ? 'grey-bg' : null;

            echo '<div class="flexible-text-and-image-wrapper container '.$grey_background.'">';

            if( get_sub_field('heading') ):

                echo '<div class="row component">
                                    <h2 class="text-center small-12 medium-12 large-6 large-offset-3">'.get_sub_field('heading').'</h2></div>';

            endif;

            // +++++++++++++++++++++++++

            // check if the nested repeater field has rows of data
            if( get_sub_field('image_content') ):

                $image_dir = get_sub_field('image_direction') ? 'direction-right' : null;
                $count = count(get_sub_field('image_content'));
                $i = 0;

                echo '<div class="row flexible-image-wrapper"><div class="small-12 large-10 large-offset-1">';

                // loop through the rows of data
                while ( have_rows('image_content') ) : the_row();

                    $i++;
                    $handle = get_sub_field('social_handle') && $count > 1 ? '<p class="social-handle--multi-desktop">'.get_sub_field('social_handle').'</p>' : null;
                    $caption = get_sub_field('caption') && $count > 1 ? '<div class="flexible-image-caption"><p>'.get_sub_field('caption').'</p>'.$handle.'</div>' : null;
                    $single_caption = $count == 1 && get_sub_field('caption') ? '<div class="flexible-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : '<div class="flexible-image-caption-below-mobile"><p>'.get_sub_field('caption').'</p></div>';
                    $single_handle = $count == 1 && get_sub_field('social_handle') ? '<div class="flexible-image-handle-below"><p>'.get_sub_field('social_handle').'</p></div>' : '<div class="flexible-image-handle-below-mobile"><p>'.get_sub_field('social_handle').'</p></div>';

                    switch($count) {
                        case 1 :
                            $layout = 'one-up-';
                            break;
                        case 2 :
                            $layout = 'two-up-';
                            break;
                        case 3 :
                            $layout = 'three-up-';
                            break;
                        case 4 :
                            $layout = 'four-up-';
                            break;
                        default:
                            $layout = 'one-up-';
                    }

                    echo '<div data-bp="'.wp_get_attachment_image_url(get_sub_field('image'), 'full').'" class="lightbox-image flexible-image '.$image_dir.' '.$layout.$i.' flexible-image-'.$i.'" data-responsive-background-image>
                                        <img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption.$single_handle;

                endwhile;

                echo '</div></div>';

                endif;

                if( get_sub_field('video') ):

                    $has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

                    echo '<div class="row"><div class="small-12 large-10 large-offset-1"><div class="flexible-video-wrapper '.$has_caption.'">';
                    the_sub_field('video');
                    echo '</div></div></div>';

                    if (get_sub_field('video_caption')) :
                        echo '<div class="flexible-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
                    endif;

                endif;

                // +++++++++++++++++++++++++++++++++++++++++++++++++

                if( get_sub_field('text') ):

                    echo '<div class="row"><div class="col-md-8 offset-md-2 text-center">';

                    the_sub_field('text');

                    echo '</div>';

                endif;

                // +++++++++++++++++++++++++++++++++++++++++++++++++

                if( get_sub_field('button_url') || get_sub_field('button_link') ):

                    $button_url = get_sub_field('button_url') ? get_sub_field('button_url') : get_sub_field('button_link');
                    $icon = get_sub_field('button_url') ? 'window-restore' : 'chevron-right';
                    $target = get_sub_field('button_url') ? 'target="_blank"' : null;

                    echo '<div class="text-center col-12"><a '.$target.' class=" btn btn-medium btn-ghost ghost-black-text blue" href="'.$button_url.'">'.get_sub_field('button_text').' <i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></div>';

                endif;

                echo '</div></div>';

        endwhile;

	endif;
	?>
</section>
