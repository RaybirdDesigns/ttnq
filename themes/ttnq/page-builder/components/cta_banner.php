<?php
// Globals
$background_image = get_sub_field('background_image');
$title = get_sub_field("title");
$subtitle = get_sub_field("subtitle");
$button_text = get_sub_field("button_text");
$button_url = get_sub_field("button_url");

?>

<section id="page-builder-<?=get_row_index()?>" class="cta-banner" style="background: url(<?=$background_image?>) no-repeat center; background-size: cover;">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 offset-md-3">
                <h2 class="section-heading">
                    <?=$title?>
                </h2>
            </div>
            <div class="col-md-6 offset-md-3">
                <div class="section-subtitle">
                    <?=$subtitle?>
                </div>
            </div>
            <div class="col-md-6 offset-md-3 mt-3">
                <div class="button-wrapper">
                    <a href="<?=$button_url?>" class="btn btn-ghost btn-medium blue">
                        <?=$button_text?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>