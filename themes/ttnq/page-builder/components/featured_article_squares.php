<?php
$background_colour  = get_sub_field('background_colour');

// Section Spacing
$padding_top        = get_sub_field('section_padding')['top'];
$padding_bottom     = get_sub_field('section_padding')['bottom'];
$padding_left       = get_sub_field('section_padding')['left'];
$padding_right      = get_sub_field('section_padding')['right'];

// Content
$title = get_sub_field('title') ?: null;
$subtitle = get_sub_field('subtitle') ?: null;
?>

<section id="page-builder-<?=get_row_index()?>"
         class="featured-article-squares background-color--<?=$background_colour?> <?=$padding_top?> <?=$padding_bottom?> <?=$padding_left?> <?=$padding_right?>
            text-center">
    <div class="container-fluid">
        <div class="row no-gutters">
            <?php
            while(have_rows('featured_articles')) : the_row();
            $page       = get_sub_field('page')[0];
            $permalink  = get_permalink($page);
            $title      = get_sub_field('title') ?: $page->post_title;
            $image      = get_sub_field('image') ?: get_the_post_thumbnail_url($page);
            ?>
                <div id="featured-article-square-<?=the_row_index()?>"
                     class="featured-article-wrapper col-12 col-md-4"
                    style="background: url(<?=$image?>) no-repeat center; background-size: cover;">
                    <a href="<?=$permalink?>" class="hero-experience-tile">
                        <div class="content-container">
                            <h3><?=$title?></h3>
                            <div class="cta-item">Take the experience <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                        </div>
                    </a>
                </div>
            <?php
            endwhile;
            ?>
        </div>
    </div>
</section>