<?php
$background_colour  = get_sub_field('background_colour');

// Section Spacing
$padding_top        = get_sub_field('section_padding')['top'];
$padding_bottom     = get_sub_field('section_padding')['bottom'];
$padding_left       = get_sub_field('section_padding')['left'];
$padding_right      = get_sub_field('section_padding')['right'];

// Content
$title = get_sub_field('title') ?: null;
$subtitle = get_sub_field('subtitle') ?: null;
?>

<section id="page-builder-<?=get_row_index()?>"
         class="intro-text background-color--<?=$background_colour?> <?=$padding_top?> <?=$padding_bottom?> <?=$padding_left?> <?=$padding_right?> text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="section-title">
                    <?=$title?>
                </h2>
                <div class="content">
                    <?=$subtitle?>
                </div>
            </div>
        </div>
    </div>
</section>