<?php
$vimeo_id = get_sub_field('vimeo_id');
$cinemagraph = get_sub_field('cinemagraph') ?: '/wp-content/uploads/MillaaMillaa_cinemagraph.mp4';

?>

<section id="page-builder-<?=get_row_index()?>"
         class="video-background lightbox-video">
    <div class="cinemagraph-wrapper embed-container"
         data-bp="<?=$vimeo_id?>">
        <div class="overlay-play-button"></div>+
        <video autoplay muted loop>
            <source src="<?=$cinemagraph?>" type="video/mp4">
        </video>
    </div>
    <button class="scroll-down-btn scroll-down-btn-js" data-scroll-to="page-builder-<?=get_row_index() + 1?>">
        <svg class="down-arrow" role="presentation">
            <use xlink:href="https://ttnqstage.wpengine.com/wp-content/themes/ttnq/images/global-sprite.svg#arrow-ico-white"></use>
        </svg>
    </button>
</section>