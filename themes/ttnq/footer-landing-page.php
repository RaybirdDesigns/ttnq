<?php

// Footer

?>
		</div>
		<footer id="footer" class="footer">
			<div class="row">
			<div class="small-12 columns">
					<div class="small-6 columns footer-label">
						<a href="https://www.tropicalnorthqueensland.org.au<?php //echo site_url(); ?>"><h4>VISIT OUR WEBSITE</h4></a>
					</div>
					<div class="small-6 columns social-icons">	
						<div class="small-3 columns">
							<a target="_blank" href="https://www.facebook.com/tropicalnorthqueensland/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</div>
						<div class="small-3 columns">
							<a target="_blank" href="https://www.instagram.com/tropicalnorthqueensland/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</div>
						<div class="small-3 columns">
							<a target="_blank" href="https://twitter.com/CairnsGBR?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</div>
						<div class="small-3 columns">
							<a target="_blank" href="https://www.youtube.com/user/ExploreTNQ?&ab_channel=TropicalNorthQueensland%23exploreTNQ"><i class="fa fa-youtube" aria-hidden="true"></i></a>
						</div>
					</div>
                </div>
            </div>
        </footer>
		<?php wp_footer()?>
		<script type="text/javascript">
		let ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
		let page = 1;
		let tab_id =  "<?php echo ( isset($_GET['tab']) ? $_GET['tab'] : "" ) ?>";
		jQuery(function($) {
			
			$('body').on('click', '.see-more', function() {
				let data = {
					'action': 'load_posts_by_ajax',
					'page': page
				};
		
				$.post(ajaxurl, data, function(response) {
					if(response != '') {
						$('.deals-first-load').html(response);
						page++;
						$('.see-more').hide();
					} else {
						$('.see-more').hide();
					}
				});
			});
			let data_second = {
				'action': 'load_posts_by_ajax',
				'page': page
			};
	
			$.post(ajaxurl, data_second, function(response) {
				if(response != '') {
					$('.deals-first-load').html(response);
					page++;
				} else {
					$('.see-more').hide();
				}
			});


			$('body').on('click', '.tab-link', function() {
				let deal_category_id = $(this).data('id');
				let deal_category_tab = $(this).data('tab');
				let data_third = {
					'action': 'get_deal_item',
					'deal_category_id': deal_category_id,
					'deal_category_tab': deal_category_tab
				};
		
				$.post(ajaxurl, data_third, function(response) {
					if(response != '') {
						$('#'+deal_category_tab).html(response);
					}
				});
			});
			let deal_category_id = tab_id;
			let deal_category_tab = "tab-"+tab_id;
			let data_fourth = {
				'action': 'get_deal_item',
				'deal_category_id': deal_category_id,
				'deal_category_tab': deal_category_tab
			};
	
			$.post(ajaxurl, data_fourth, function(response) {
				if(response != '') {
					$('#'+deal_category_tab).html(response);
				}
			});

		});
		</script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){

				$(window).resize(function() {
					if($(window).width() <= 639){
						$(".center").slick({
							dots: false,
							infinite: true,
							centerMode: true,
							slidesToShow: 1,
							slidesToScroll: 1
						});             
					}
				});
				
				$(window).trigger('resize');


				
			});
		</script>
    </body>
</html>