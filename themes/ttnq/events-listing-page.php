<?php
/**
 * Template Name: Events listing
 */

$theme_colour = get_field('theme_colour');

$category = get_the_category();

$first_category = $category[0]->slug;

// check if page has children
// use to show sticky nav
function has_children() {
    global $post;

    $children = get_pages( array( 'child_of' => $post->ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }
}

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-things/hero', 'things') ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>
<?php if (has_children()) : ?>
<?php get_template_part('components/sticky-nav/sticky-nav'); ?>
<?php endif; ?>

<section id="description-1">

	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php
				have_posts();
				if (have_posts()) :
				    while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
				    <?php
				    endwhile;
			    endif;
				wp_reset_query();
			?>
		</div>
	</div>

	<?php
	// multi image
	if( have_rows('image_content') ):

		$count = count(get_field('image_content'));

		$i = 0;

	 	echo '<div class="row things-image-wrapper"><div class="small-12 large-10 large-offset-1">';

	 	// loop through the rows of data
	    while ( have_rows('image_content') ) : the_row();

	    	$i++;
			$caption = get_sub_field('caption') && $count > 1 ? '<div class="things-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
			$single_caption = $count == 1 && get_sub_field('caption') ? '<div class="things-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : null;

			switch($count) {
				case 1 :
					$layout = 'one-up-';
					break;
				case 2 : 
					$layout = 'two-up-';
					break;
				case 3 :
					$layout = 'three-up-';
					break;
				case 4 :
					$layout = 'four-up-';
					break;
				default:
					$layout = 'one-up-';
			}

			echo '<div class="lightbox-image things-image '.$layout.$i.' things-image-'.$i.'" data-responsive-background-image>
					<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

		endwhile;

		echo '</div></div>';

	endif;
	?>
</section>

<?php if(get_field('video')) : ?>
<section id="video">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 medium-6 columns">
			<?php the_field('video_text'); ?>

			<?php if(get_field('video_btn_url')) : ?>
			<a target="_blank"  class="btn btn-medium btn-ghost btn-margin" href="<?php the_field('video_btn_url'); ?>"><?php the_field('video_btn_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php endif; ?>

<?php if ( have_rows('featured_articles') ) : ?>
<section id="articles-1">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center component">
			<?php the_field('articles_text'); ?>
		</div>
	</div>
	<div class="row">
		<div class="article-carousel">
		<?php

				while ( have_rows('featured_articles') ) {

					the_row();

					$post_object = get_sub_field('featured_article');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object ); ?>
						
						<div>
							<?php get_template_part('components/article-tile/article', 'tile') ?>
						</div>

		<?php			wp_reset_postdata();
					}
				}
		?>
		</div>
	</div>

	<?php 
	$post_URL = add_query_arg(
		array(
			'post-type' => 'post',
			'category_name' => $first_category,
			), get_site_url() . '/post-list/' 
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost blue btn-margin" href="<?php echo $post_URL; ?>">Browse all posts <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>
<?php endif; ?>

<?php

// get subpages
$id = get_the_ID();

if ( have_posts() && has_children() ) : the_post();
  $gp_args = array(
      'post_type' => 'page',
      'post_parent' => $id,
      'order' => 'ASC',
      'orderby' => 'menu_order',
      'posts_per_page' => -1
  );
  $locations = get_posts($gp_args);

  $item_index = 0;
  $item_count = count($locations);

?>

<section id="in-page-nav" class="collapse-bottom">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center component">
			<?php the_field('explore_text'); ?>
		</div>
	</div>
	<div class="clearfix">
		<ul class="in-page-nav-list item-count-<?php echo $item_count; ?>">
			<?php foreach ($locations as $location) {

				$current_post_id = $location->ID;
				$bg_image = get_field('hero_banner_image', $current_post_id);
				$title = $location->post_title;
				$link = $location->guid;
				$item_index++;

	  			echo '<li class="in-page-nav-item in-page-nav-item-'.$item_index.'">
	  					<a class="in-page-nav-tile" href="'.$link.'" data-responsive-background-image>
	  					<img '.responsive_image_return(get_field('hero_banner_image', $current_post_id), 'large').' >
		  					<div class="tile-content">
								<h4>'.$title.'</h4>
							</div>
	  					</a>
	  				 </li>';
	  		} ?>
		</ul>
	</div>
</section>

<?php endif; ?>


<?php
/*
	Set dynamic events tiles with custom override
*/

// set vars
$tile_spaces = 5;
$row_name = 'attractions_row';
$subfield_name = 'attraction';

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);
}

// clean the acf array, remove the 'featured_events' key.
if (is_array($selected_posts_obj)) {
	
	$clean_selected_posts_obj = array();
	foreach($selected_posts_obj as $item) {
		$clean_selected_posts_obj[] = $item[$subfield_name];
	}
}

// only run priority sort and merge if less than $tile_spaces
if ($selected_post_count < $tile_spaces) {

	// calculate number of pages needed from event priority sort query
	$sort_posts_per_page = $tile_spaces - $selected_post_count;

	// event priority sort query
	$priority_wp_query_posts = priority_event_sort($sort_posts_per_page, false, null, $first_category);
	$priority_wp_query_posts = $priority_wp_query_posts->posts;

	// merge the two arrays together
	if ($selected_post_count > 0) {

		$posts = array_merge($clean_selected_posts_obj, $priority_wp_query_posts);
	} else {

		$posts = $priority_wp_query_posts;
	}

} else {

	$posts = $clean_selected_posts_obj;
}

?>

<?php if (is_user_logged_in() && count($posts) < $tile_spaces): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Events section doesn't have enough posts to be visible, <?php echo $tile_spaces;  ?> posts are required. Posts dynamically found: <?php echo count($priority_wp_query_posts); ?>. Posts authored: <?php echo count($clean_selected_posts_obj); ?>.</p>
</div>
<?php endif; ?>

<?php //change !== to == to reinitialise on site
if ($posts && count($posts) !== $tile_spaces): ?>
<section id="events">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<?php the_field('events_text'); ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php include(locate_template('/components/tiles-5-up/tiles-5-up.php')); ?>
		</div>
	</div>
	<?php
	$stay_URL = add_query_arg(
		array(
			'post-type' => 'event',
			), get_site_url() . '/post-list/'
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost btn-margin" href="<?php echo $stay_URL; ?>">view all events <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>

</section>
<?php endif; ?>

<?php get_footer()?>