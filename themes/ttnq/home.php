<?php
/**
 * Template Name: Home
 */
?>
<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-home/hero', 'home'); ?>

<?php get_template_part('components/crisis-notification/crisis-notification'); ?>

<section id="hero-exp-nav" class="clearfix collapse-bottom">
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center component">
			<?php the_field('hero_exp_text'); ?>
		</div>
	</div>
	<?php get_template_part('components/hero-exp-nav/hero-exp', 'nav') ?>
</section>

<?php get_template_part('components/article-tile/featured-articles'); ?>

<section id="places" class="collapse">
	<div class="clearfix">
		<?php get_template_part('components/map-carousel/map', 'carousel') ?>
	</div>
</section>


<?php
/*
	Set dynamic events tiles with custom override
*/

// set vars
$tile_spaces = 5;
$selected_post_count = 0;
$row_name = 'featured_events';
$subfield_name = 'featured_event';
$clean_selected_posts_obj = array();

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {

		
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// only run priority sort and merge if less than $tile_spaces
if ($selected_post_count < $tile_spaces) {
	// merge the two arrays together if there are selected tiles
	// otherwise just return dynamic posts
	if ($selected_post_count > 0) {

		// calculate number of pages needed from event priority sort query
		$sort_posts_per_page = $tile_spaces - $selected_post_count;
		// event priority sort query
		// priority_event_sort($posts_per_page, $paged, $category, $tag)
		$priority_wp_query = priority_event_sort($sort_posts_per_page, false, null, null);
		$priority_wp_query_posts = $priority_wp_query->posts;

		$posts = array_merge($clean_selected_posts_obj, $priority_wp_query_posts);

	} else {

		// event priority sort query
		// priority_event_sort($posts_per_page, $paged, $category, $tag)
		$priority_wp_query = priority_event_sort(5, false, null, null);
		$priority_wp_query_posts = $priority_wp_query->posts;
		$posts = $priority_wp_query_posts;

	}

} else {

	$posts = $clean_selected_posts_obj;
}
?>

<?php if (is_user_logged_in() && count($posts) < $tile_spaces): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Events section doesn't have enough posts to be visible, <?php echo $tile_spaces;  ?> posts are required. Posts dynamically found: <?php echo count($priority_wp_query_posts); ?>. Posts authored: <?php echo count($clean_selected_posts_obj); ?>.</p>
</div>
<?php endif; ?>

<?php //change !== to == to reinitialise on site
if ($posts && count($posts) !== $tile_spaces): ?>
<section id="events">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<?php the_field('events_text'); ?>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php include(locate_template('/components/tiles-5-up/tiles-5-up.php')); ?>
		</div>
	</div>
	<?php
	$events_URL = add_query_arg(
		array(
			'post-type' => 'event',
			), get_site_url() . '/post-list/'
		);
	?>
	<div class="row text-center">
		<a class="btn btn-medium btn-ghost green btn-margin" href="<?php echo $events_URL; ?>">Browse all events <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</section>
<?php endif; ?>

<section class="map-section theme-coral">
<?php get_template_part('components/map/map'); ?>
<?php map_script(); ?>
</section>

<section id="looking-for-nav">
	<img class="lazyload" data-object-fit="cover" <?php responsive_image(get_field('looking_for_section_bg'), 'full', true); ?> >
	<div class="row looking-for-content">
		<div class="small-12 columns text-center component">
			<h2>What are you looking for?</h2>
		</div>
		<ul class="looking-for-list list-reset clearfix">
		<?php
			if ( have_rows('looking_for_things_to_do_items') ) {

				while ( have_rows('looking_for_things_to_do_items') ) {

					the_row();

					$post_object = get_sub_field('thing_to_do');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object ); ?>

						<li class="small-6 medium-4 large-2 columns">
							<?php get_template_part('components/icon-tile/icon', 'tile') ?>
						</li>


		<?php			wp_reset_postdata();
					}
				}
			}
		?>
		</ul>
	</div>
</section>

<?php
// Show ad unit based on checkbox "Show on home page"
// randomised if more than one
$args = array(
    'post_type'     => 'ad_unit',
    'post_status'   => 'publish',
    'meta_key' => 'show_on_home_page',
    'meta_value' => true,
    'orderby' 		=> 'rand',
    'posts_per_page' => '1',
    'no_found_rows' => true,
    'update_post_term_cache' => false,
    'posts_per_page' => 1,
    'paged'          => false,
);

$wp_query = new WP_Query($args);

$posts = $wp_query->posts;

if ( have_posts() ) : ?>
<section id="banner-1" class="collapse">
	<div class="small-12">

		<?php foreach( $posts as $post ): 
		
		setup_postdata( $post );
		
		?>
		
		<?php get_template_part('/components/ad-units/banner-ad') ?>

		<?php endforeach; ?>
					
	</div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>

<?php if (get_field('stackla_html_snippet')) : ?>
<?php include(locate_template('/components/stackla/stackla.php')); ?>
<?php endif; ?>

<?php get_footer()?>