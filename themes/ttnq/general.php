<?php
/**
 * Template Name: flexible
 */

$theme_colour = get_field('theme_colour');

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/hero-standard/hero', 'standard') ?>

<style>

	h2:before {
		border-bottom: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.btn:focus {
		background-color: #<?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}
	
</style>

<div class="flexible">

	<?php if (have_posts()) : ?>
	<section>
		<div class="row">
			<div class="columns">
				<?php
				while (have_posts()) : the_post();
					the_content();
				endwhile;
				?>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php
	if (have_posts()) :
		while (have_posts()) : the_post(); ?>

	<!-- check if the flexible content field has rows of data -->
	<?php if( have_rows('flexible_content') ): ?>
	<div id="description-1" class="collapse">
		<div class="row">
			<div class="small-12 columns">
			
				<?php
					// loop through the rows of data
					while ( have_rows('flexible_content') ) : the_row();

				        // +++++++++++++++++++ Start: Text and Image +++++++++++++++++++++++

				        if( get_row_layout() == 'text_and_image' ):

				        	echo '<div class="flexible-text-and-image-wrapper">';

				        		if( get_sub_field('heading') ):

					        		echo '<div class="row component">
					        				<h2 class="text-center small-12 medium-12 large-6 large-offset-3">'.get_sub_field('heading').'</h2></div>';

					        	endif;

					        	// check if the nested repeater field has rows of data
					        	if( get_sub_field('image_content') ):

					        		$image_dir = get_sub_field('image_direction') ? 'direction-right' : null;
					        		$count = count(get_sub_field('image_content'));
					        		$i = 0;

								 	echo '<div class="row flexible-image-wrapper"><div class="small-12 large-10 large-offset-1">';

								 	// loop through the rows of data
								    while ( have_rows('image_content') ) : the_row();

								    	$i++;
										$caption = get_sub_field('caption') && $count > 1 ? '<div class="flexible-image-caption"><p>'.get_sub_field('caption').'</p></div>' : null;
										$single_caption = $count == 1 && get_sub_field('caption') ? '<div class="flexible-image-caption-below"><p>'.get_sub_field('caption').'</p></div>' : null;

										switch($count) {
											case 1 :
												$layout = 'one-up-';
												break;
											case 2 : 
												$layout = 'two-up-';
												break;
											case 3 :
												$layout = 'three-up-';
												break;
											case 4 :
												$layout = 'four-up-';
												break;
											default:
												$layout = 'one-up-';
										}

										echo '<div class="lightbox-image flexible-image '.$image_dir.' '.$layout.$i.' flexible-image-'.$i.'" data-responsive-background-image>
												<img class="text-image-tile-image" '.responsive_image_return(get_sub_field('image'), 'large').'/>'.$caption.'</div>'.$single_caption;

									endwhile;

									echo '</div></div>';

								endif;

								if( get_sub_field('video') ):

									$has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

									echo '<div class="row"><div class="small-12 large-10 large-offset-1"><div class="flexible-video-wrapper '.$has_caption.'">';
									the_sub_field('video');
									echo '</div></div></div>';

									if (get_sub_field('video_caption')) :
										echo '<div class="flexible-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
									endif;

								endif;

								// +++++++++++++++++++++++++++++++++++++++++++++++++

								if( get_sub_field('text') ):

					        		echo '<div class="row">
					        				<div class="clearfix"><div class="small-12 medium-12 large-8 large-offset-2 columns">';

				        			the_sub_field('text');

				        			echo '</div></div>';

					        	endif;

					        	// +++++++++++++++++++++++++++++++++++++++++++++++++

					        	if( get_sub_field('button_url') || get_sub_field('button_link') ):

					        		$button_url = get_sub_field('button_url') ? get_sub_field('button_url') : get_sub_field('button_link');
					        		$icon = get_sub_field('button_url') ? 'window-restore' : 'chevron-right';
					        		$target = get_sub_field('open_in_new_window') ? 'target="_blank"' : null;

					        		echo '<div class="text-center"><a '.$target.' class="flexible-button btn btn-medium btn-ghost" href="'.$button_url.'">'.get_sub_field('button_text').' <i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></div>';

					        	endif;

				        	echo '</div></div>';

				        endif;

				    	// +++++++++++++++++++ End: Text and image ++++++++++++++++++++++++++++++++++++

					endwhile;
				?>

			</div>
		</div>
	</div>
	<?php endif;

		endwhile;

	endif; ?>

</div>

<?php get_footer()?>