<?php

// expire offer posts on date field.
if (!wp_next_scheduled('expire_events_posts')){
  wp_schedule_event(time(), 'daily', 'expire_events_posts'); // this can be hourly, twicedaily, or daily
}

add_action('expire_events_posts', 'expire_posts_function');

function expire_posts_function() {
	$today = date('Ymd');
	$args = array(
		'post_type' => array('event'), // post types you want to check
		'posts_per_page' => -1 
	);
	$posts = get_posts($args);
	foreach($posts as $p){
		$expiredate = get_field('end_time_of_event', $p->ID, false, false); // get the raw date from the db
		if ($expiredate) {
			if($expiredate < $today){
				$postdata = array(
					'ID' => $p->ID,
					'post_status' => 'draft'
				);
				wp_update_post($postdata);
			}
		}
	}
}