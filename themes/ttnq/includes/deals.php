<?php
/***
 * Array params
 * int @per_page
 * string @post_in - comma delimited
 *
 */
function get_deals($params=null, $rand=false){
    $per_page = -1;
    if($params)
        extract($params);
    $type = 'deal';
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'posts_per_page' => $per_page
    );
    if(isset($category_id)){
        $args['category'] = $category_id;
    }
    if($post_in){
        $post_ids = explode(',', $post_in);
        if($rand){
            shuffle($post_ids);
        }
        $args['post__in'] = $post_ids;
        $args['orderby'] = 'post__in';
    }
    $my_query = null;
    return new WP_Query($args);
}

function get_deals_list($params=null){
    $per_page = -1;
    if($params)
        extract($params);
    $type = 'deal';
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'ignore_sticky_posts'=> true,
        'orderby' => 'slug',
        'order' => 'ASC'
    );
    if($category_id){
        $args['cat'] = $category_id;
    }
    if($post_in){
        $args['post__in'] = explode(',', $post_in);
    }
    $my_query = null;
    $my_query = new WP_Query($args);
    ?>

    <?php if( $my_query->posts ):?>
        <?php foreach ($my_query->posts as $post) : ?>
            <?php

            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url($post->ID,'large');
            $category = get_the_terms($post->ID, "deal_category");
            $category_name = $category[0]->name;
            $post_title = $post->post_title;
            $post_title= strlen($post_title) > 80 ? substr($post_title,0,80)."..." : $post_title;
            $link = get_field('cta', $post->ID);
            $provider = get_field('provider', $post->ID);
            $location = get_field('location', $post->ID);
            $slug = get_post_field( 'post_name', $post->ID );
            ?>
            <li class="small-6 medium-6 large-3 columns">
                <a class="custom-post-tile" target="blank" href="<?php echo $link; ?>">
                    <img class="lazyload" data-object-fit="cover" alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                    <div class="custom-post-tile-tag"><?php echo $category_name ?> </div>
                    <div class="custom-post-tile-text">
                        <p><?php echo $provider; ?></p>
                        <h5 class="caps"><?php echo $post_title; ?></h5>
                        <?php if($location): ?>
                            <span class="location-icon">
								<i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?>
							</span>
                        <?php endif; ?>
                        <!--<div class="custom-post-tile-text-view" style="display: none;">
                            View Deal<i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>-->
                        <div class="overlay"></div>
                    </div>
                </a>
            </li>
        <?php endforeach; ?>
    <?php
    endif;
    wp_reset_postdata();
    wp_reset_query();
}

add_action('wp_ajax_nopriv_get_deals_list', 'get_deals_list');
add_action('wp_ajax_get_deals_list', 'get_deals_list');

function get_deals_categories(){
    $args = array(
        'taxonomy' => 'deal_category',
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => 1
    );
    return get_categories($args);
}

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');

function load_posts_by_ajax_callback() {
    $paged = $_POST['page'];
    $limit = 4;
    if($paged > 1) $limit = -1;
    $args = array(
        'post_type' => 'deal',
        'post_status' => 'publish',
        'posts_per_page' => $limit,
        'paged' => $paged,
        'sort_custom' => true,
        'meta_query' => array(
            array(
                'key' => 'member_level',
            ),
            array(
                'key' => 'provider',
            )
        ),
        'orderby'    => array(
            'meta_value' => 'ASC'
        )
    );
    $myposts = get_posts($args);

    foreach ($myposts as $post) : ?>
        <?php
        /* grab the url for the full size featured image */
        $featured_img_url = get_the_post_thumbnail_url($post->ID,'large');
        $post_title = $post->post_title;
        $post_title = strlen($post_title) > 80 ? substr($post_title,0,80)."..." : $post_title;
        $category = get_the_terms($post->ID, "deal_category");
        $category_name = $category[0]->name;
        $link = get_field('cta', $post->ID);
        $provider = get_field('provider', $post->ID);
        $location = get_field('location', $post->ID);
        $slug = get_post_field( 'post_name', $post->ID );
        ?>
        <li class="small-6 medium-6 large-3 columns">
            <a class="custom-post-tile" target="blank" href="<?php echo $link; ?>">
                <img class="lazyload" data-object-fit="cover" alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                <div class="custom-post-tile-tag"><?php echo $category_name ?> </div>
                <div class="custom-post-tile-text">
                    <p><?php echo $provider; ?></p>
                    <h5 class="caps"><?php echo $post_title; ?></h5>
                    <?php if($location): ?>
                        <span class="location-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?>
						</span>
                    <?php endif; ?>
                    <!--<div class="custom-post-tile-text-view" style="display: none;">
                        View Deal<i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>-->
                    <div class="overlay"></div>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
    <?php
    wp_reset_postdata();
    wp_die();
}

add_shortcode( 'exclusive_deals', 'exclusive_deals_shortcode' );
function exclusive_deals_shortcode($atts){
    ob_start();
    $post_ids = $atts['post_ids'];
    $my_query = get_deals(['per_page'=>3, 'post_in'=>"$post_ids"], true);
    if( $my_query->have_posts() ):?>
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <?php
            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'large');
            $category = get_the_terms(get_the_ID(), "deal_category");
            $category_name = $category[0]->name;
            $post_title = get_the_title();
            $post_title = strlen($post_title) > 80 ? substr($post_title,0,80)."..." : $post_title;
            $link = get_field('cta');
            $provider = get_field('provider');
            $location = get_field('location');
            $slug = get_post_field( 'post_name', get_the_ID() );
            ?>
            <li class="small-12 medium-4 large-4 columns">
                <a class="custom-post-tile" target="blank" href="<?php echo $link; ?>">
                    <img class="lazyload" data-object-fit="cover" alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                    <div class="custom-post-tile-tag"><?php echo $category_name ?> </div>
                    <div class="custom-post-tile-text">
                        <p><?php echo $provider; ?></p>
                        <h5 class="caps"><?php echo $post_title; ?></h5>
                        <?php if($location): ?>
                            <span class="location-icon">
								<i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?>
							</span>
                        <?php endif; ?>
                        <!--<div class="custom-post-tile-text-view" style="display: none;">
                            View Deal<i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>-->
                        <div class="overlay"></div>
                    </div>
                </a>
            </li>
        <?php endwhile; ?>
    <?php
    endif;
    wp_reset_query();
    return ob_get_clean();
}

add_shortcode( 'exclusive_deals_normal', 'exclusive_deals_normal_shortcode' );
function exclusive_deals_normal_shortcode($atts){
    ob_start();
    $post_ids = $atts['post_ids'];
    $my_query = get_deals(['per_page'=>6, 'post_in'=>"$post_ids"]);
    if( $my_query->have_posts() ):?>
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <?php
            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'large');
            $category = get_the_terms(get_the_ID(), "deal_category");
            $category_name = $category[0]->name;
            $post_title = get_the_title();
            $post_title = strlen($post_title) > 80 ? substr($post_title,0,80)."..." : $post_title;
            $link = get_field('cta');
            $provider = get_field('provider');
            $location = get_field('location');
            $slug = get_post_field( 'post_name', get_the_ID() );
            ?>
            <li class="small-12 medium-6 large-6 columns">
                <a class="custom-post-tile" target="blank" href="<?php echo $link; ?>">
                    <img class="lazyload" data-object-fit="cover" alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                    <div class="custom-post-tile-tag"><?php echo $category_name ?> </div>
                    <div class="custom-post-tile-text">
                        <p><?php echo $provider; ?></p>
                        <h5 class="caps"><?php echo $post_title; ?></h5>
                        <?php if($location): ?>
                            <span class="location-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?>
						</span>
                        <?php endif; ?>
                        <!--<div class="custom-post-tile-text-view" style="display: none;">
                            View Deal<i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>-->
                        <div class="overlay"></div>
                    </div>
                </a>
            </li>
        <?php endwhile; ?>
    <?php
    endif;
    wp_reset_query();
    return ob_get_clean();
}


function acf_load_exclusivedeals_field_choices( $field ) {
    // reset choices
    $field['choices'] = array();
    $args = array(
        'post_type' => 'deal',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'slug',
        'order' => 'ASC'
    );
    $myposts = get_posts($args);

    // loop through array and add to field 'choices'
    if( is_array($myposts) ) {
        foreach( $myposts as $post ) {
            $provider = get_field('provider', $post->ID);
            $field['choices'][ $post->ID ] = $provider;
        }
    }
    // return the field
    return $field;
}

add_filter('acf/load_field/name=exclusivedeals', 'acf_load_exclusivedeals_field_choices');

function get_deal_item(){
    $deal_category_id = $_POST['deal_category_id'];
    $args = array(
        'post_type' => 'deal',
        'numberposts' => -1,
        'tax_query' => [
            [
                'taxonomy' => 'deal_category',
                'terms' => $deal_category_id,
                'include_children' => false // Remove if you need posts from term 7 child terms
            ],
        ],
        'sort_custom' => true,
        'meta_query' => array(
            array(
                'key' => 'member_level',
            ),
            array(
                'key' => 'provider',
            )
        ),
        'orderby'    => array(
            'meta_value' => 'ASC'
        )
    );
    $myposts = get_posts($args);
    $ctr = 0;
    echo "<div class='row'><div class='small-12 show-more-container-inner'><ul class='list-reset tile-3-list clearfix'>";
    foreach ($myposts as $post) : ?>
        <?php
        /* grab the url for the full size featured image */
        $featured_img_url = get_the_post_thumbnail_url($post->ID,'large');
        $post_title = $post->post_title;
        $post_title = strlen($post_title) > 80 ? substr($post_title,0,80)."..." : $post_title;
        $category = get_the_terms($post->ID, "deal_category");
        $category_name = $category[0]->name;
        $link = get_field('cta', $post->ID);
        $provider = get_field('provider', $post->ID);
        $location = get_field('location', $post->ID);
        $slug = get_post_field( 'post_name', $post->ID );
        ?>
        <li class="small-6 medium-6 large-3 columns">
            <a class="custom-post-tile" target="blank" href="<?php echo $link; ?>">
                <img class="lazyload" data-object-fit="cover" alt="<?php echo $provider; ?>" src="<?php echo ($featured_img_url)?$featured_img_url:get_template_directory_uri() . "/images/landing-page/image-not-available.png"; ?>" />
                <div class="custom-post-tile-tag"><?php echo $category_name ?> </div>
                <div class="custom-post-tile-text">
                    <p><?php echo $provider; ?></p>
                    <h5 class="caps"><?php echo $post_title; ?></h5>
                    <?php if($location): ?>
                        <span class="location-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $location; ?>
						</span>
                    <?php endif; ?>
                    <!--<div class="custom-post-tile-text-view" style="display: none;">
                        View Deal<i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>-->
                    <div class="overlay"></div>
                </div>
            </a>
        </li>
        <?php
        $ctr++;
    endforeach; ?>
    <?php
    echo "</ul></div></div>";
    wp_reset_postdata();
    wp_die();
}

add_action('wp_ajax_nopriv_get_deal_item', 'get_deal_item');
add_action('wp_ajax_get_deal_item', 'get_deal_item');