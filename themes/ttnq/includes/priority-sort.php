<?php 
/*
priority_sort sorts posts by the custom field priority (low to high), and then
randomises within priority. Priority can take values in [1, 3] and may be
unset. Posts that do not have the custom field priority set are ordered after
posts that do have priority set. 

For example, if posts a, b, c have priority 1; d, e priority 2; f priority 3;
and x, y, z do not have priority set, the following is a valid return array:
[b, c, a, e, d, f, z, y, x]

args: 
  posts_per_page (numerical) the number of posts (per page if paged == true).
  paged (boolean) whether or not the posts should be paged.
  post_type (string) the type of the posts.
  category (string) the category of the posts.
  tag (string) post tag.

returns:
  the WP_Query object.
*/
function priority_sort($posts_per_page, $paged, $post_type, $category, $tag) {

	$meta_key = 'priority';

	$args = array(
    	'post_type'              => $post_type,
  		'category_name'          => $category,
   		'tag'                    => $tag,
   	    'post_status'            => 'publish',
	    'update_post_term_cache' => false,
	    'posts_per_page'         => $posts_per_page,
	    'paged'                  => $paged,
	    // 'meta_key'				 => $meta_key,
	    // Get all posts, that have priority set.
	    'meta_query' => array(
	        array(
	            'key'     => $meta_key,
	            'compare' => 'EXISTS',
	        ),
	    ),
	    // Order first by priority (1, 2, 3, unset) and then randomise within
	    // priority.
	    'orderby' => array(
	        'meta_value_num' => 'ASC',
	        'rand'           => 'DESC',
	    ),
	);

return new WP_Query($args);
}

/*
priority_event_sort sorts event posts by the custom field priority (low to 
high), and then by the custom field time_of_event (soonest to furtherest in
the future), within priority. Priority can take values in [1, 3] and may be
unset. Posts that do not have the custom field priority set are ordered after
posts that do have priority set.

args: 
  posts_per_page (numerical) the number of posts (per page if paged == true).
  paged (boolean) whether or not the posts should be paged.
  post_type (string) the type of the posts.
  category (string) the category of the posts.
  tag (string) post tag.

returns:
  the WP_Query object.
*/
function priority_event_sort($posts_per_page, $paged, $category, $tag) {

	$args = array(
		// This function is strictly for post_type 'event'.
    	'post_type'              => 'event',
  		'category_name'          => $category,
   		'tag'                    => $tag,
   	    'post_status'            => 'publish',
	    'update_post_term_cache' => false,
	    'posts_per_page'         => $posts_per_page,
	    'paged'                  => $paged,
	    'meta_query' => array(
    	    // Get all posts, that have priority set.
    		'priority_clause' => array(
	        	'relation' => 'OR',
	        	array(
	            	'key'     => 'priority',
	            	'compare' => 'EXISTS',
	        	),       
	    	),
	     	'date_clause' => array(
	    	    'key'     => 'time_of_event',
	            'compare' => 'EXISTS',
	     	),
	     	'expired_clause' => array(
	     		'relation' => 'OR',
	     		array(
		     		'key' => 'time_of_event',
		            'value' => date('Y-m-d'),
		            'compare' => '>=',
		            'type' => 'DATE'
		     	),
		     	array(
		     		'key' => 'end_time_of_event',
		            'value' => date('Y-m-d'),
		            'compare' => '>=',
		            'type' => 'DATE'
		     	),
	     	),
	    ),
	    // Order first by priority (1, 2, 3, unset) and then by time_of_event
	    // (within priority).
	    'orderby' => array(
	    	'meta_value_num'  => 'ASC',
	        'date_clause'     => 'ASC',
	    ),
	);

return new WP_Query($args);
}

/*
Priority sort itinerary sorts posts by priority and by itinerrary type
*/

function priority_sort_itinerary($posts_per_page, $paged, $category, $itinerary_type) {

	$args = array(
    	'post_type'              => 'itinerary',
  		'category_name'          => $category,
   	    'post_status'            => 'publish',
	    'update_post_term_cache' => false,
	    'posts_per_page'         => $posts_per_page,
	    'paged'                  => $paged,
	    // Get all posts, that have priority set.
	    'meta_query' => array(
	        array(
	            'key'     => 'priority',
	            'compare' => 'EXISTS',
	        ),
	        array(
	            'key'     => 'itinerary_type',
	            'value'	  => $itinerary_type,
	            'compare' => '=',
	        ),
	    ),
	    // Order first by priority (1, 2, 3, unset) and then randomise within
	    // priority.
	    'orderby' => array(
	        'meta_value_num' => 'ASC',
	        'rand'           => 'DESC',
	    ),
	);

return new WP_Query($args);
}

/*
event_sort sorts event posts by the custom field time_of_event (from soonest
to furtherst in the future).

args: 
  posts_per_page (numerical) the number of posts (per page if paged == true).
  paged (boolean) whether or not the posts should be paged.
  post_type (string) the type of the posts.
  category (string) the category of the posts.
  tag (string) post tag.

returns:
  the WP_Query object.
*/
function event_sort($posts_per_page, $paged, $category, $tag, $meta_key) {

	$args = array(
    	'post_type'              => 'event',
  		'category_name'          => $category,
   		'tag'                    => $tag,
   	    'post_status'            => 'publish',
	    'update_post_term_cache' => false,
	    'posts_per_page'         => $posts_per_page,
	    'paged'                  => $paged,
	    'orderby'                => 'meta_value_num',
	    'order'                  => 'ASC',
	    'meta_query' => array(
	     	'date_clause' => array(
	    	    'key'     => $meta_key,
	            'compare' => 'EXISTS',
	     	),
	     	'expired_clause' => array(
	     		'relation' => 'OR',
	     		array(
		     		'key' => $meta_key,
		            'value' => date('Y-m-d'),
		            'compare' => '>=',
		            'type' => 'DATE'
		     	),
		     	array(
		     		'key' => 'end_time_of_event',
		            'value' => date('Y-m-d'),
		            'compare' => '>=',
		            'type' => 'DATE'
		     	),
	     	),
	    ),
	    'orderby' => array(
	        'date_clause'     => 'ASC',
	    ),
	);

return new WP_Query($args);
}

/*
Simply sorts posts (blog articles) by date published from newest to oldest.

args: 
  posts_per_page (numerical) the number of posts (per page if paged == true).
  paged (boolean) whether or not the posts should be paged.
  post_type (string) the type of the posts.
  category (string) the category of the posts.
  tag (string) post tag.

returns:
  the WP_Query object.
*/
function post_sort($posts_per_page, $paged, $category, $tag, $meta_key) {

	$args = array(
		// This function is strictly for post_type 'event'.
    	'post_type'              => 'post',
  		'category_name'          => $category,
   		'tag'                    => $tag,
   	    'post_status'            => 'publish',
	    'update_post_term_cache' => false,
	    'posts_per_page'         => $posts_per_page,
	    'paged'                  => $paged,
	    'order'                  => 'DESC'
	);

	return new WP_Query($args);
}

?>