<?php 

// add wrapper to sub menu
class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth, $args = array() ) {
		$indent = str_repeat("\t", $depth);

		if ($depth == 0) {
			$output .= "\n$indent<div class='sub-menu-wrapper'><div class='clearfix'><ul class='sub-menu'>\n";
		} else {
			$output .= "\n$indent<ul class='sub-menu-child'>\n";
		}
	}
	function end_lvl( &$output, $depth, $args = array() ) {
		$indent = str_repeat("\t", $depth);

		if ($depth == 0) {
			$output .= "$indent</ul></div></div>\n";
		} else {
			$output .= "$indent</ul>\n";
		}
	}
}