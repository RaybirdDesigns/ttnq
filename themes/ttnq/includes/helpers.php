<?php

// A group of useful functions

/*
	=======================================
	Get entire url and query string
	=======================================
*/
if ( ! function_exists( 'get_current_page_url' ) ) {
    function get_current_page_url() {
      global $wp;
      return add_query_arg( $_SERVER['QUERY_STRING'], '', home_url( $wp->request ) );
    }
}

/*
	=======================================
	Add http if needed
	=======================================
*/
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/**
 * @param $event_start
 * @param $event_end
 * @param null $event_category
 * @return int[]|WP_Post[]
 */
function get_events_within($event_start, $event_end, $event_category= null)
{
    $date_format = get_sub_field_object('time_of_event')['return_format'];
    $event_start = date_parse_from_format($date_format, $event_start);
    $event_end = date_parse_from_format($date_format, $event_end);
    $event_start = get_formatted_event_date($event_start);
    $event_end = get_formatted_event_date($event_end);

    $args = array(
        'numberposts' => 5,
        'post_type' => 'event',
        'meta_key' => 'time_of_event',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'meta_query' => array(
            array(
                'key' => 'time_of_event',
                'value' => $event_start,
                'type' => 'numeric',
                'compare' => '>=',
            ),
            array(
                'key' => 'time_of_event',
                'value' => $event_end,
                'type' => 'numeric',
                'compare' => '<=',
            ),
        ),
    );
    if (!empty($event_category)) {
        $args['category'] = $event_category;
    }

    return get_posts($args);
}

/**
 * @param array $event_parsed_date
 * @return string
 */
function get_formatted_event_date(array $event_parsed_date)
{
    return $event_parsed_date['year'] . str_pad($event_parsed_date['month'], 2, '0', STR_PAD_LEFT) . str_pad($event_parsed_date['day'], 2, '0', STR_PAD_LEFT);
}