<?php

// ===============================================================================================
// This is used for Itineraries
// ===============================================================================================
// @example: /wp-json/custom/v1/itineraries
add_action( 'rest_api_init', 'custom_api_itineraries' );   

function custom_api_itineraries() {
    register_rest_route( 'custom/v1', '/itineraries', array(
        'methods' => 'GET',
        'callback' => 'custom_api_itineraries_callback'
    ));
}
// Used in the quick search.
function custom_api_itineraries_callback( $request ) {
    // Receive and set the page parameter from the $request for pagination purposes
    $itinerary_type = $request->get_param( 'itinerary_type' ) ? $request->get_param( 'itinerary_type' ) : null;

    $itineraries_args = array(
        'paged' => false,
        'post_type' => 'itinerary',
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => 800,            
        'orderby' => 'date',
        'order'   => 'DESC',
        'meta_query' => array(
			array(
            	'key'      => 'itinerary_type',
            	'value'    => $itinerary_type,
				'compare' => 'LIKE',
			),
        ) 
    );

    $itineraries_query = new WP_Query($itineraries_args);

    $posts = $itineraries_query->posts;

    // Initialize the array that will receive the posts' data.
    $posts_data = array();
    $global_fallback_image = get_field('global_fallback_image', 'options');
    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 32);
        $image_id = has_post_thumbnail( $id ) ? get_post_thumbnail_id( $id ) : null;
        $featured_image = $image_id ? wp_get_attachment_image_url($image_id, 'large') : wp_get_attachment_image_url($global_fallback_image, 'large');
        $featured_image_medium = $image_id ? wp_get_attachment_image_url($image_id, 'medium') : wp_get_attachment_image_url($global_fallback_image, 'medium');
        $featured_image_alt = $image_id ? get_post_meta($image_id , '_wp_attachment_image_alt', true) : null;
        $location = get_field('location', $id);
        $distance = get_field('distance', $id);
        $highlights = get_field('highlights', $id);
        $map = get_field('map', $id);
        $time = get_field('time', $id);
        $icons = get_field('icons', $id);
        $type = get_field('itinerary_type', $id);


        if (!empty($icons)) {
            $icons_urls = array();
            foreach($icons as $icon) {
                $icons_urls[] = $icon['url'];
            }
        } else {
            $icons_urls = array();
        }

        $sights = get_the_terms($id, 'sights');

        if (!empty($sights)) {
            $sights_ids = array();
            foreach($sights as $sight) {
                $sights_ids[] = $sight->term_id;
            }
        } else {
            $sights_ids = array();
        }

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'link' => $post_link,
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>')['rendered'],
            'category' => get_the_category( $post->ID )[0]->cat_name,
            'list_image' => $featured_image ? $featured_image : wp_get_attachment_image_url($global_fallback_image, 'large'),
            'list_image_medium' => $featured_image_medium ? $featured_image_medium : wp_get_attachment_image_url($global_fallback_image, 'medium'),
            'alt' => $featured_image ? $featured_image_alt : get_post_meta($global_fallback_image , '_wp_attachment_image_alt', true),
            'location' => $location,
            'distance' => $distance,
            'highlights' => $highlights,
            'map' => $map,
            'time' => $time,
            'sights' => $sights_ids,
            'icons' => $icons_urls,
            'type' => $type,
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $itineraries_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $itineraries_query->max_num_pages );

    return $response;                   
}