<?php
// $is_stage = array_shift((explode('.', $_SERVER['HTTP_HOST']))) == 'staging' ? true : flase;
// $is_local = site_url() == 'http://localhost' ? true : false;

if (is_page_template('home.php') && $_SERVER["HTTP_CF_IPCOUNTRY"]) {

	// Collect the current state from the URL and CloudFlare
	$user_country_code = strtolower($_SERVER["HTTP_CF_IPCOUNTRY"]);
	// Get cookie value
	$cookie_locale = strtolower($_COOKIE['locale']);
	$primary_locale = 'au';

	if (!$cookie_locale) {
		switch($user_country_code) {
			case 'jp':
				header('Location: https://www.tropicalnorthqueensland.org/jp/');
				die();
				break;
			default:
				//nuthing
				die();
		}
	} else if ($cookie_locale != $primary_locale) {
		header('Location: https://www.tropicalnorthqueensland.org/jp/');
		die();
	}
}