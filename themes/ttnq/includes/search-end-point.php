<?php

// ===============================================================================================
// This is used for search
// ===============================================================================================
// @example: /wp-json/custom/v1/search?s=kiama
add_action( 'rest_api_init', 'custom_api_search' );   

function custom_api_search() {
    register_rest_route( 'custom/v1', '/search', array(
        'methods' => 'GET',
        'callback' => 'custom_api_search_callback'
    ));
}
// Used in the quick search.
function custom_api_search_callback( $request ) {
    // Receive and set the page parameter from the $request for pagination purposes
    $search_term = $request->get_param( 's' ) ? $request->get_param( 's' ) : null;
    $engine = $request->get_param( 'engine' ) ? $request->get_param( 'engine' ) : 'default';
    $page = $request->get_param( 'page' ) || !empty($request->get_param( 'page' )) ? $request->get_param( 'page' ) : 1;

    $search_args = array(
        's' => $search_term,
        'page' => $page,
        'engine' => $engine,
        'post_type' => array(),
        'nopaging' => false,
        'fields' => 'all',
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => 7         
    );

    $search_query = new SWP_Query($search_args);

    $posts = $search_query->posts;

    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    $global_fallback_image = get_field('global_fallback_image', 'options');
    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $post_thumbnail = has_post_thumbnail( $id ) ?
                wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') :
                (get_field('product_image_0', $id) ? get_field('product_image_0', $id) : get_field('product_image_1', $id));
        $featured_image = has_post_thumbnail( $id ) ? array('media_details' => array('sizes' => array('large' => array('source_url' => $post_thumbnail))))['media_details']['sizes']['large']['source_url'] : null;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 32);
        $time_of_event = get_field('time_of_event', $id);

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'category' => $post->post_type,
            'link' => $post_link,
            'image' => $featured_image ? $featured_image : ($post_thumbnail ? $post_thumbnail.'?w=480' : wp_get_attachment_image_url($global_fallback_image, 'large')),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>')['rendered'],
            'time_of_event' => $time_of_event,
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $search_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $search_query->max_num_pages );

    return $response;                   
}