<?php

// convert image to base64
function base_64_convert($path) {

   $type = pathinfo($path, PATHINFO_EXTENSION);
   $data = file_get_contents($path, false, basic_auth_cred());
   return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

//Get meta info about an image
function wp_get_attachment( $attachment_id ) {

$attachment = get_post( $attachment_id );
	return array(
	    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
	    'caption' => $attachment->post_excerpt,
	    'description' => $attachment->post_content,
	    'href' => get_permalink( $attachment->ID ),
	    'src' => $attachment->guid,
	    'title' => $attachment->post_title
	);
}


// Output ATDW image
function adtw_image($size = 1200, $image_index, $is_lazy = false) {
	$src = $is_lazy ? 'data-src' : 'src';
	$src_set = $is_lazy ? 'data-srcset' : 'srcset';

	$image_index = $image_index == null ? 0 : $image_index;
	$image_src = get_field('product_image_'.$image_index) ? get_field('product_image_'.$image_index) : get_sub_field('product_image_'.$image_index);
	$image_alt = get_field('product_image_alt_'.$image_index) ? get_field('product_image_alt_'.$image_index) : get_sub_field('product_image_alt_'.$image_index);
	$image_src_sized = $image_src.'?w='.$size;
	$image_srcset = $image_src.'?w='.$size.' '.$size.'w, '.$image_src.'?w='.ceil($size/1.3).' '.ceil($size/1.3).'w, '.$image_src.'?w='.ceil($size/1.6).' '.ceil($size/1.6).'w, '.$image_src.'?w='.ceil($size/2).' '.ceil($size/2).'w, '.$image_src.'?w=680 580w';

    echo $src.'="'.$image_src_sized.'" '.$src_set.'="'.$image_srcset.'" alt="'.$image_alt.'"';
}

// setup responsive image feature image
// supports lazy load with $is_lazy arg 
function responsive_image_post($image_post_size, $is_lazy = false)
{
	global $post;
	$src = $is_lazy ? 'data-src' : 'src';
	$src_set = $is_lazy ? 'data-srcset' : 'srcset';

	$image_id = get_post_thumbnail_id($post->ID);
	// set the default src image size
	$image_src = wp_get_attachment_image_url($image_id, $image_post_size);

	// set the srcset with various image sizes
	$image_srcset = wp_get_attachment_image_srcset($image_id, $image_post_size);

	$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);

	// generate the markup for the responsive image
	echo $src.'="'.$image_src.'" '.$src_set.'="'.$image_srcset.'" alt="'.$image_alt.'"';
}

// setup responsive image from acf field
// supports lazy load with $is_lazy arg
function responsive_image($image_id, $image_size, $is_lazy = false)
{

	// check the image ID is not blank
	if($image_id != '') {
		$src = $is_lazy ? 'data-src' : 'src';
		$src_set = $is_lazy ? 'data-srcset' : 'srcset';

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);

		// generate the markup for the responsive image
		echo $src.'="'.$image_src.'" '.$src_set.'="'.$image_srcset.'" alt="'.$image_alt.'"';

	}
}

// return responsive from acf field.
// usefull when creating markup in php.
function responsive_image_return($image_id, $image_size, $is_lazy = false)
{

	// check the image ID is not blank
	if($image_id != '') {
		$src = $is_lazy ? 'data-src' : 'src';
		$src_set = $is_lazy ? 'data-srcset' : 'srcset';

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);

		// generate the markup for the responsive image
		return $src.'="'.$image_src.'" '.$src_set.'="'.$image_srcset.'" alt="'.$image_alt.'"';

	}
}

// Allow svg
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');