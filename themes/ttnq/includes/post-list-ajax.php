<?php

// hook or 'action'. call 'ajax_post_list' on ajax request
add_action('wp_ajax_post_list', 'ajax_post_list');
add_action('wp_ajax_nopriv_post_list', 'ajax_post_list');

// functioncalled on every ajax request with action name 'post_list'
function ajax_post_list() {

	// all query params passed through the with ajax url
	$query_data = $_GET;
	
	// get post type from ajax url query string
	$post_type = isset($query_data['postType']) ? $query_data['postType'] : '';
	// get category from ajax url query string
	$category = isset($query_data['category']) ? $query_data['category'] : '';
	// get tag from ajax url query string
	$tag = isset($query_data['tag']) ? $query_data['tag'] : '';
	// get meta_key from ajax url query string
	$meta_key = isset($query_data['metaKey']) ? $query_data['metaKey'] : '';

	$paged = isset($query_data['paged']) ? intval($query_data['paged']) : 1;

	// set sort_type
	if ('event' && $meta_key == 'time_of_event') {
		$sort_type = 'event_date_sort';
	} elseif ($post_type == 'event' && $meta_key == '') {
		$sort_type = 'event_priority_sort';
	} elseif ($post_type == 'post') {
		$sort_type = 'post';
	} else {
		$sort_type = 'default';
	}

	// Determine which sorting method to use based on $sort_type. Options are:
	// > 'event_priority_sort' = event list sorted by priority and start date
	// > 'event_date_sort' = event list sorted just by start date (toggle in ui)
	// > 'post' = posts are ordered from newest to oldest
	// > default = normal post list sorted by priority
	switch ($sort_type) {
	    case 'event_priority_sort':
	        $list_loop = priority_event_sort(10, $paged, $category, $tag);
	        break;
	    case 'event_date_sort':
	        $list_loop = event_sort(10, $paged, $category, $tag, $meta_key);
	        break;
	    case 'post':
	        $list_loop = post_sort(10, $paged, $category, $tag, $meta_key);
	        break;
	    default:
	        $list_loop = priority_sort(10, $paged, $post_type, $category, $tag);
	}
	
	if( $list_loop->have_posts() ):

		echo '<ul class="list-reset">';

		while( $list_loop->have_posts() ): $list_loop->the_post();

			$post_image = has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' )[0] : wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
			$event_time = get_field('time_of_event') ? ' - ' . get_field('time_of_event') : null;

            echo '<li class="post" id="post-'.get_the_ID().'">';
            echo     '<div class="post-image" style="background-image: url('.esc_url($post_image).');"></div>';
            echo     '<div class="post-text">';

	        echo 	    '<h4><a href="'.esc_url(get_the_permalink()).'" rel="bookmark" title="Permanent Link to '.esc_html(get_the_title()).'">'.esc_html(get_the_title()).' '.esc_html($event_time).'</a></h4>';

			            if ( function_exists('the_excerpt') ) {
			                echo get_the_excerpt();
			            }

			            if (is_user_logged_in()) {
			            	// show priority notification if admin logged in
			            	$priority_num = get_field('priority') > -1 ? get_field('priority') : 'Not set';

			            	echo '<div class="tnq-notification priority priority-'.get_field('priority').'">Priority: '.esc_html($priority_num).'</div>';
			            }
            echo    '</div>'; 
                
            echo '</li>';

		endwhile;

		echo '</ul>';

		echo '<div class="post-list-pagination">';
		$big = 999999999;
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, $paged ),
			'next_text' => false,
			'prev_text' => false,
			'total' => $list_loop->max_num_pages
		) );
		echo '</div>';	
	else: ?>

		<section id="no-search-results" class="component">
		    <div class="row">
		        <div class="small-12 columns">
		            <h2 class="text-center">Search for it?</h2>
		            <div class="search-form-wrapper">
		                <svg class="search-ico" role="presentation">
		                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
		                </svg>
		                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		                    <label>
		                        <span class="show-for-sr">Search for:</span>
		                        <input type="search" class="search-field" placeholder="Search Tropical North Queensland" value="" name="s" title="Search for:" />
		                    </label>
		                </form>
		            </div>
		        </div>
		    </div>
		</section>
<?php
	endif;
	wp_reset_postdata();
	
	die();
} ?>