<?php

function cptui_register_my_taxes_sights() {

	/**
	 * Taxonomy: Sights.
	 */

	$labels = array(
		"name" => __( "Sights", "" ),
		"singular_name" => __( "Sight", "" ),
		"menu_name" => __( "Sights", "" ),
		"all_items" => __( "All Sights", "" ),
		"edit_item" => __( "Edit Sights", "" ),
	);

	$args = array(
		"label" => __( "Sights", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Sights",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'sights', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "sights",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "sights", array( "itinerary" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_sights' );
