<?php

// check how many event posts there are and return the number of
// calls required to get all the posts. This is to overcome WPs 100 post limitation
function getNumCalls(){
    $totalItems = wp_count_posts('event')->publish;
    $callsToMake = ceil($totalItems/80);  
    return $callsToMake;
}

// Get selected tags from acf field
// and return associative array of tag id => name
// Used in calendar and search apps
function calendar_filters() {
	$filters = get_field('tags_to_filter');
	$tags = get_tags();

	// create array of category id => name
	foreach($tags as $tag) {
		$tagname = $tag->name;
		$tagId = (string)$tag->term_id;
		$tagArr[$tagId] = $tagname;
	}

	// filter by the selected categories
	$filtered = array_filter(
	    $tagArr,
	    function ($key) use ($filters) {
	        return in_array($key, $filters);
	    },
	    ARRAY_FILTER_USE_KEY
	);

	$filtered['all'] = 'All';

	return $filtered;
}

// Get selected categories from acf field
// and return associative array of category id => name
// Used in calendar and search apps
function categories() {
	$categories = get_categories();

	// create array of category id => name
	foreach($categories as $category) {
		$catname = $category->name;
		$catid = (string)$category->term_id;
		$categoryArr[$catid] = $catname;
	}

	// echo '<pre>';
	// print_r($categoryArr);
	// echo '</pre>';

	// filter by the selected categories
	// $filtered = array_filter(
	//     $categoryArr,
	//     function ($key) use ($filters) {
	//         return in_array($key, $filters);
	//     },
	//     ARRAY_FILTER_USE_KEY
	// );

	// $filtered['all'] = 'All';

	return $categoryArr;
}