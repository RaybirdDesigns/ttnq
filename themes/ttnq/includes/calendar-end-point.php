<?php

// ===============================================================================================
// This is used for events calendar
// ===============================================================================================
// @example: /wp-json/custom/v1/calendar
add_action( 'rest_api_init', 'custom_api_calendar' );   

function custom_api_calendar() {
    register_rest_route( 'custom/v1', '/calendar', array(
        'methods' => 'GET',
        'callback' => 'custom_api_calendar_callback'
    ));
}
// Used in the quick search.
function custom_api_calendar_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    // Receive and set the page parameter from the $request for pagination purposes

    $calendar_args = array(
        'paged' => false,
        'post_type' => 'event',
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => 800,            
        'orderby' => 'date',
        'order'   => 'DESC',
    );

    $calendar_query = new WP_Query($calendar_args);

    $posts = $calendar_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $post_link = get_permalink( $id, false );
        $date_type = get_field('date_type', $id);
        $time_of_event = get_field('time_of_event', $id);
        $prod_email = get_field('prod_email', $id);
        $prod_phone = get_field('prod_phone', $id);
        $end_time_of_event = get_field('end_time_of_event', $id);

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'link' => $post_link,
            'category' => get_the_category( $post->ID )[0]->cat_name,
            'tags' => array(get_the_tags( $post->ID )[0]->term_id),
            'date_type' => $date_type,
            'time_of_event' => $time_of_event,
            'prod_email' => $prod_email,
            'prod_phone' => $prod_phone,
            'end_time_of_event' => $end_time_of_event,
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $calendar_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $calendar_query->max_num_pages );

    return $response;                   
}