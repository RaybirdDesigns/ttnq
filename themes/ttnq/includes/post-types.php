<?php


function cptui_register_my_cpts_accommodation() {

	/**
	 * Post Type: Accomodation.
	 */

	$labels = array(
		"name" => __( "Accommodation", "" ),
		"singular_name" => __( "Accommodation", "" ),
	);

	$args = array(
		"label" => __( "Accommodation", "" ),
		"labels" => $labels,
		"description" => "Posts type for accommodation or places to stay.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "listing/accommodation", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-store",
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "accommodation", $args );
}

add_action( 'init', 'cptui_register_my_cpts_accommodation',1 );


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function cptui_register_my_cpts_product() {

	/**
	 * Post Type: Products.
	 */

	$labels = array(
		"name" => __( "Products", "" ),
		"singular_name" => __( "Product", "" ),
	);

	$args = array(
		"label" => __( "Products", "" ),
		"labels" => $labels,
		"description" => "Post type for attractions or activities",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "listing/product", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-cart",
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "product", $args );
}

add_action( 'init', 'cptui_register_my_cpts_product',2 );


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function cptui_register_my_cpts_event() {

	/**
	 * Post Type: Events.
	 */

	$labels = array(
		"name" => __( "Events", "" ),
		"singular_name" => __( "Event", "" ),
	);

	$args = array(
		"label" => __( "Events", "" ),
		"labels" => $labels,
		"description" => "Post type for posts about events or things that are happening in a given location.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "listing/event", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-clock",
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "event", $args );
}

add_action( 'init', 'cptui_register_my_cpts_event',3 );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function cptui_register_my_cpts_itinerary() {

	/**
	 * Post Type: Events.
	 */

	$labels = array(
		"name" => __( "Itineraries", "" ),
		"singular_name" => __( "Itinerary", "" ),
	);


	$args = array(
		"label" => __( "Itineraries", "" ),
		"labels" => $labels,
		"description" => "Post type for posts about itineraries.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "listing/itinerary", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-location-alt",
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "itinerary", $args );
}

add_action( 'init', 'cptui_register_my_cpts_itinerary',3 );


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function cptui_register_my_cpts_influencers() {

	/**
	 * Post Type: Influencers.
	 */

	$labels = array(
		"name" => __( "Influencers", "" ),
		"singular_name" => __( "Influencer", "" ),
	);

	$args = array(
		"label" => __( "Influencers", "" ),
		"labels" => $labels,
		"description" => "Posts about instagram or social media influencers",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "influencers", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-megaphone",
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "influencers", $args );
}

add_action( 'init', 'cptui_register_my_cpts_influencers',4 );


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function cptui_register_my_cpts_adunit() {

	/**
	 * Post Type: Ad Units.
	 */

	$labels = array(
		"name" => __( "Ad Units", "" ),
		"singular_name" => __( "Ad Unit", "" ),
		"menu_name" => __( "Ad Units", "" ),
		"all_items" => __( "All Ad Units", "" ),
	);

	$args = array(
		"label" => __( "Ad Units", "" ),
		"labels" => $labels,
		"description" => "Post type for banner ads.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "ad_unit", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-money",
		"supports" => array( "title", "editor" ),
		"taxonomies" => array( "category", "post_tag", "media_category" ),
	);

	register_post_type( "ad_unit", $args );
}

add_action( 'init', 'cptui_register_my_cpts_adunit',5 );


function cptui_register_my_cpts() {

	/**
	 * Post Type: Deals.
	 */

	$labels = array(
		"name" => __( "Deals", "custom-post-type-ui" ),
		"singular_name" => __( "Deal", "custom-post-type-ui" ),
		"menu_name" => __( "Deals", "custom-post-type-ui" ),
	);

	$args = array(
		"label" => __( "Deals", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "deal", "with_front" => false ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "deal", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "custom-post-type-ui" ),
		"singular_name" => __( "Category", "custom-post-type-ui" ),
	);

	$args = array(
		"label" => __( "Categories", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'deal_category', 'with_front' => false, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "deal_category",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "deal_category", array( "deal" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes' );

