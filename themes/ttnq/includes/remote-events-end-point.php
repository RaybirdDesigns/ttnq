<?php

// ===============================================================================================
// This is used for getting events
// ===============================================================================================
// @example: /wp-json/custom/v1/events
add_action( 'rest_api_init', 'custom_api_events' );   

function custom_api_events() {
    register_rest_route( 'custom/v1', '/events', array(
        'methods' => 'GET',
        'callback' => 'custom_api_events_callback'
    ));
}
// Used in the quick search.
function custom_api_events_callback( $request ) {

    $posts_per_page = $request->get_param( 'count' ) ? $request->get_param( 'count' ) : 5;

    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    
    $current_day = date('d');
    $current_month = date('m');
    $current_year = date('Y');
    $events_start_date = date($current_year.'-'.$current_month.'-'.$current_day);

    $listing_args = array(
    	'paged' => false,
        'post_type' => 'event',
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => $posts_per_page,            
        'meta_query' => array(
            'date_clause' => array(
                'key'     => 'time_of_event',
                'value' => '',
                'compare' => '!=',
            ),
            'start' => array(
                'key' => 'time_of_event',
                'value' => $events_start_date,
                'type' => 'DATE',
                'compare' => '>='
            ),
        ),
        'orderby' => array(
            'date_clause'     => 'ASC',
        ),
    );

    $listing_query = new WP_Query($listing_args);
    $global_fallback_image = get_field('global_fallback_image', 'options');
    $posts = $listing_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 32);
        $image_id = has_post_thumbnail( $id ) ? get_post_thumbnail_id( $id ) : null;
        $featured_image = $image_id ? wp_get_attachment_image_url($image_id, 'large') : wp_get_attachment_image_url($global_fallback_image, 'large');
        $featured_image_alt = $image_id ? get_post_meta($image_id , '_wp_attachment_image_alt', true) : null;
        $time_of_event = get_field('time_of_event', $id);
        $tag = get_the_tags($id) ? get_the_tags($id)[0]->name : null;

        $posts_data[] = (object) array(
            'id' => $id,
            'title' => $post->post_title,
            'tag' => $tag,
            'title' => array('rendered' => $post->post_title)['rendered'],
            'link' => $post_link,
            'image' => $featured_image ? $featured_image : null,
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>')['rendered'],
            'time_of_event' => $time_of_event
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $listing_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $listing_query->max_num_pages );

    return $response;                   
}